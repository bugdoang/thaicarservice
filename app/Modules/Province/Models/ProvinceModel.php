<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Province\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProvinceModel extends Model
{
    /**
     * @return mixed
     */
    public static function getAll()
{
    return DB::table('provinces')->whereNull('deleted_at')->get();
}
}