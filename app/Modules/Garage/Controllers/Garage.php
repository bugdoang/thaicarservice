<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Garage\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Brand\Models\BrandModel;
use App\Modules\Category\Models\CategoryModel;
use App\Modules\Insurance\Models\InsuranceModel;
use App\Modules\Province\Models\ProvinceModel;
use App\Modules\Service\Models\ServiceModel;
use Illuminate\Support\Facades\Auth;

class Garage extends Controller
{
public function index()
{
    $member_id = Auth::user()->member_id; //ตรวจสอบการล็อคอิน
    $provinces=ProvinceModel::getAll();
    $open_time = config('myconfig.open_time');
    $close_time = config('myconfig.close_time');
    $open_garages = config('myconfig.open_garages');

    return view ('garage::garage',[ //ตัวแรกโฟลเดอที่เราประกาศไว้ในโมดูลโปรไวเดอะ ตัวสองโลเดอ
        'member_id'=>$member_id,
        'provinces'=>$provinces,
        'open_time'     => $open_time,
        'close_time'    => $close_time,
        'open_garages'  => $open_garages,
    ]);
}
}