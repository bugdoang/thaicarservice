<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/9/2559
 * Time: 10:02
 */

namespace App\Modules\Garage\Controllers;

use App\Modules\Profile\Services\GarageProperty;
use DB;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddGarage extends Controller
{
    public function index (Request $request)
    {
        $member_id      =Auth::user()->member_id;//เฉพาะผู้ที่ล้อคอินเข้ามาเท่านั้นจิงต้องดึงเมมเบอร์ไอดีมาด้วย
        $garage_name    =$request->get('garage_name');
        $address        =$request->get('address');
        $province_id    =$request->get('province_id');
        $latitude       =$request->get('latitude');
        $longitude      =$request->get('longitude');
        $email          =$request->get('email');
        $mobile         =$request->get('mobile');
        $website        =$request->get('website');
        $open_time      =$request->get('open_time');
        $close_time     =$request->get('close_time');
        $open_garages   =$request->get('open_garage');
        $txtDescription =$request->get('txtDescription');
        $check=array('garage_name','address','province_id','email','mobile');

        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ข้อมูลให้ครบทุกช่องด้วยค่ะ'],422);
            }
        }
        if (empty($txtDescription))
        {
            return response(['กรุณาใส่คำแนะนำอู่ซ่อมรถยนต์ด้วยค่ะ'],422);

        }
        if(!is_numeric($province_id))
        {
            return response(['กรุณาป้อนจังหวัดให้ถูกต้องด้วยค่ะ'], 422);
        }
        $open_select=[];
        if(!empty($open_garages))
        {
            $open_select=[];

            foreach ($open_garages as $open_garage)
            {
                if(!empty($open_garage))
                {
                    $open_select[]=$open_garage;
                }
            }
            if(count($open_select)==7)
            {
                $open_select=['เปิดทุกวัน'];
            }
        }
       $data = array(
            'garage_name'   => $garage_name,
            'address'       => $address,
            'province_id'   => $province_id,
            'member_id'     => $member_id,
            'latitude'      => ($latitude=='13.868148791978353')?'':$latitude,
            'longitude'     => ($longitude=='100.48204421975242')?'':$longitude,//เพราะค่า ที่เซ็ท้เป็นค่ากลาางไว้ ไม่ใช่ค่าที่userระบุมา เวลาเซฟกลับควรทีทจะเป็นค่าที่user ระบุมาเท่านั้น
            'email'         => $email,
            'mobile'        => $mobile,
            'website'       => $website,
            'open_time'     => $open_time,
            'close_time'    => $close_time,
            'open_garages'  => join(',',$open_select),
            'detail'        => $txtDescription,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $garage_id=DB::table('garages')->insertGetId($data);
        return response(['เพิ่มข้อมูลอู่ซ่อมเรียบร้อยแล้วค่ะ']);
    }
}