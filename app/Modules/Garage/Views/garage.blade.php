@extends('masterlayout')
@section('title','ลงทะเบียนอู่')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มซ้าย-->
        <div class="register-container1 col-lg-3 col-md-3">
            <div class="panel-title">
                <h2 class="header-text myfont color2">คำแนะนำ</h2>
            </div>
            <div class="item-border">
                <ul class="register-suggestion">
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        กรุณากรอกข้อมูลให้ครบทุกช่อง เพื่อสิทธิประโยชน์ในการให้ข้อมูลอู่ซ่อมรถยนต์ของท่าน
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        กรุณาใช้ข้อมูลที่เป็นความจริง เพื่อเป็นข้อมูลในการดำเนินการต่อไป
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        คุณสามารถแก้ไขข้อมูลอู่ซ่อมรถยนต์ได้ ภายหลังจากที่ได้ลงทะเบียนอู่ซ่อมรถยนต์เรียบร้อยแล้ว
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        เมื่อคุณลงทะเบียนอู่ซ่อมรถยนต์เรียบร้อยแล้ว คุณสามารถรับการจองอู่ซ่อมรถยนต์ ได้ตามที่วันเวลาเปิด/ปิดอู่ของท่าน
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                         <b>หลังจากลงทะเบียนอู่ซ่อมรถยนต์เรียบร้อยแล้ว คุณสามารถเพิ่มความน่าเชื่อถือของอู่ซ่อมรถยนต์ของคุณ ด้วยการกรอกข้อมูลให้ละเอียดมากขึ้นได้ที่เมนูข้อมูลส่วนตัว -> อู่ซ่อมรถยนต์</b>
                    </li>
                </ul>
            </div>
        </div>
        <!--เริ่มกลาง-->
        <div class="register-container col-lg-9 col-md-9">
            <div class="panel-title">
                    <h2 class="header-text myfont color2"><i class="fa fa-car" aria-hidden="true"></i> ลงทะเบียนอู่ซ่อมรถ</h2>
            </div>

                <form action="/garage" method="POST" id="fileForm" role="form" class="item-border-garage">
                 {{ csrf_field() }}
                <!--เริ่ม-->
                    <div class="col-md-12 col-lg-12">
                        <div class="header-detail myfont line1" style="margin-top: -20px">
                            ข้อมูลอู่ซ่อมรถ
                        </div>
                        <div class="row">
                            <input type="hidden" name="member_id" value="{{$member_id}}">
                            <div class="form-group col-md-12 col-lg-12">
                                <label>ชื่ออู่ซ่อมรถ <span style="color:red;"> * </span> : </label>

                                <input class="form-control" type="text"  name="garage_name" id = "garage_name" onkeyup = "Validate(this)" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-lg-12">
                                <label><i class="fa fa-home" aria-hidden="true"></i> ที่ตั้งอู่ซ่อมรถ <span style="color:red;"> * </span> : </label>
                                <input class="form-control" type="text" name="address" id="address"/>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <label>จังหวัด <span style="color:red;"> * </span> :</label>
                                <select class="form-control" name="province_id" id="province_id">
                                    <option value="">กรุณาเลือก</option>
                                    @if(!empty($provinces))
                                        @foreach($provinces as $province)
                                            <option value="{{$province->province_id}}">
                                                {{$province->province_name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label><i class="fa fa-envelope-o" aria-hidden="true"></i> อีเมล์ <span style="color:red;"> * </span> :</label>
                                <input class="form-control" type="text" name="email" id="email"/>
                            </div>
                            <div class="form-group col-md-3 col-lg-3">
                                <label><i class="fa fa-phone" aria-hidden="true"></i> เบอร์โทรศัพท์อู่ซ่อมรถ <span style="color:red;"> * </span> :</label>
                                <input class="form-control" type="text" name="mobile" id="mobile"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-lg-12">
                                <label><i class="fa fa-globe" aria-hidden="true"></i> เว็บไซต์ :</label>
                                <input class="form-control" type="text" name="website" id="website"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-2 col-lg-2"  style="margin-bottom: 5px;">
                                <label><i class="fa fa-calendar" aria-hidden="true"></i> วันเปิดทำการ :</label>
                            </div>

                            <div class="form-group col-md-4 col-lg-4" style="padding-left: 12px; margin-bottom: 0px;">
                                <input type="checkbox" class="open-all"> เปิดทุกวัน
                            </div>
                            <div class="col-md-12 col-lg-12"  style="padding-left: 0px;">
                                @if(!empty($open_garages))
                                    @foreach($open_garages as $open_garage)
                                        <div class="col-lg-1 col-md-1"  style="padding-right: 0px; width: 100px;">
                                            <label class="font-original">
                                                <input type="checkbox" name="open_garage[]" class="open-day"  value="{{$open_garage}}">
                                                {{$open_garage}}
                                            </label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label><i class="fa fa-clock-o" aria-hidden="true"></i> เวลาเปิด :</label>
                                <select class="form-control" name="open_time" id="open_time">
                                    <option value="">กรุณาเลือก</option>
                                    @if(!empty($open_time))
                                        @foreach($open_time as $time)
                                            <option value="{{$time}}">
                                                {{$time}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label><i class="fa fa-clock-o" aria-hidden="true"></i> เวลาปิด :</label>
                                <select class="form-control" name="close_time" id="close_time">
                                    <option value="">กรุณาเลือก</option>
                                    @if(!empty($close_time))
                                        @foreach($close_time as $time)
                                            <option value="{{$time}}">
                                                {{$time}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-group col-md-12 col-lg-12">
                                    <label><i class="fa fa-map-marker" aria-hidden="true"></i> ตำแหน่งร้าน : </label>
                                </div>
                                <div class="form-group col-md-12 col-lg-12">
                                    <input id="searchTextField" class="form-control" type="text" size="50" style="text-align: left;width:100%;direction: ltr;" placeholder=" ค้นหาตำแหน่ง">
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    latitude:<input name="latitude" id="latitude" class="MapLat form-control" value="13.868148791978353" type="text" placeholder="Latitude" style="width:100%;" >
                                </div>
                                <div class="form-group col-md-6 col-lg-6">
                                    longitude:<input name="longitude" id="longitude" class="MapLon form-control" value="100.48204421975242" type="text" placeholder="Longitude" style="width:100%;" >
                                </div>

                                <div id="map_canvas" style="height: 350px;width: 97%;margin: 0.6em;"></div>

                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="header-detail myfont line1">
                                    คำแนะนำ/ประวัติ อู่ซ่อมรถ <span style="color:red;"> * </span>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 listservice">
                                        <textarea name="txtDescription" id="txtDescription" cols="20" rows="3" class="text-area1 "></textarea>
                                    </div>
                                </div>
                            </div>



                        </div>
                            <!--ประกัน-->

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="button-garage myfont">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    บันทึก
                                </button>
                            </div>
                        </div>
                    </div>
                </form><!-- ends register form -->
        </div>
    </div>
    @stop
@push('scripts')
<script>
    $(function () {

        var lat =13.868148791978353;
                lng =100.48204421975242;
                latlng = new google.maps.LatLng(lat, lng),
                image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
        var mapOptions = {
                    center: new google.maps.LatLng(lat, lng),
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    panControl: true,
                    panControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.TOP_left
                    }
                },
                map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: image
                });
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
            $('.MapLat').val(place.geometry.location.lat());
            $('.MapLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function (event) {
            $('.MapLat').val(event.latLng.lat());
            $('.MapLon').val(event.latLng.lng());
            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status) {
                console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                            lng = results[0].geometry.location.lng(),
                            placeName = results[0].address_components[0].long_name,
                            latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                    $("#searchTextField").val(results[0].formatted_address);
                }
            });
        });

        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            infowindow.open(map, marker);
        }
    });
</script>
<script>
    function callback_ajax(data)
    {
        MessageBox.alert(data[0],function () {
            window.location.href='/profile' ;
        });
    }
</script>
@endpush