<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Garage\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GarageModel extends Model
{
    /**
     * @return mixed
     */
    public static function check_garagename($garage_name,$member_id)
    {
        return DB::table('garages')
            ->where('garage_name',$garage_name)
            ->where('member_id',$member_id)
            ->first();
    }

}