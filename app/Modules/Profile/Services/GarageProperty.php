<?php
namespace App\Modules\Profile\Services;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 14/9/2559
 * Time: 15:31
 */
use App\Modules\Profile\Models\ProfileModel;
use DB;

class GarageProperty
{
//
    public static function update($properties,$garage_id,$type,$options=Null)
    {
        if(!empty($properties) && is_array($properties))
        {
            $func_get='get_property';
            $func_insert='insert_property';
            $func_update='update_property';
            $func_deleted='deleted_property';
            $ole_garage_item=ProfileModel::$func_get($garage_id,$type);//ประกาศตัวแปรคือ ที่อู่เลือกมาอันเก่า มีอะไรบ้างเพืื่อไปเช็คว่ามันซ้ำไหม
            $del_garage_item=ProfileModel::$func_get($garage_id,$type,'delete');//ประกาศตัวแปรคือ ที่อู่เลือกมาอันเก่า มีอะไรบ้างเพืื่อไปเช็คว่ามันซ้ำไหม
            if ($type=='category')
            {
                $pk_id='category_id';
            }
            else
            {
                $pk_id=$type.'_id';
            }


            $item_selected = array();
            $item_delete_ole = array();


            if(!empty($ole_garage_item))//ถ้าข้อมูลเก่ามีค่าให้ วนลูปทีละตัวเพื่อเช็คค่าให้ตรงกับข้อมูลใหม่ เวลามีข้อมูลใหม่มาเช็คว่า ถ้าอันเก่าที่มีแล้วไม่เลือกแล้วให้ลบออก แต่ถ้ามีข้อมูลใหม่ที่อันเก่าไม่มีให้เพิ่มเข้าไป
            {
                foreach ($ole_garage_item as $_ole_garage_item)
                {
                    $item_selected[$_ole_garage_item->$pk_id] = 1;//1 เป็นค่าอะไรก็ได้ มีไว้เฉยๆ
                }
            }
            if(!empty($del_garage_item))
            {
                foreach ($del_garage_item as $_del_garage_item)
                {
                    $item_delete_ole[$_del_garage_item->$pk_id]=1;
                }
            }

            $item_new=array();
            $item_delete=array();
            $item_update=array();
            foreach ($properties as $index=>$item_id)
            {
                if(!isset($item_selected[$item_id]))
                {
                    if(!isset($item_delete_ole[$item_id]))
                    {
                        $insert=array(
                            'garage_id'=>$garage_id,
                            $pk_id=>$item_id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        );
                        if(!empty($options) && isset($options[$item_id]) && !empty($options[$item_id]))
                        {
                            $insert['service_detail']=$options[$item_id];
                        }
                        $item_new[]=$insert;
                    }
                    else
                    {
                        $update=array(
                            'updated_at'=>date('Y-m-d H:i:s'),
                            'deleted_at'=>Null
                        );
                        if(!empty($options) && isset($options[$item_id]))
                        {
                            $update['service_detail']=$options[$item_id];
                        }
                        $item_update[$item_id]=$update;
                    }
                }
                else
                {
                    if(!empty($options) && isset($options[$item_id]))
                    {
                        $update['service_detail']=$options[$item_id];
                        $item_update[$item_id]=$update;
                    }
                }
            }
            foreach ($item_selected as $item_id=>$_pk_id)
            {
                if(!in_array($item_id,$properties))
                {
                    $item_delete[]=$item_id;
                }
            }
            if(!empty($item_new))
            {
                ProfileModel::$func_insert($item_new,$type);
            }
            if(!empty($item_delete))
            {
                ProfileModel::$func_deleted($garage_id,$pk_id,$item_delete,$type);
            }
            if (!empty($item_update))
            {
                ProfileModel::$func_update($garage_id,$pk_id,$item_update,$type);
            }
        }
        else
        {
            if ($type=='category')
            {
                $table='garages_categories';
            }
            else
            {
                $table='garages_'.$type.'s';
            }

            DB::table($table)->where('garage_id',$garage_id)->update(['deleted_at'=>date('Y-m-d H:i:s')]);
        }
    }
    public static function update_image($garage_id,$images,$image_names)
    {
        if(!empty($images))
        {
            $old_images = DB::table('images')->where('garage_id',$garage_id)->whereNull('deleted_at')->get();
            $ole_selected = array();
            foreach($images as $index=>$image)
            {
                if(!is_numeric($image))
                {
                    $original_name = isset($image_names[$index])?$image_names[$index]:'';
                    DB::table('images')->insert([
                        'image_path'=>$image,
                        'original_name'=>$original_name,
                        'garage_id'=>$garage_id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
                else
                {
                    $ole_selected[]=$image;
                }
            }
            if(!empty($old_images))
            {
                foreach($old_images as $item)
                {
                    if(!in_array($item->image_id,$ole_selected))
                    {
                        DB::table('images')
                            ->where('image_id',$item->image_id)
                            ->update([
                            'deleted_at'=>date('Y-m-d')
                        ]);
                    }
                }
            }
        }
    }
}