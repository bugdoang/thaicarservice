<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/8/2559
 * Time: 11:46
 */

Route::group(['middleware'=>['auth']],function(){
    Route::get('profile','\App\Modules\Profile\Controllers\Profile@index');
    Route::put('profile/{id}','\App\Modules\Profile\Controllers\UpdateProfile@index');
    Route::put('garageprofile/{id}','\App\Modules\Profile\Controllers\UpdateGarageProfile@index');
    Route::put('password/{id}','\App\Modules\Profile\Controllers\UpdatePassword@index');
    Route::delete('garageprofile/{garage_id}', '\App\Modules\Profile\Controllers\DeleteGarage@delete');
});