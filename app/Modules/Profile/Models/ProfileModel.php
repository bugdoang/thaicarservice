<?php
namespace App\Modules\Profile\Models;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 26/8/2559
 * Time: 15:38
 */
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

//use DB;

class ProfileModel extends Model
{
    public static function me($member_id)
    {
        return DB::table('members')
            ->whereNull('deleted_at')
            ->where('member_id',$member_id)
            ->first();
    }

    public static function  garage_me($member_id)
    {
        return DB::table('garages')
            ->whereNull('deleted_at')
            ->where('member_id',$member_id)
            ->get();
    }
    public static function  garage_id($id)
    {
        return DB::table('garages')
            ->whereNull('deleted_at')
            ->where('garage_id',$id)
            ->first();
    }
    public static function  insurances_me($garage_id)
    {
        return DB::table('garages_insurances')
            ->whereNull('deleted_at')
            ->where('garage_id',$garage_id)
            ->get();
    }
    public static function  services_me($garage_id)
    {
        return DB::table('garages_services')
            ->whereNull('deleted_at')
            ->where('garage_id',$garage_id)
            ->get();
    }
    public static function  categories_me($garage_id)
    {
        return DB::table('garages_categories')
            ->whereNull('deleted_at')
            ->where('garage_id',$garage_id)
            ->get();
    }
    public static function  brands_me($garage_id)
    {
        return DB::table('garages_brands')
            ->whereNull('deleted_at')
            ->where('garage_id',$garage_id)
            ->get();
    }

    public static function update_profile($member_id,$item)
    {
        return DB::table('members')
            ->where('member_id',$member_id)
            ->update($item);
    }
    public static function update_garage($garage_id,$item)
    {
        return DB::table('garages')
            ->where('garage_id',$garage_id)
            ->update($item);
    }
    //--------------เริ่มอัพเดตข้อมูลประกัน----------------------
    public static function insert_property($item,$table)
    {
        $table = ($table=='category')?'categorie':$table;
        return DB::table('garages_'.$table.'s')
            ->insert($item);
    }
    public static function deleted_property($garage_id,$key_type,$ids,$table)
    {
        $_table = ($table=='category')?'categorie':$table;
        if(!empty($ids))
        {
             DB::table('garages_'.$_table.'s')
                 ->where('garage_id',$garage_id)
                 ->whereIn($key_type,$ids)
                 ->update(['deleted_at'=>date('Y-m-d H:i:s')]);
        }
    }
    public static function get_property($garage_id,$table,$delete=null)
    {
        $_table = ($table=='category')?'categorie':$table;
        if(empty($delete))
        {
            return DB::table('garages_'.$_table.'s')
                ->where('garage_id',$garage_id)
                ->whereNull('deleted_at')//ดึงข้อมุลเก่าว่าอันเก่ามีอะไรและอันใหม่มีอะไร จะได้ไม่ซ้ำ
                ->get();
        }
        else
        {
            return DB::table('garages_'.$_table.'s')
                ->where('garage_id',$garage_id)
                ->whereNotNull('deleted_at')//ดึงข้อมุลเก่าว่าอันเก่ามีอะไรและอันใหม่มีอะไร จะได้ไม่ซ้ำ
                ->get();
        }
    }
//    public static function update_property($items,$table)
//    {
//        $_table = ($table=='category')?'categorie':$table;
//
//        if(!empty($items))
//        {
//            foreach ($items as $id=>$item)
//            {
//                DB::table('garages_'.$_table.'s')
//                    ->where('garage_'.$table.'_id',$id)
//                    ->update($item);
//            }
//        }
//    }
    public static function update_property($garage_id,$key_type,$items,$table)
    {
        $_table = ($table=='category')?'categorie':$table;

        if(!empty($items))
        {
            foreach ($items as $id=>$item)
            {
                DB::table('garages_'.$_table.'s')
                    ->where('garage_id',$garage_id)
                    ->where($key_type,$id)
                    ->update($item);
            }
        }
    }

    //--------------การลบอู่----------------------
    public static function delete_garage($garage_id)
    {
        DB::table('garages_insurances')
            ->where('garage_id', '=', $garage_id)
            ->update(['garages_insurances.deleted_at'=>date('Y-m-d H:i:s')]);
        DB::table('garages_services')
            ->where('garage_id', '=', $garage_id)
            ->update(['garages_services.deleted_at'=>date('Y-m-d H:i:s')]);
        DB::table('garages_categories')
            ->where('garage_id', '=', $garage_id)
            ->update(['garages_categories.deleted_at'=>date('Y-m-d H:i:s')]);
        DB::table('garages_brands')
            ->where('garage_id', '=', $garage_id)
            ->update(['garages_brands.deleted_at'=>date('Y-m-d H:i:s')]);
        return DB::table('garages')
            ->where('garage_id', '=', $garage_id)
            ->update(['garages.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    public static function check_email($email)
    {
        return DB::table('members')
            ->where('email',$email)
            ->first();
    }
    public static function check_password($member_id,$old_password)
    {
        $pass=DB::table('members')
            ->where('member_id',$member_id)
            ->first();
        if(!empty($pass))
        {
            if (Auth::attempt(array('email' => $pass->email, 'password' => $old_password))) {
                return true;
            }
        }
        return false;
    }
    public static function update_password($member_id,$item)
    {
        return DB::table('members')
            ->where('member_id',$member_id)
            ->update($item);
    }
}

