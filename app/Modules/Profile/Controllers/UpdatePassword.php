<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/9/2559
 * Time: 10:27
 */

namespace App\Modules\Profile\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Profile\Models\ProfileModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UpdatePassword extends Controller
{
    public function index(Request $request)
    {
        $member_id      =Auth::user()->member_id;
        $old_password   =$request->get('old_password');
        $new_password   =$request->get('new_password');
        $re_password    =$request->get('re_password');
        $check_pass     =ProfileModel::check_password($member_id,$old_password);
//        dd($check_pass);
        $check=array('new_password','old_password','re_password');
        foreach ($check as $item)
        {
            if ($request->get ($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ'],422);
            }
        }
        if(!$check_pass)
        {
            return response(['รหัสผ่านเดิมไม่ถูกต้อง กรุณาป้อนใหม่อีกครั้งค่ะ'],422);
        }
        if($new_password!=$re_password)
        {
            return response(['การยืนยันรหัสผ่านไม่ถูกต้อง กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }

        ProfileModel::update_password($member_id,[
            'password'  =>Hash::make($new_password),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['บันทึกข้อมูลเรียบร้อยแล้วค่ะ']);
    }

}