<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Profile\Controllers;
use App\Helepers\DateFormat;
use App\Http\Controllers\Controller;
use App\Modules\Brand\Models\BrandModel;
use App\Modules\Category\Models\CategoryModel;
use App\Modules\Image\Models\ImageModel;
use App\Modules\Insurance\Models\InsuranceModel;
use App\Modules\Profile\Models\ProfileModel;
use App\Modules\Province\Models\ProvinceModel;
use App\Modules\Service\Models\ServiceModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class Profile extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index(Request $request)
    {
        $member_id = Auth::user()->member_id; //ตรวจสอบการล็อคอิน
        $my_profile = ProfileModel::me($member_id); //เอาค่าเมมเบอร์ไอดีที่อยู่ในตัวแปรโปรไฟล์โมเดล มาเก็บไว้ในตัวแปรมายโปรไฟล์
        $title_names = config('myconfig.title_names');
        $open_time = config('myconfig.open_time');
        $close_time = config('myconfig.close_time');
        $open_garages = config('myconfig.open_garages');
        $provinces=ProvinceModel::getAll();
        $garage_me=ProfileModel::garage_me($member_id);
        $display_info=true;
        if (!empty ($garage_me))//ถ้าUserมีอู่ให้แสดงอู่ค่าแรกก่อน แต่ถ้าไม่มีให้เป็นค่าว่างแล้วหน้าก็จะโชว์หน้าส่วนตัว !empty คือ ถ้าค่างไม่ว่างให้โชว์อู่
        {
            $garage_selected=$garage_me[0];
        }
        else
        {
            $garage_selected=Null;
        }

        if(is_numeric($request->garage_id))
        {
            $garage_selected=ProfileModel::garage_id($request->garage_id);
//            $display_info=false;
            if(empty($garage_selected))
            {
                $garage_selected=$garage_me[0];
                $display_info=true;
            }
            else
            {
                $display_info=false;
            }
        }


        $insurance=InsuranceModel::getAllInsurance();
        $insurances_selected=[];

        $service=ServiceModel::getAllService();
        $services_selected=[];

        $category=CategoryModel::getAllCategory();
        $categories_selected=[];

        $brand=BrandModel::getAllBrand();
        $brands_selected=[];

        $images=[];
        $status=[];

        if(!empty($garage_selected))
        {

            $insurances_me=ProfileModel::insurances_me($garage_selected->garage_id);
            if(!empty($insurances_me))
            {
                foreach ($insurances_me as $item)
                {
                    $insurances_selected[]=$item->insurance_id;
                }
            }

            $services_me=ProfileModel::services_me($garage_selected->garage_id);
            if(!empty($services_me))
            {
                foreach ($services_me as $item)
                {
                    $services_selected[$item->service_id]=$item;
                }
            }

            $categories_me=ProfileModel::categories_me($garage_selected->garage_id);
            if(!empty($categories_me))
            {
                foreach ($categories_me as $item)
                {
                    $categories_selected[]=$item->category_id;
                }
            }

            $brands_me=ProfileModel::brands_me($garage_selected->garage_id);
            if(!empty($brands_me))
            {
                foreach ($brands_me as $item)
                {
                    $brands_selected[]=$item->brand_id;
                }
            }

            $images=ImageModel::getAllImage($garage_selected->garage_id);
            if (empty($brands_me && $categories_me && $services_me))
            {
                $status='noshow';
            }
            else
            {
                $status='show';
            }
        }
        if(!empty($my_profile->dob))//เรียกใช้ฟังก์ชั่น วันที่ที่แปลงค่าไว้ข้างบน
        {
            $my_profile->dob=DateFormat::thaiDate($my_profile->dob);
        }

//        dd($insurance);
        return view ('profile::profile',[ //ตัวแรกโฟลเดอที่เราประกาศไว้ในโมดูลโปรไวเดอะ ตัวสองโลเดอ
            'my_profile'=>$my_profile, //เก็บค่าตัวแปรมายโปรไฟล์ไว้ เวลาเรียกใช้ให้เรียกใช้ชื่อได้เลย
            'provinces'=>$provinces,
            'title_names'=>$title_names,
            'open_time'=>$open_time,
            'close_time'=>$close_time,
            'open_garages'=>$open_garages,
            'garage_me'=>$garage_me,
            'garage_selected'=>$garage_selected,
            'display_info'=>$display_info,
            'insurances'=>$insurance,
            'insurances_selected'=>$insurances_selected,
            'services'=>$service,
            'services_selected'=>$services_selected,
            'categories'=>$category,
            'categories_selected'=>$categories_selected,
            'brands'=>$brand,
            'brands_selected'=>$brands_selected,
            'images'=>$images,
            'status'=>$status,
        ]);
    }
}
