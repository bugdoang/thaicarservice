<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/9/2559
 * Time: 17:50
 */

namespace App\Modules\Profile\Controllers;


use App\Modules\Profile\Models\ProfileModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Profile\Services\GarageProperty;

class UpdateGarageProfile extends Controller
{
        public function index($garage_id,Request $request)
        {
            $garage_name=$request->get('garage_name');
            $address=$request->get('address');
            $province_id=$request->get('province_id');
            $latitude=$request->get('latitude');
            $longitude=$request->get('longitude');
            $email=$request->get('email');
            $mobile=$request->get('mobile');
            $txtDescription=$request->get('txtDescription');
            $website=$request->get('website');
            $open_time=$request->get('open_time');
            $close_time=$request->get('close_time');
            $open_garages=$request->get('open_garage');

            $chk_insurance=$request->get('chk_insurance');
            $chk_service=$request->get('chk_service');
            $service_detail=$request->get('service_detail');
            $chk_category=$request->get('chk_category');
            $chk_brand=$request->get('chk_brand');
            $check=array('garage_name','address','province_id','email','mobile');
            foreach ($check as $item)
            {
                if ($request->get ($item)=='')
                {
                    return response(['กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ'],422);
                }
            }
//            $check_data_garage=array('chk_service','chk_category','chk_brand');
//            foreach ($check_data_garage as $item)
//            {
//                if ($request->get ($item)=='')
//                {
//                    return response(['กรุณาระบุงานที่ให้บริการ/ประเภทรถยนต์/ยี่ห้อรถยนต์ อย่างน้อย 1 รายการค่ะ'],422);
//                }
//            }
            if (empty($chk_service))
            {
                return response(['กรุณาเลือกงานที่ให้บริการอย่างน้อย 1 ตัวเลือกค่ะ'],422);
            }
            if (empty($chk_category))
            {
                return response(['กรุณาเลือกประเภทรถยนต์อย่างน้อย 1 ตัวเลือกค่ะ'],422);
            }
            if (empty($chk_brand))
            {
                return response(['กรุณาเลือกยี่ห้อรถยนต์อย่างน้อย 1 ตัวเลือกค่ะ'],422);
            }
            if (empty($txtDescription))
            {
                return response(['กรุณากรอกคำแนะนำอู่ซ่อมรถยนต์ด้วยค่ะ'],422);
            }
            if (!is_numeric($province_id))
            {
                return response(['กรุณาป้อนจังหวัดให้ถูกต้องด้วยค่ะ']);
            }
            $open_select=[];
            if(!empty($open_garages))
            {
                $open_select=[];

                    foreach ($open_garages as $open_garage)
                    {
                        if(!empty($open_garage))
                        {
                            $open_select[]=$open_garage;
                        }
                    }
                if(count($open_select)==7)
                {
                    $open_select=['เปิดทุกวัน'];
                }
            }
//            dd($open_select);

            ProfileModel::update_garage($garage_id,[
                'garage_name'=>$garage_name,
                'address'=>$address,
                'province_id'=>$province_id,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'email'=>$email,
                'mobile'=>$mobile,
                'detail'=>$txtDescription,
                'website'=>$website,
                'open_time'=>$open_time,
                'close_time'=>$close_time,
                'open_garages'=>join(',',$open_select),
                'updated_at'=>date('Y-m-d H:i:s'),
            ]);
            GarageProperty::update($chk_insurance,$garage_id,'insurance');
            GarageProperty::update($chk_service,$garage_id,'service',$service_detail);
            GarageProperty::update($chk_category,$garage_id,'category');
            GarageProperty::update($chk_brand,$garage_id,'brand');
            GarageProperty::update_image($garage_id,$request->get('image'),$request->get('image_name'));

            return response(['บันทึกข้อมูลเรียบร้อยแล้วค่ะ']);
        }
}