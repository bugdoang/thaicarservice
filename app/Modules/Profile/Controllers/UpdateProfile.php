<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/9/2559
 * Time: 17:22
 */

namespace App\Modules\Profile\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Profile\Models\ProfileModel;
use App\Helepers\DateFormat;
use Illuminate\Http\Request;

class UpdateProfile extends Controller
{
    public function index($member_id,Request $request)
    {
        $title_name =$request->get('title_name');
        $first_name =$request->get('first_name');
        $last_name  =$request->get('last_name');
        $dob        =$request->get('dob');
        $card_id    =$request->get('card_id');
        $gender     =$request->get('gender');
        $mobile     =$request->get('mobile');
        $address    =$request->get('address');
        $province_id=$request->get('province_id');
        $check=array('title_name','first_name','last_name','dob','card_id','gender','mobile','address','province_id');
        foreach ($check as $item)
        {
            if ($request->get ($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ'],422);
            }
        }

        if (!is_numeric($province_id))
        {
            return response(['กรุณาป้อนจังหวัดให้ถูกต้องด้วยค่ะ']);
        }
        ProfileModel::update_profile($member_id,[
            'title_name'=>$title_name,
            'first_name'=>$first_name,
            'last_name' =>$last_name,
            'dob'       =>DateFormat::engDate($dob),
            'card_id'   =>$card_id,
            'gender'   =>$gender,
            'mobile'    =>$mobile,
            'address'   =>$address,
            'province_id'=>$province_id,
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        return response(['บันทึกข้อมูลเรียบร้อยแล้วค่ะ']);
    }
}