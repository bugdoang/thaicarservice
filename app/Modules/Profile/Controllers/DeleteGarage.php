<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20/9/2559
 * Time: 16:47
 */

namespace App\Modules\Profile\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Garage\Models\GarageModel;
use App\Modules\Profile\Models\ProfileModel;
use Illuminate\Support\Facades\Redirect;

class DeleteGarage extends Controller
{
    public function delete($garage_id)
    {
        ProfileModel::delete_garage($garage_id);
        return response(['ลบข้อมูลอู่เรียบร้อยแล้วค่ะ']);

    }
}