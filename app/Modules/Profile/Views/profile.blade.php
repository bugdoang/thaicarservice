@extends('masterlayout')
@section('title','ข้อมูลส่วนตัว '.$my_profile->first_name)
@section('content')

<!-- Page Content -->

<div class="row">
        <!-- เริ่มซ้าย-->
        <div class="col-lg-9 col-md-9">
            <div class="row">
                <h2 class="header-text myfont color2"><i class="fa fa-user" aria-hidden="true"></i> ข้อมูลส่วนตัว
                    <div class="btn-group pull-right">
                        <button type="button" class="btn color3 dropdown-toggle myfont" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            รายการอู่ของฉัน ({{count($garage_me)}} รายการ) <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu  myfont">
                            <li><a href="garage">ลงทะเบียนอู่ใหม่</a></li>
                            @if (!empty($garage_me))
                                @foreach($garage_me as $garage)
                                    <li><a href="/profile?garage_id={{$garage->garage_id}}">{{$garage->garage_name}}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </h2>

                <div class="item-border">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            @if(!empty($my_profile->avatar))
                            <img id="avatar" src="{{$my_profile->avatar}}" class="img-profile">
                            @else
                                @if(($my_profile->gender)=='F')
                                    <img id="avatar" src="/assets/img/no-f.png" class="img-profile">
                                @elseif(($my_profile->gender)=='M')
                                    <img id="avatar" src="/assets/img/no-m.png" class="img-profile">
                                @endif
                            @endif
                                <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <button  data-profile="profile" class="button-picture myfont uploadFile">
                                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                                        เลือกรูปภาพ
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-9 col-lg-9 ">
                            <div class="row">
                                <div class="well">
                                    <ul class="nav nav-tabs">
                                        <li class="{{(!empty($display_info))?'active':''}}"><a href="#profile" data-toggle="tab" class="myfont tabbar" style="font-size: 19px">ข้อมูลส่วนตัว</a></li>
                                        <li><a href="#editpassword" data-toggle="tab" class="myfont tabbar"style="font-size: 19px">แก้ไขรหัสผ่าน</a></li>
                                        @if (!empty($garage_selected))
                                            <li class="{{(empty($display_info))?'active':''}}"><a href="#detail" data-toggle="tab" class="myfont tabbar"style="font-size: 19px" >รายละเอียดอู่ {{$garage_selected->garage_name}}</a></li>
                                        @endif
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        @include('profile::info')
                                        @include('profile::password')
                                        @if (!empty($garage_me))
                                            @include('profile::garageprofile')
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- จบซ้าย-->
        </div>
    {!! App\Services\SearchBox::get() !!}

</div>
@stop