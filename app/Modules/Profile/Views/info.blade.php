<div class="tab-pane {{(!empty($display_info))?'active in':''}}" id="profile">
    <form id="fm-profile" method="PUT" action="/profile/{{$my_profile->member_id}}">
        {{ csrf_field() }}
        <div class="col-md-12 col-lg-12">
            <div class="row" style="margin-top: 15px;">
                <div class="form-group col-md-4 col-lg-4" style="padding-left: 0px;">
                    <label>คำนำหน้าชื่อ: </label>
                    <select class="form-control" name="title_name" id="title_name">
                        <option value="">กรุณาเลือก</option>
                        @if(!empty($title_names))
                            @foreach($title_names as $title_name)
                                <option {{($my_profile->title_name==$title_name)?' selected ':''}} value="{{$title_name}}">
                                    {{$title_name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 col-lg-6">
                <label>ชื่อ: </label>
                <input class="form-control" type="text" name="first_name" id="first_name" value="{{$my_profile->first_name}}"/>
            </div>

            <div class="form-group col-md-6 col-lg-6">
                <label>นามสกุล: </label>
                <input class="form-control" type="text" name="last_name" id="last_name" value="{{$my_profile->last_name}}"/>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6 col-lg-6">
                <label>วัน/เดือน/ปี เกิด: </label>
                <input placeholder="ถึงวันที่"  value="{{$my_profile->dob}}" name="dob" id="dob" class="form-control datepicker"/>
                {{--<input class="form-control datepicker" type="text" name="dob" id="dob" value="{{$my_profile->dob}}" >--}}
            </div>
            <div class="form-group col-md-6 col-lg-6">
                <label>เพศ: </label></br>
                <input {{($my_profile->gender=='M')?'checked':''}} name="gender" type="radio" value="M"> ชาย &nbsp; &nbsp; &nbsp;
                <input {{($my_profile->gender=='F')?'checked':''}} name="gender" type="radio" value="F"> หญิง
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 col-lg-6">
                <label>เลขบัตรประชาชน: </label>
                <input class="form-control" type="text" name="card_id" id="card_id" maxlength="13" value="{{$my_profile->card_id}}"/>
            </div>
            <div class="form-group col-md-6 col-lg-6">
                <label>เบอร์โทรศัพท์: </label>
                <input class="form-control" type="text" name="mobile" id="mobile" value="{{$my_profile->mobile}}"/>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <div class="form-group">
                    <label>ที่อยู่:</label>
                    <input class="form-control" type="text" name="address" id="address" value="{{$my_profile->address}}" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <label>จังหวัด: </label>
                <select class="form-control" name="province_id" id="province_id">
                    <option value="">กรุณาเลือก</option>


                    @if(!empty($provinces))
                        @foreach($provinces as $province)
                            <option {{($my_profile->province_id==$province->province_id)?' selected ':''}} value="{{$province->province_id}}">
                                {{$province->province_name}}
                            </option>
                        @endforeach
                    @endif


                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <button type="submit" class="button-save myfont">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    บันทึก
                </button>
            </div>
        </div>
    </form>
</div>
