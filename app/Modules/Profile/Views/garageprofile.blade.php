<div class="tab-pane fade {{(empty($display_info))?'active in':''}}" id="detail">
    <form id="fm-garage" method="PUT" action="/garageprofile/{{$garage_selected->garage_id}}">
    {{ csrf_field() }}
        <!--ข้อมูลพื้นฐาน-->
        <div class="header-detail myfont">
            <div class="col-lg-1 col-md-1"  style="padding-left: 0px; padding-right: 0px;">
                สถานะ :
            </div>
            @if($status=='show')
                <div style="color:green;">
                    <i class="fa fa-check" aria-hidden="true"></i> เผยแพร่ข้อมูลอู่ซ่อมรถยนต์
                    <span>
                     <a type="#" href="/product-detail/{{$garage_selected->garage_id}}">
                        ดูข้อมูลอู่ซ่อมรถยนต์
                    </a>
                    </span>
                </div>
            @else
                <div style="color:red;">
                    <i class="fa fa-times" aria-hidden="true"></i> ยังไม่เผยแพร่ข้อมูลอู่ซ่อมรถยนต์ กรุณากรอกข้อมูลอู่ซ่อมรถยนต์ให้ครบ
                </div>
            @endif
        </div>
        <div class="header-detail myfont line1">
            คำแนะนำ/ประวัติ อู่ซ่อมรถ <span style="color:red;"> * </span>
            <div class="pull-right">
                <div class="col-lg-12 col-md-12">
                    <a type="#" href="garageprofile/{{$garage_selected->garage_id}}" class="button-delete myfont delete-content">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        ลบข้อมูลอู่
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 listservice">
                <textarea name="txtDescription" id="txtDescription" cols="20" rows="3" class="text-area1 ">{{$garage_selected->detail}}</textarea>
            </div>
        </div>
        <div class="header-detail myfont line1">
            ข้อมูลพื้นฐาน
        </div>
        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <label>ชื่ออู่ซ่อมรถ <span style="color:red;"> * </span> : </label>
                <input class="form-control" type="text" name="garage_name" value="{{$garage_selected->garage_name}}"/>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-8 col-lg-8">
                <label><i class="fa fa-home" aria-hidden="true"></i> ที่ตั้งอู่ซ่อมรถ <span style="color:red;"> * </span> : </label>
                <input class="form-control" type="text" name="address" value="{{$garage_selected->address}}"/>
            </div>
            <div class="col-lg-4 col-md-4">
                <label>จังหวัด <span style="color:red;"> * </span> :</label>
                <select class="form-control" name="province_id" id="province_id">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($provinces))
                        @foreach($provinces as $province)
                            <option {{($garage_selected->province_id==$province->province_id)?' selected ':''}} value="{{$province->province_id}}">
                                {{$province->province_name}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-8 col-lg-8">
                <label><i class="fa fa-envelope-o" aria-hidden="true"></i> อีเมล์ <span style="color:red;"> * </span> :</label>
                <input class="form-control" type="text" name="email" value="{{$garage_selected->email}}" />
            </div>
            <div class="form-group col-md-4 col-lg-4">
                <label><i class="fa fa-phone" aria-hidden="true"></i> เบอร์โทรศัพท์อู่ซ่อมรถ <span style="color:red;"> * </span> :</label>
                <input class="form-control" type="text" name="mobile" value="{{$garage_selected->mobile}}" />
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <label><i class="fa fa-globe" aria-hidden="true"></i> เว็บไซต์ :</label>
                <input class="form-control" type="text" name="website" value="{{$garage_selected->website}}" />
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3 col-lg-3"  style="margin-bottom: 5px;">
                <label><i class="fa fa-calendar" aria-hidden="true"></i> วันเปิดทำการ :</label>
            </div>
            <?php $open = (!empty($garage_selected->open_garages))?explode(',',$garage_selected->open_garages):[]; ?>
            <div class="form-group col-md-4 col-lg-4" style="padding-left: 12px; margin-bottom: 0px;">
                <input type="checkbox" class="open-all" {{(in_array('เปิดทุกวัน',$open))?'checked':''}}> เปิดทุกวัน
            </div>
            <div class="col-md-12 col-lg-12"  style="padding-left: 0px;">
                @if(!empty($open_garages))
                    @foreach($open_garages as $open_garage)
                        <div class="col-lg-3 col-md-3">
                            <label class="font-original">
                                <input type="checkbox" name="open_garage[]" class="open-day" {{(in_array($open_garage,$open) || in_array('เปิดทุกวัน',$open)?'checked':'')}} value="{{$open_garage}}">
                                {{$open_garage}}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 col-lg-6">
                <label><i class="fa fa-clock-o" aria-hidden="true"></i> เวลาเปิด :</label>
                <select class="form-control" name="open_time" id="open_time">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($open_time))
                        @foreach($open_time as $time)
                            <option {{($garage_selected->open_time==$time)?'selected':''}} value="{{$time}}">
                                {{$time}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group col-md-6 col-lg-6">
                <label><i class="fa fa-clock-o" aria-hidden="true"></i> เวลาปิด :</label>
                <select class="form-control" name="close_time" id="close_time">
                    <option value="">กรุณาเลือก</option>
                    @if(!empty($close_time))
                        @foreach($close_time as $time)
                            <option {{($garage_selected->close_time==$time)?' selected ':''}} value="{{$time}}">
                                {{$time}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <label><i class="fa fa-map-marker" aria-hidden="true"></i> ตำแหน่งร้าน : </label>
            </div>
            <div class="form-group col-md-12 col-lg-12">
                <input id="searchTextField" class="form-control" type="text" size="50" style="text-align: left;width:100%;direction: ltr;" placeholder=" ค้นหาตำแหน่ง">
            </div>
            <div class="form-group col-md-6 col-lg-6">
                latitude:<input name="latitude" class="MapLat form-control" value="{{$garage_selected->latitude}}" type="text" placeholder="Latitude" style="width:100%;" >
            </div>
            <div class="form-group col-md-6 col-lg-6">
                longitude:<input name="longitude" class="MapLon form-control" value="{{$garage_selected->longitude}}" type="text" placeholder="Longitude" style="width:100%;" >
            </div>

            <div id="map_canvas" style="height: 350px;width: 97%;margin: 0.6em;"></div>
        </div>



        <div class="row">

                <!--การบริการ-->

                <div class="form-group col-lg-12 col-md-12">
                    @if(!empty($services))
                        <div class="header-detail line1">
                            <div class="myfont">
                                ข้อมูลงานที่ให้บริการ <span style="color:red;"> * </span>
                            </div>
                            <div class="pull-right">
                                <div class="col-lg-12 col-md-12">
                                    <a type="#" href="/contact" class="pull-right"  style="font-size: 12px; margin-top: -17px;">
                                        ข้อมูลอื่นๆ คลิกที่นี่
                                    </a>
                                </div>
                            </div>
                        </div>

                        @foreach($services as $service)
                        <div class="row">
                            <div class="col-lg-4 col-md-4" style="padding-right: 0px; padding-top: 7px;">
                                <label class="font-original">
                                    <input type="checkbox" name="chk_service[]" {{(isset($services_selected[$service->service_id]))?'checked':''}} value="{{$service->service_id}}">
                                    {{$service->service_name}}
                                </label>
                            </div>
                            <div class="col-lg-8 col-md-8 listservice">
                                <input type="text" name="service_detail[{{$service->service_id}}]" class="form-control" value="{{(isset($services_selected[$service->service_id]))?$services_selected[$service->service_id]->service_detail:''}}">
                            </div>
                        </div>
                        @endforeach
                    @endif

                    @if(!empty($categories))
                        <div class="header-detail line1">
                            <div class="myfont">
                                ข้อมูลประเภทรถที่รับซ่อม <span style="color:red;"> * </span>
                            </div>
                            <div class="pull-right">
                                <div class="col-lg-12 col-md-12">
                                    <a type="#" href="/contact" class="pull-right"  style="font-size: 12px; margin-top: -17px;">
                                        ข้อมูลอื่นๆ คลิกที่นี่
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($categories as $category)
                                @if($category->category_name=='ไม่ระบุ')
                                @else
                                    <div class="col-lg-3 col-md-3">
                                        <label class="font-original">
                                            <input type="checkbox" name="chk_category[]" {{(in_array($category->category_id,$categories_selected))?'checked':''}} value="{{$category->category_id}}">
                                            {{$category->category_name}}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif

                    @if(!empty($brands))
                    <div class="header-detail line1">
                        <div class="myfont">
                            ข้อมูลยี่ห้อรถที่รับซ่อม <span style="color:red;"> * </span>
                        </div>
                        <div class="pull-right">
                            <div class="col-lg-12 col-md-12">
                                <a type="#" href="/contact" class="pull-right"  style="font-size: 12px; margin-top: -17px;">
                                    ข้อมูลอื่นๆ คลิกที่นี่
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($brands as $brand)
                            @if($brand->brand_name=='ไม่ระบุ')

                            @else
                        <div class="col-lg-3 col-md-3">
                            <label class="font-original">
                                <input type="checkbox" name="chk_brand[]" {{(in_array($brand->brand_id,$brands_selected))?'checked':''}} value="{{$brand->brand_id}}">
                                {{$brand->brand_name}}
                            </label>
                        </div>
                            @endif
                        @endforeach
                    </div>
                    @endif

                    <!--ประกัน-->
                        @if(!empty($insurances))
                            <div class="header-detail line1">
                                <div class="myfont">
                                    ข้อมูลบริษัทประกันภัยที่รับซ่อม
                                </div>
                                <div class="pull-right">
                                    <div class="col-lg-12 col-md-12">
                                        <a type="#" href="/contact" class="pull-right"  style="font-size: 12px; margin-top: -17px;">
                                            ข้อมูลอื่นๆ คลิกที่นี่
                                        </a>
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                    @foreach($insurances as $insurance)
                                        @if($insurance->insurance_name=='ไม่ระบุ')

                                        @else
                                        <div class="col-lg-4 col-md-4">
                                            <label class="font-original">
                                                <input type="checkbox" name="chk_insurance[]" {{(in_array($insurance->insurance_id,$insurances_selected))?'checked':''}} value="{{$insurance->insurance_id}}">
                                                {{$insurance->insurance_name}}
                                            </label>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                        @endif
                        <div class="header-detail myfont line1" >
                            รูปภาพอู่ของท่าน
                            <div class="pull-right" >
                            <button class="button-picture myfont uploadFile" style="margin-top: 2px; margin-bottom: 15px; padding-bottom: 0px; padding-top: 0px;">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                    เพิ่มรูปภาพ
                            </button>
                            </div>
                        </div>

                        <div class="row" id="image-lists" style="padding-left: 15px; padding-right: 15px;">
                        @if(!empty($images))
                            @foreach($images as $image)
                                <div class="col-lg-4 col-md-4"  style="padding-left: 0px; padding-right: 0px; margin-left: 0px; margin-right: 0px;">
                                    <input type="hidden" name="image[]" value="{{$image->image_id}}">
                                    <input type="hidden" name="image_name[]" value="{{$image->original_name}}">
                                    <img src="{{$image->image_path}}" class="img-profile1" style="margin: auto; display: block;">
                                    <a  href="#" class="button-delete myfont delete-image" style="margin-bottom: 10px;">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        ลบรูป
                                    </a>
                                </div>
                            @endforeach
                        @endif
                        </div>

                        <div class="row">
                            <div class="col-lg-10 col-md-10 item-border" style="display:block; text-align:center; color: red; margin-top: 50px; background-color: whitesmoke">
                                กรุณาเลือกข้อมูลประเภทรถยนต์,ข้อมูลยี่ห้อรถยนต์,ข้อมูลงานบริการ อย่างน้อย 1 ตัวเลิอก หากไม่ระบุ ระบบจะไม่นำข้อมูลอู่ซ่อมรถยนต์ของท่านไปแสดงบนเว็บไซต์
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="button-save myfont">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    บันทึก
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
    </form>
</div>
@push('scripts')
<script>
    $(function () {

        var lat ={!! (isset($garage_selected->latitude) && !empty($garage_selected->latitude))?$garage_selected->latitude:13.868148791978353 !!},
            lng ={!! (isset($garage_selected->longitude) && !empty($garage_selected->longitude))?$garage_selected->longitude:100.48204421975242 !!},
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
        var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_left
                }
            },
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
                $('.MapLat').val(place.geometry.location.lat());
                $('.MapLon').val(place.geometry.location.lng());
            });
            google.maps.event.addListener(map, 'click', function (event) {
                $('.MapLat').val(event.latLng.lat());
                $('.MapLon').val(event.latLng.lng());
                infowindow.close();
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "latLng":event.latLng
                }, function (results, status) {
                    console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                            lng = results[0].geometry.location.lng(),
                            placeName = results[0].address_components[0].long_name,
                            latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                    $("#searchTextField").val(results[0].formatted_address);
                }
            });
        });

        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            infowindow.open(map, marker);
        }
        function callback_ajax(data)
        {
            MessageBox.alert(data[0],function () {
                window.location.href='/profile' ;
            });
        }
    });
</script>
@endpush