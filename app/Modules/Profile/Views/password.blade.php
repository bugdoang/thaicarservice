<div class="tab-pane fade" id="editpassword">
    <form id="fm-password" method="PUT" action="/password/{{$my_profile->member_id}}">
        {{ csrf_field() }}
        <div class="row" style="margin-top: 15px;">
            <div class="form-group col-md-12 col-lg-12">
                <label>อีเมล์: </label>
                <input class="form-control" type="text" name="email" id = "email" disabled="disabled"  value=" {{$my_profile->email}}"/>

            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <div class="form-group">
                    <label>รหัสผ่านเดิม: </label>
                    <input  name="old_password" type="password" class="form-control inputpass" minlength="4" maxlength="16"  id="pass1" /> </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <div class="form-group">
                    <label>รหัสผ่านใหม่: </label>
                    <input  name="new_password" type="password" class="form-control inputpass" minlength="4" maxlength="16"  id="pass2" /> </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12 col-lg-12">
                <label>กรอกรหัสผ่านใหม่อีกครั้ง: </label>
                <input  name="re_password" type="password" class="form-control inputpass" minlength="4" maxlength="16"  id="pass3" />
                <span id="confirmMessage" class="confirmMessage"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <button type="submit" class="button-save myfont">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    บันทึก
                </button>
            </div>
        </div>
    </form>
</div>
