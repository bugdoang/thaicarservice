<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Service\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ServiceModel extends Model
{
    /**
     * @return mixed
     */
    public static function getAllService()
{
    return DB::table('services')->whereNull('deleted_at')->get();
}
}