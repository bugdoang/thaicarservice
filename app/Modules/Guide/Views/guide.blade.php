@extends('masterlayout')
@section('title','คำแนะนำ')
@section('content')

    <!-- Page Content -->
    <form id="form-all-guide" name="form-all-guide" class="form-horizontal" role="form" >
        <div class="row">
            <!--start left-->
            <div class="col-lg-3 col-md-3">
                <div class="row">
                    <h2 class="header-text myfont">ประเภทข้อมูล</h2>
                    <div id="rootwizard" class="tabs-left shadow">

                        <div class="item-border " style="padding-top: 0px; padding-left: 0px; padding-bottom: 0px;">
                            <div id="rootwizard" class="tabbable tabs-left">
                                <ul class="manage-menu button-mnm nav-tabs" style="border-bottom: 0px;">
                                    <li class="active">
                                        <a data-toggle="tab" href="#guide-register">สมัครสมาชิก</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-edit-profile">แก้ไขข้อมูลส่วนตัว</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-addgarage">ลงทะเบียนอู่ซ่อมรถยนต์</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-editgarage">แก้ไข/เพิ่มข้อมูลอู่ซ่อมรถยนต์</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-searchgarage">ค้นหาอู่ซ่อมรถยนต์</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-reservation">จองอู่ซ่อมรถยนต์</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-view-reservation">ตรวจสอบข้อมูลการจองของคุณ</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#guide-booking">ตรวจสอบข้อมูลการจองของลูกค้า</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--start right-->
            <div style="padding-right: 0;" class="col-lg-9 col-md-9">
                <div class="tab-content">
                    <div id="guide-register"  class="tab-pane fade in active">
                        <h2 class="header-text myfont">วิธีการสมัครสมาชิก</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/register.jpg" style="width: 600px;">
                        </div>
                    </div>
                    <div id="guide-edit-profile" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการแก้ไขข้อมูลส่วนตัว</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/edit-profile.jpg" style="width: 600px;">
                        </div>
                    </div>
                    <div id="guide-addgarage" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการลงทะเบียนอู่ซ่อมรถยนต์</h2>
                            <div class="item-border" style="padding-top: 0px;">
                                <img  src="/assets/img/guide/add-garage1.jpg" style="width: 600px;">
                                <img  src="/assets/img/guide/add-garage2.jpg" style="width: 600px; margin-top: -26px;">
                                <img  src="/assets/img/guide/add-garage3.jpg" style="width: 600px; margin-top: -46px;">
                            </div>
                    </div>
                    <div id="guide-editgarage" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการแก้ไข/เพิ่มข้อมูลอู่ซ่อมรถยนต์</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/edit-garage1.jpg" style="width: 600px;">
                            <img  src="/assets/img/guide/edit-garage2.jpg" style="width: 600px; margin-top: -17px;">
                        </div>
                    </div>
                    <div id="guide-searchgarage" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการค้นหาอู่ซ่อมรถยนต์</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/search-garage1.jpg" style="width: 600px;">
                            <img  src="/assets/img/guide/search-garage2.jpg" style="width: 600px; margin-top: -62px;">
                        </div>
                    </div>
                    <div id="guide-reservation" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการจองอู่ซ่อมรถยนต์</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/reservation1.jpg" style="width: 600px;">
                            <img  src="/assets/img/guide/reservation2.jpg" style="width: 600px;  margin-top: -21px;">
                        </div>
                    </div>
                    <div id="guide-view-reservation" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการตรวจสอบข้อมูลการจองอู่ซ่อมรถยนต์</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/view-reservation.jpg" style="width: 600px;">
                        </div>
                    </div>
                    <div id="guide-booking" class="tab-pane fade">
                        <h2 class="header-text myfont">วิธีการตรวจสอบข้อมูลการจองของลูกค้า</h2>
                        <div class="item-border" style="padding-top: 0px;">
                            <img  src="/assets/img/guide/booking.jpg" style="width: 600px;">
                            <img  src="/assets/img/guide/booking-2.jpg" style="width: 600px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop
