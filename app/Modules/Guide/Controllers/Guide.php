<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Guide\Controllers;


use App\Http\Controllers\Controller;

use App\Modules\Management\Services\ManagementList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Guide extends Controller
{
    public function index()
    {
        return view('guide::guide');
    }

}