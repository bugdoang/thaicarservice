<?php
namespace App\Modules\Reservation\Services;
use App\Modules\Profile\Models\ProfileModel;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/10/2559
 * Time: 15:59
 */
class ReservationProperty
{
    public static function insert($chk_service,$id)
    {
        if(!empty($chk_service) && is_array($chk_service))
        {
            $item_new=array();
            foreach ($chk_service as $index=>$item_id)
            {
                $insert=array(
                    'reservation_id'=>$id,
                    'service_id'=>$item_id,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                );
                $item_new[]=$insert;

            }
            if(!empty($item_new))
            {
                DB::table('reservations_services')->insert($item_new);
            }
        }
    }
}