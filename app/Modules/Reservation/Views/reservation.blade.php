@extends('masterlayout')
@section('title','จอง'.$garage_detail->garage_name)
@section('content')

<!-- Page Content -->


<div class="row">
    <form action="/reservation/{{$garage_detail->garage_id}}" method="POST" id="fileForm" role="form">
    {{ csrf_field() }}
    <!--เริ่มซ้าย-->
    <div class="register-container1 col-lg-3 col-md-3">
        <div class="panel-title">
            <h2 class="header-text myfont color2">คำแนะนำ</h2>
        </div>
        <div class="item-border">
            <ul class="register-suggestion">
                <li>
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                    เนื้อหาในเว็บไซต์นี้มีขึ้นเพื่อวัตถุประสงค์ในการให้ข้อมูลและเพื่อการศึกษาเท่านั้น
                </li>
                <li>
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                    ผู้จัดทำเว็บไซต์นี้ ไม่ต้องรับผิดต่อการกระทำของผู้เข้าใช้ งานเว็บไซต์
                </li>
                <li>
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                    เว็บไซต์นี้จะไม่รับผิดต่อความสูญเสีย เสียหาย ความรับผิด หรือค่าใช้จ่ายที่เกิดจากการฟ้องร้องสืบเนื่องมาจากการใช้งานเว็บไซต์นี้และข้อมูลใด
                </li>
            </ul>
        </div>
    </div>
        <!-- เริ่มซกลาง-->

        <div class="col-lg-6 col-md-6">
            <div class="row">
                <h2 class="header-text myfont color2">จองอู่ซ่อมรถ</h2>
                <div class="item-border2"  >

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="header-detail myfont line1">
                                ข้อมูลอู่
                            </div>
                            <div class="col-lg-3 col-md-3">
                                ชื่ออู่  :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$garage_detail->garage_name}}
                            </div>

                            <div class="col-lg-3 col-md-3">
                                ที่ตั้ง  :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$garage_detail->address}}
                            </div>

                            <div class="col-lg-3 col-md-3">
                                จังหวัด  :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$garage_detail->province_name}}
                            </div>

                            <div class="col-lg-3 col-md-3">
                                อีเมล์  :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$garage_detail->email}}
                            </div>

                            <div class="col-lg-3 col-md-3">
                                เบอร์โทร :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$garage_detail->mobile}}
                            </div>
                            @if(!empty($garage_detail->open_garages))
                                <div class="col-lg-3 col-md-3">
                                    วันเปิดทำการ :
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    {{$garage_detail->open_garages}}
                                </div>
                            @endif
                            @if(!empty($garage_detail->open_time && $garage_detail->close_time))
                                <div class="col-lg-3 col-md-3">
                                    เวลาเปิด-ปิด :
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    {{$garage_detail->open_time}} - {{$garage_detail->close_time}} น.
                                </div>
                            @endif
                            @if(!empty($services[$garage_detail->garage_id]))
                                <div class="col-lg-3 col-md-3">
                                    รายการที่รับซ่อม :
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    <div class="row">
                                        @if(!empty($services[$garage_detail->garage_id]))
                                            @foreach($services[$garage_detail->garage_id] as $service)
                                                    <div class="col-lg-9 col-md-9">
                                                            - {{$service->service_name}} <BR> &nbsp;
                                                        @if(!empty($service->service_detail))
                                                            ({{$service->service_detail}})
                                                        @endif
                                                    </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endif

                        </div>

                        <div class="col-md-12 col-lg-12 ">
                            <div class="row set">
                                    <div class="header-detail myfont line">
                                        รายละเอียดการจองอู่
                                    </div>
                                <div class="row"  style="margin-left: 15px;">

                                    <div class="row">
                                            <div class="col-lg-3 col-md-3">
                                                วันที่นัด <span style="color:red;"> * </span> :
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                <input placeholder="xx/xx/xxxx" type="text" name="date" id="date" class="form-control datepicker" style="width: 130px;">
                                            </div>
                                            @if(!empty($garage_detail->open_time && $garage_detail->close_time))
                                                <div class="col-lg-2 col-md-2" >
                                                    เวลานัด <span style="color:red;"> * </span> :
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                    <select class="form-control" name="time" id="time">
                                                        <option value="">กรุณาเลือก</option>
                                                        @if(isset($times))
                                                            @foreach($times as $time)
                                                            <option value="{{$time}}">
                                                                {{$time}}
                                                            </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                @else
                                                <div class="col-lg-2 col-md-2" >
                                                    เวลานัด <span style="color:red;"> * </span> :
                                                </div>
                                                    <div class="col-lg-3 col-md-3">
                                                        <select class="form-control" name="time" id="time">
                                                            <option value="">กรุณาเลือก</option>
                                                            @if(isset($time_all))
                                                                @foreach($time_all as $time)
                                                                    <option value="{{$time}}">
                                                                        {{$time}}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                            @endif
                                    </div>

                                    @if(!empty($categories[$garage_detail->garage_id]))
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 listservice">
                                                ประเภทรถ :
                                            </div>
                                            <div class="col-lg-5 col-md-5"  style="margin-top: 10px;">
                                                <select class="form-control" name="chk_category" id="chk_category">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(isset($categories[$garage_detail->garage_id]))
                                                        @foreach($categories[$garage_detail->garage_id] as $item)
                                                            <option value="{{$item->category_id}}">
                                                                {{$item->category_name}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($brands[$garage_detail->garage_id]))
                                        <div class="row" >
                                            <div class="col-lg-3 col-md-3 listservice">
                                                ยี่ห้อรถ :
                                            </div>
                                            <div class="col-lg-5 col-md-5"  style="margin-top: 10px;">
                                                <select class="form-control" name="chk_brand" id="chk_brand">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(isset($brands[$garage_detail->garage_id]))
                                                        @foreach($brands[$garage_detail->garage_id] as $item)
                                                            <option value="{{$item->brand_id}}">
                                                                {{$item->brand_name}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($insurances[$garage_detail->garage_id]))
                                        <div class="row" >
                                            <div class="col-lg-3 col-md-3 listservice">
                                                บริษัทประกันภัย :
                                            </div>
                                            <div class="col-lg-5 col-md-5"  style="margin-top: 10px;">
                                                <select class="form-control" name="chk_insurance" id="chk_insurance">
                                                    <option value="">กรุณาเลือก</option>
                                                    @if(isset($insurances[$garage_detail->garage_id]))
                                                        @foreach($insurances[$garage_detail->garage_id] as $item)
                                                            <option value="{{$item->insurance_id}}">
                                                                {{$item->insurance_name}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    {{--@if(!empty($services[$garage_detail->garage_id]))--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-lg-3 col-md-3 listservice">--}}
                                                {{--รายการซ่อม <span style="color:red;"> * </span> :--}}
                                            {{--</div>--}}
                                            {{--<div class="col-lg-9 col-md-9">--}}
                                                {{--<div class="row" style="margin-left: -60px;">--}}
                                                    {{--@if(!empty($services[$garage_detail->garage_id]))--}}
                                                        {{--@foreach($services[$garage_detail->garage_id] as $service)--}}
                                                            {{--<div class="row" style="margin-right: 30px; height: auto; width: 486px;">--}}
                                                                {{--<div class="col-lg-5 col-md-5" style="padding-right: 0px; padding-top: 7px; padding-left: 75px;">--}}
                                                                    {{--<label class="font-original">--}}
                                                                        {{--<input type="checkbox" name="chk_service[]" value="{{$service->service_id}}">--}}
                                                                        {{--{{$service->service_name}}--}}
                                                                            {{--@if(!empty($service->service_detail))--}}
                                                                                {{--<div class="row">--}}
                                                                                    {{--<div class="col-lg-8 col-md-8 listservice">--}}
                                                                                        {{--<textarea name="service_detail"  rows="1" class="text-area" disabled="disabled" style="height: 24px;">{{$service->service_detail}}</textarea>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                {{--@endif--}}
                                                                    {{--</label>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endforeach--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                         {{--@endif--}}
                                        {{--</div>--}}

                                            <div class="row col-lg-12 col-md-12">
                                                <div class="checkbox1  listservice">
                                                    รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม <span style="color:red;"> * </span> :
                                                </div>
                                            </div>
                                            <div class="row col-lg-12 col-md-12">
                                                <div class="listservice">
                                                    <textarea name="reservation_detail" id="reservation_detail" cols="20" rows="3" style="width: 400px;" class="text-area"></textarea>
                                                </div>
                                            </div>

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <button type="submit" class="button-garage myfont">
                                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                                    บันทึก
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </form>

    {!! App\Services\SearchBox::get() !!}

</div>

@stop
@push('scripts')
<script>
    function callback_ajax(data)
    {
        MessageBox.alert(data[0],function () {
           window.location.href='/status' ;
        });
    }
</script>
@endpush