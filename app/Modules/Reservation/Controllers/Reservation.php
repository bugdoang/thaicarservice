<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Reservation\Controllers;


use App\Helepers\DateFormat;
use App\Http\Controllers\Controller;
use App\Modules\Product\Models\ProductModel;
use Auth;
use Illuminate\Support\Facades\Request;

class Reservation extends Controller
{
public function index($id)
{
    $member_id = Auth::user()->member_id;
    $garage_detail=ProductModel::garage($id);
    $insurances=ProductModel::garages_insurances();
    $services=ProductModel::garages_services();
    $categories=ProductModel::garages_categories();
    $brands=ProductModel::garages_brands();
    $time_all=config('myconfig.open_time');
//dd($categories);
    $time=DateFormat::get_time();
    $times=[];
    foreach ($time as $item)
    {
        if($garage_detail->open_time <= $item && $garage_detail->close_time >= $item)
        {
            $times[]=$item;
        }

    }

    return view ('reservation::reservation',[
        'garage_detail'=>$garage_detail,
        'insurances'=>$insurances,
        'services'=>$services,
        'categories'=>$categories,
        'brands'=>$brands,
        'member_id'=>$member_id,
        'times'=>$times,
        'time_all' =>$time_all

    ]);
}
}