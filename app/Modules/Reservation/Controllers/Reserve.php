<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/10/2559
 * Time: 11:16
 */

namespace App\Modules\Reservation\Controllers;


use App\Helepers\DateFormat;
use App\Http\Controllers\Controller;
use App\Modules\Reservation\Models\ReservationModel;
use App\Modules\Reservation\Services\ReservationProperty;
use Auth;
use DB;
use Illuminate\Http\Request;

class Reserve extends Controller
{
    public function index($id,Request $request)
    {
        $member_id      = Auth::user()->member_id;
        $date           = $request->get('date');
        $time           = $request->get('time');
        $chk_category   = $request->get('chk_category');
        $chk_brand      = $request->get('chk_brand');
        $chk_insurance  = $request->get('chk_insurance');
        $reservation_detail = $request->get('reservation_detail');
        $status_id      = '1';
        $active='Y';
        if($chk_category=='')
        {
            $chk_category='6';
        }
        if($chk_brand=='')
        {
            $chk_brand='18';
        }
        if($chk_insurance=='')
        {
            $chk_insurance='17';
        }

//                dd($chk_category);

//        $chk_service=$request->get('chk_service');
        $check=array('date','time','reservation_detail');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องด้วยค่ะ'],422);
            }
        }
        $date_reservation = strtotime(DateFormat::engDate($date));
        $date_ymd = strtotime(date('Y-m-d'));
        if ($date_reservation<=$date_ymd)
        {
            return response(['กรุณากรอกวันที่ให้ถูกต้องด้วยค่ะ'],422);
        }

        $data = array(
            'member_id'     => $member_id,
            'garage_id'     => $id,
            'date'          => DateFormat::engDate($date),
            'time'          => $time,
            'category_id'  => $chk_category,
            'brand_id'     => $chk_brand,
            'insurance_id' => $chk_insurance,
            'reservation_detail' => $reservation_detail,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );

        $reservation_id=DB::table('reservations')->insertGetId($data);

        $data_status = array(
            'reservation_id'=> $reservation_id,
            'status_id'     => $status_id,
            'active'        => $active,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $reservation_status_id=DB::table('reservations_status')->insertGetId($data_status);
//        ReservationProperty::insert($chk_service,$reservation_id);
      //  ให้เลือกรายการซ่อมตอนจอง แก้ใหม่ ให้โชว์ข้อมูลแล้วพิมพ์รายละเอียดการซ่อมเอาเอง
        return response(['เพิ่มข้อมูลการจองเรียบร้อยแล้วค่ะ']);
    }

}