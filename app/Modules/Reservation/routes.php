<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/8/2559
 * Time: 11:46
 */
Route::group(['middleware'=>['auth']],function(){
Route::get('/reservation/{id}','\App\Modules\Reservation\Controllers\Reservation@index');
Route::post('/reservation/{id}','\App\Modules\Reservation\Controllers\Reserve@index');
});
