<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Category\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoryModel extends Model
{
    /**
     * @return mixed
     */
    public static function getAllCategory()
{
    return DB::table('categories')->whereNull('deleted_at')->get();
}
}