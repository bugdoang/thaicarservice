<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Brand\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BrandModel extends Model
{
    /**
     * @return mixed
     */
    public static function getAllBrand()
    {
        return DB::table('brands')->whereNull('deleted_at')->get();
    }
}