@extends('masterlayout')
@section('title','รายการจองอู่ซ่อมรถยนต์')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มกลาง-->
        <div class="register-container1 col-lg-3 col-md-3">
            <div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
                <h2 class="header-text myfont color2">รายการสถานะ</h2>
                <div class="item-border">
                    <div class="row">
                        <div class="col-lg-5 col-md-5"  style="padding-right: 0px; width: 141px;">
                            รายการจองทั้งหมด :
                        </div>
                        {{--<div class="col-lg-6 col-md-6"  style="padding-left: 0px; padding-right: 0px;">--}}
                            {{--<span id="total_status"></span> รายการ--}}

                        <div class="col-lg-5 col-md-5">
                            <span id="total_status"></span> รายการ
                        </div>
                    </div>
                    <?php
                    $total_status = 0;
                    ?>
                    @foreach($count as $item_count)
                        <?php
                        $total_status +=$item_count->total;
                        ?>
                    <div class="row">
                            <div class="col-lg-6 col-md-6">
                                {{$item_count->status_name}} :
                            </div>
                            <div class="col-lg-5 col-md-5">
                                {{$item_count->total}} รายการ
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
            {!! App\Modules\Booking\Controllers\Booking::search_reservation() !!}
        </div>

        <div class="register-container col-lg-9 col-md-9"  >
            <div class="panel-title">
                <h2 class="header-text myfont color2">รายการจองอู่ซ่อมรถยนต์</h2>
            </div>
            <form action="" method="post" id="fileForm" role="form">
                <div class="item-border">
                    <div class="pull-left">
                        <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px; padding-left: 0px;">
                            รายการจองทั้งหมด {{$booking->total()}}  รายการ
                        </div>
                    </div>
                    <div class="row">
                        <div class="pull-right">
                            <div class="col-lg-12 col-md-12 ">
                                <a href="/export/booking" class="button-delete myfont" title="excel"   style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if(count($booking)>0)
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:60px;" class="text-center"> ลำดับที่</th>
                            <th class="text-center"  style="width: 200px;"> ชื่ออู่ซ่อมรถ</th>
                            <th class="text-center" style="width: 177px;"> ชื่อลูกค้า</th>
                            <th class="text-center" style="width: 82px;"> วันที่จอง</th>
                            <th class="text-center" style="width: 131px;"> วัน-เวลาที่นัดเข้าอู่</th>
                            <th class="text-center" style="width: 113px;"> สถานะ</th>
                            <th class="text-center"  style="width: 70px;" ></th>
                        </tr>
                        @foreach($booking as $index=>$item)
                            <tr>
                                <td class="text-center">
                                    {{($booking->firstItem()+$index)}}
                                </td>
                                <td>
                                    {{$item->garage_name}}
                                </td>
                                <td>
                                    {{$item->title_name}}{{$item->first_name}} {{$item->last_name}}
                                </td>
                                <td class="text-center">{{\App\Helepers\DateFormat::Date($item->created_at)}}</td>
                                <td class="text-center">
                                    {{\App\Helepers\DateFormat::thaiDate($item->date)}} {{$item->time}} น.
                                </td>
                                <td class="text-center">
                                    {{--@if($status_thai[$item->status_id]=='ยืนยันการจอง')--}}
                                        {{--<i class="fa fa-check" aria-hidden="true"></i> {{$status_thai[$item->status_id]}}--}}
                                    {{--@else--}}
                                        {{--{{$status_thai[$item->status_id]}}--}}
                                    {{--@endif--}}
                                    {{$item->status_name}}
                                </td>
                                <td class="text-center">
                                    <a href="/viewbooking/{{$item->reservation_id}}" title="รายละเอียดการจอง" class="edit-content">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </a>
                                    @if($item->status_id =='1'|| $item->status_id =='3' || $item->status_id =='5' || $item->status_id =='6' || $item->status_id =='8' || $item->status_id =='10')
{{--                                        @if($item->status_id=='1' || $item->status_id=='9')--}}
                                        <a href="/editbooking/{{$item->reservation_id}}" title="เปลี่ยนสถานะ">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                    @endif
                                    {{--@endif--}}
                                    @if($item->status_id =='5' || $item->status_id =='6' || $item->status_id =='8' || $item->status_id =='9' || $item->status_id =='10')
                                    <a href="/report/{{$item->reservation_id}}" title="รายงานการจองอู่ซ่อมรถยนต์">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                        <div class="text-center">
                            {!!$booking->render()!!}
                        </div>
                    @else
                        <p style="text-align: center; margin-top: 10px;">ไม่มีรายการจอง</p>
                    @endif
                </div>
            </form><!-- ends register form -->
        </div>

    </div>
    @stop
@push('scripts')
<script>
    $('#total_status').text('{{$total_status}}');
    function status_change()
    {
        var x = document.getElementById('contract_status').value;

            if(x=='3' || x=='6' )// 2 ค่าที่จะขึ้นสีขาว
        {
            document.getElementById('status_select').style.display='';
        }
        else
        {
            document.getElementById('status_select').style.display='none';
        }
    }
    function title_status_change()
    {
        var x = document.getElementById('contract_status').value;

        if(x=='3' || x=='6' )// 2 ค่าที่จะขึ้นสีขาว
        {
            document.getElementById('title_status').style.display='';
        }
        else
        {
            document.getElementById('title_status').style.display='none';
        }
    }
    function status_data_change()
    {
        var x = document.getElementById('contract_status').value;

        if(x=='6' ||x=='8')
//        if(x=='1')
        {
            document.getElementById('status_textbox').style.display='';
        }
        else
        {
            document.getElementById('status_textbox').style.display='none';
        }
    }
    function title_status_data_change()
    {
        var x = document.getElementById('contract_status').value;

        if(x=='6' ||x=='8')
//        if(x=='1')
        {
            document.getElementById('title_additional').style.display='';
        }
        else
        {
            document.getElementById('title_additional').style.display='none';
        }
    }
</script>
@endpush
