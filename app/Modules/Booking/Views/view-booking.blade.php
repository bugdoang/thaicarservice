<div id="view-booking" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 60%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-car" aria-hidden="true"></i> ข้อมูลการจอง{{$reservation->garage_name}}</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px;">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-12">
                        <div class="header-detail myfont line1">
                            ข้อมูลลูกค้า
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <p><b>ชื่อลูกค้า : </b><span>{{$reservation->title_name}}{{$reservation->first_name}} {{$reservation->last_name}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>เบอร์โทรศัพท์ : </b><span>{{$reservation->mobile}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>อีเมล : </b><span>{{$reservation->email}}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            @if(!empty($reservation->category_id))
                            <div class="col-md-4 col-lg-4">
                                <p><b>ประเภทรถยนต์ : </b><span>{{$reservation->category_name}}</span></p>
                            </div>
                            @endif
                            @if(!empty($reservation->brand_id))
                            <div class="col-md-4 col-lg-4">
                                <p><b>ยี่ห้อรถยนต์ : </b><span>{{$reservation->brand_name}}</span></p>
                            </div>
                            @endif
                            @if(!empty($reservation->insurance_id))
                            <div class="col-md-4 col-lg-4">
                                <p><b>บริษัทประกันภัย : </b><span>{{$reservation->insurance_name}}</span></p>
                            </div>
                            @endif
                        </div>
                        <div class="header-detail myfont line1">
                            รายละเอียดการจองอู่
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <p><b>วันที่ทำการจอง : </b><span>{{\App\Helepers\DateFormat::Date_Clock($reservation->created_at)}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>วันที่นัดเข้าอู่ : </b><span>{{\App\Helepers\DateFormat::thaiDate($reservation->date)}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>เวลา : </b><span>{{$reservation->time}}</span></p>
                            </div>
                        </div>

                        @if(!empty($reservation->reservation_detail))
                            <div class="row">
                                <div class="col-lg-12 col-md-12"  style="margin-bottom: 5px;">
                                    <b>รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม : </b>
                                </div>
                                <div class="col-lg-12 col-md-12"  style="margin-bottom: 5px; color: red">
                                    <input class="form-control"  disabled="disabled"  value=" {{$reservation->reservation_detail}}"/>
                                </div>
                            </div>
                        @endif
                        @if(!empty($repair_list))
                            <div class="header-detail myfont line">
                                รายการซ่อมเบื้องต้น
                            </div>
                            <div class="row"   style=" margin-left: 15px; width: 90%;">
                            <table class="table table-bordered" >
                                    <tr>
                                        <th class="text-center" > ลำดับที่</th>
                                        <th class="text-center" > ประเภทรายการซ่อม</th>
                                        <th class="text-center" > รายการซ่อม</th>
                                        <th class="text-center" > สถานะ</th>
                                        <th class="text-center" > วันที่ตอบรับการซ่อม</th>
                                    </tr>
                                    @foreach($repair_list as $index=>$item)
                                        <tr>
                                            <td class="text-center">
                                                {{$index+1}}
                                            </td>
                                            <td class="text-center">
                                                {{$item->service_name}}
                                            </td>

                                            <td class="text-center">
                                                @if($item->sub_service_name==null)
                                                    -
                                                @else
                                                    {{$item->sub_service_name}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($item->status_repair_list_member=='Y')
                                                    <i class="fa fa-check" aria-hidden="true" style="color: green"></i>
                                                @elseif($item->status_repair_list_member=='N' && $item->dob_status_member=='')
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i> รอการตอบรับ
                                                @elseif($item->status_repair_list_member=='N')
                                                    <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                                @endif


                                            </td>
                                            <td class="text-center">
                                                @if($item->dob_status_member==null)
                                                    -
                                                @else
                                                    {{\App\Helepers\DateFormat::thaiDate($item->dob_status_member)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                            </table>
                        </div>
                        @endif

                        <div class="header-detail myfont line1">
                                สถานะการทำงาน
                        </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <p><b>สถานะล่าสุด : </b><span>{{$reservation_status->status_name}} (แก้ไขสถานะล่าสุดวันที่ {{\App\Helepers\DateFormat::Date_Clock($reservation_status->created_at)}})</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <table class="table table-bordered" style="width: 80%; margin-left: 30px;">
                                    @if(!empty($data_status))
                                        <tr>
                                            <th class="text-center" > ลำดับที่</th>
                                            <th class="text-center" > ชื่อสถานะ</th>
                                            <th class="text-center" > วันที่เปลี่ยนสถานะ</th>
                                            <th class="text-center" > อัพเดตโดย</th>

                                        </tr>
                                        @foreach($data_status as $index=>$item)
                                            @if($item->active == 'Y')
                                                <tr style="background-color:#F8D486">
                                            @else
                                                <tr>
                                            @endif
                                                    <td class="text-center">
                                                        {{$index+1}}
                                                    </td>
                                                    <td class="text-center">
                                                       {{$item->status_name}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{\App\Helepers\DateFormat::Date_Clock($item->created_at)}}
                                                    </td>
                                                    <td class="text-center">
                                                        @if($item->status_id==3 || $item->status_id==4 || $item->status_id==6 || $item->status_id==7 || $item->status_id==8 || $item->status_id==9 || $item->status_id==10)
                                                            อู่ซ่อมรถยนต์
                                                        @else
                                                            คุณ{{$reservation->first_name}} {{$reservation->last_name}}
                                                        @endif
                                                    </td>
                                                </tr>
                                        @endforeach
                                </table>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>