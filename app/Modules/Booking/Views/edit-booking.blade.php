@extends('masterlayout')
@section('title','แก้ไขสถานะ')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มซ้าย-->
        <div class="register-container1 col-lg-2 col-md-2"></div>
        <!--เริ่มกลาง-->
        <div class="register-container col-lg-8 col-md-8"  id="list-container">
            <div class="panel-title">
                <h2 class="header-text myfont color2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขสถานะ</h2>
            </div>
                <form action="/editbooking/{{$reservation->reservation_id}}" method="PUT" id="fileForm" role="form" class="item-border1">
                    {{ csrf_field() }}
                        <!--เริ่ม-->
                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <h2 class="header1-text myfont line">รายละเอียดผู้จอง</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                   <b>ชื่อ-นามสกุลผู้จอง :</b>
                                </div>
                                <div class="col-md-5 col-lg-5">
                                    {{$reservation->title_name}}{{$reservation->first_name}} {{$reservation->last_name}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <b>เบอร์โทรศัพท์ :</b>
                                </div>
                                <div class="col-md-5 col-lg-5">
                                    {{$reservation->mobile}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <b>อีเมล :</b>
                                </div>
                                <div class="col-md-5 col-lg-5">
                                    {{$reservation->email}}
                                </div>
                            </div>
                        </div>
                    <div class="col-md-12 col-lg-12"  style="margin-top: 25px;">
                        <div class="row">
                            <h2 class="header1-text myfont line">ข้อมูลการจองอู่ซ่อมรถยนต์</h2>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <b>ชื่ออู่ซ่อมรถยนต์ :</b> {{$reservation->garage_name}}
                            </div>

                            <div class="col-md-6 col-lg-6">
                                <b>วันที่ทำการจอง :</b> {{\App\Helepers\DateFormat::Date_Clock($reservation->created_at)}}
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2">
                                <b>วันที่นัดเข้าอู่ :</b>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                {{\App\Helepers\DateFormat::thaiDate($reservation->date)}}
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <b>เวลานัด :</b>
                            </div>
                            <div class="col-md-3 col-lg-3">
                                {{$reservation->time}} น.
                            </div>
                        </div>
                    </div>
                        <div class="col-md-12 col-lg-12"  style="margin-top: 25px;">
                            <div class="row">
                                <h2 class="header1-text myfont line">ข้อมูลรถที่ต้องการจะซ่อม</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <b>ประเภทรถ :</b>
                                    @if(!empty($reservation->category_name))
                                        {{$reservation->category_name}}
                                    @else
                                        -
                                    @endif
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <b>ยี่ห้อรถ :</b>
                                    @if(!empty($reservation->brand_name))
                                        {{$reservation->brand_name}}
                                    @else
                                        -
                                    @endif
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <b>บริษัทประกันภัย :</b>
                                    @if(!empty($reservation->insurance_name))
                                        {{$reservation->insurance_name}}
                                    @else
                                        -
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                @if(!empty($reservation->reservation_detail))
                                    <div class="col-lg-12 col-md-12"  style="margin-bottom: 5px;">
                                        <b>รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม : </b>
                                    </div>
                                    <div class="col-lg-12 col-md-12"  style="margin-bottom: 5px; color: red">
                                        <input class="form-control"  disabled="disabled"  value=" {{$reservation->reservation_detail}}"/>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if(!empty($repair_lists))
                        <div class="col-md-12 col-lg-12"  style="margin-top: 25px;">
                            <div class="row">
                                <h2 class="header1-text myfont line">รายการซ่อมเบื้องต้น</h2>
                            </div>
                            <div class="row"   style=" margin-left: 15px; width: 90%;">
                                <table class="table table-bordered" >
                                    <tr>
                                        <th class="text-center" > ลำดับที่</th>
                                        <th class="text-center" > ประเภทรายการซ่อม</th>
                                        <th class="text-center" > รายการซ่อม</th>
                                        <th class="text-center" > รายการที่ยืนยันการซ่อม</th>
                                        <th class="text-center" > วันที่ยืนยันรายการซ่อม</th>
                                    </tr>
                                    @foreach($repair_lists as $index=>$item)
                                        <tr>
                                            <td class="text-center">
                                                {{$index+1}}
                                            </td>
                                            <td class="text-center">
                                                {{$item->service_name}}
                                            </td>

                                            <td class="text-center">
                                                @if($item->sub_service_name==null)
                                                    -
                                                @else
                                                    {{$item->sub_service_name}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($item->status_repair_list_member=='Y')
                                                    <i class="fa fa-check" aria-hidden="true" style="color: green"></i>
                                                @elseif($item->status_repair_list_member=='N')
                                                    <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($item->sub_service_name==null)
                                                    -
                                                @else
                                                    {{\App\Helepers\DateFormat::thaiDate($item->dob_status_member)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>

                        </div>
                        @endif
                        <div class="col-md-12 col-lg-12"  style="margin-top: 25px;">
                            <div class="row">
                                <h2 class="header1-text myfont line"><i class="fa fa-check-square-o" aria-hidden="true"></i> สถานะการดำเนินงาน</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <b>สถานะการดำเนินงาน :</b>
                                </div>
                                <div class="col-md-4 col-lg-4"  style="margin-top: -5px;">
                                    <select class="form-control"  name="contract_status" id="contract_status" onchange="change_status(this)">
                                        <option value="">กรุณาเลือก</option>
                                        @if(!empty($reservation_status->status_id))
                                                @if($reservation_status->status_id==1)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='0' || $index=='2' || $index=='3')
                                                        <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                            {{$item->status_name}}
                                                        </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==2)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='1')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==3)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='2'|| $index=='9')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==10)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='4'|| $index=='6')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==4)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='3')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==5)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='4' || $index=='5' || $index=='6')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==6)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='5' || $index=='6' || $index=='7')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==7)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='6')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==8)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='7' || $index=='8')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @elseif($reservation_status->status_id==9)
                                                    @foreach($status as $index=>$item)
                                                        @if($index=='8')
                                                            <option {{($reservation_status->status_id==$item->status_id)?' selected disabled':''}} value="{{$item->status_id}}">
                                                                {{$item->status_name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                        @endif
                                    </select>
                                </div>
                                <div v-show="my_show" class="col-md-12 col-lg-12"  style="margin-top: 25px;">
                                    <div class="row">
                                        <h2 class="header1-text myfont line"> รายการซ่อมเบื้องต้น</h2>
                                    </div>
                                    <div class="row">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th style="width:60px;" class="text-center"> ลำดับที่</th>
                                                <th class="text-center"  style="width: 200px;"> ประเภทงานบริการ</th>
                                                <th class="text-center" style="width: 177px;"> งานบริการ</th>
                                                <th class="text-center" style="width: 131px;"> </th>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    #
                                                </td>
                                                <td class="text-center">
                                                    <select class="form-control" onchange="select_service(this)" name="service_id" id="service_id">
                                                        <option value="">กรุณาเลือก</option>
                                                        @if(!empty($data_service))
                                                            @foreach($data_service as $item)
                                                                <option value="{{$item->service_id}}">
                                                                {{$item->service_name}}
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="sub_service_id" id="sub_service_id">
                                                        <option value="">กรุณาเลือก</option>
                                                    </select>
                                                </td>
                                                <td class="text-center">
                                                    <a href="javascript:;" @click="add"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูลการซ่อม</a>
                                                </td>
                                            </tr>
                                            <tr v-for="(item,index) in items">
                                                <td class="text-right">
                                                    @{{ (index+1) }}.
                                                </td>
                                                <td class="text-left">
                                                    @{{ item.service_name }}
                                                    <input type="hidden" v-model="item.service_id" name="service_ids[]">
                                                    <input type="hidden"  v-model="item.sub_service_id" name="sub_service_ids[]">
                                                </td>
                                                <td class="text-left">
                                                    @{{ item.sub_service_name }}
                                                </td>
                                                <td class="text-center">
                                                    <a href="javascript:;" @click="remove(index)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="button-garage myfont">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    บันทึก
                                </button>
                            </div>
                        </div>
                </form><!-- ends register form -->
        </div>
        <div class="register-container1 col-lg-2 col-md-2"></div>
    </div>
    @stop
@push('scripts')
<script>
    function callback_ajax(data)
    {
        MessageBox.alert(data[0],function () {
            window.location.href='/booking' ;
        });
    }
    var sub_service_items={!! json_encode($data_sub_services) !!};
    function select_service(o)
    {
        var service_id = $(o).val();
        if(typeof sub_service_items[service_id]!=='undefined')
        {
            $('#sub_service_id').empty().append(sub_service_items[service_id]);
            $('#sub_service_id').val("").trigger("change");
            $('#select2-sub_service_id-container').attr('title','');
        }
        else
        {
            $('#sub_service_id').empty();
            $('#sub_service_id').val("").trigger("change");
            $('#select2-sub_service_id-container').attr('title','');
        }
    }

    function change_status(o)
    {
        if($(o).val()==10)
        {
            app.my_show = true;
        }
        else
        {
            app.my_show = false;
        }
    }

    var app = new Vue({
        el: '#list-container',
        data: {
            items:{!! json_encode($data_repair_list) !!},
            service_selected:{!! !empty($data_repair_check)?json_encode($data_repair_check):'{}' !!},
            my_show:{{$reservation_status->status_id==10?'true':'false'}}
        },
        methods: {
            add: function () {
                var service_id = $('#service_id').val();
                var service_name = $.trim($('#select2-service_id-container').attr('title'));
                var sub_service_id = $('#sub_service_id').val();
                var sub_service_name = $.trim($('#select2-sub_service_id-container').attr('title'));
                var key = service_id+'-'+sub_service_id;
                if(service_id=='' && sub_service_id=='')
                {
                    MessageBox.error('กรุณาเลือกรายการก่อนค่ะ');
                    return;
                }
                if(typeof this.service_selected[key]==='undefined')
                {
                    this.items.push({
                        service_id: service_id,
                        service_name: service_name,
                        sub_service_id: sub_service_id,
                        sub_service_name: sub_service_name
                    });
                    this.service_selected[key] = 1;
                }
                else
                {
                    MessageBox.error('รายการนี้ได้เพิ่มเข้าไปเแล้วค่ะ');
                }
            },
            remove:function(index){
                var key = this.items[index].service_id+'-'+this.items[index].sub_service_id;
                delete this.service_selected[key];
                this.items.splice(index,1);
            }
        }
    })

</script>
@endpush

