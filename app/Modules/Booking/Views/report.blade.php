<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>รายงานการจองอู่ซ่อมรถยนต์</title>
    <link href="/assets/css/report.css" rel="stylesheet">
    <style>
        .border-none{
            border-bottom: 0!important;
            border-top: 0!important;
        }
    </style>
</head>
<body>
<?php
$date =  Date('Y-m-d H:i:s');
?>
<header class="clearfix">
    <h2>{{$reservation['garage']->garage_name}}</h2>
    <h3>{{$reservation['garage']->garage_address}} {{$reservation['garage']->province_name}} <br>
    เบอร์โทรศัพท์ {{$reservation['garage']->garage_mobile}} อีเมล์ {{$reservation['garage']->garage_email}}</h3>
    <h1>รายงานการจองอู่ซ่อมรถยนต์</h1>
    <div id="project">
        <div><span>ชื่อ-นามสกุลลูกค้า</span> {{$reservation['member']->title_name}}{{$reservation['member']->first_name}}  {{$reservation['member']->last_name}}</div>
        <div><span>เบอร์โทรศัพท์</span> {{$reservation['member']->mobile}}</div>
        <div><span>ที่อยู่</span> {{$reservation['member']->address}} {{$reservation['member']->province_name}}</div>
        <div><span>อีเมล</span> {{$reservation['member']->email}}</div>
        <div><span>วันที่จอง</span> {{\App\Helepers\DateFormat::Date_Clock($reservation['member']->created_at)}}
        </div>
        <div><span>วันที่-เวลา ที่นัดเข้าอู่</span> {{\App\Helepers\DateFormat::Date($reservation['member']->date)}} {{$reservation['member']->time}} น.</div>
    </div>
</header>

<div id="project">
    <div><span>รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม ลูกค้าแจ้งเบื้องต้น :</span></div>
    <div>{{$reservation['member']->reservation_detail}}</div>
</div>

<main>
    <table>
        @if(!empty($repair_list))
        <thead>
        <tr>
            <th class="desc1" >ลำดับ</th>
            <th class="desc1">รายการซ่อม</th>
            <th class="desc2">ประเภทงานซ่อม</th>
        </tr>
        </thead>
            <tbody>
            @foreach($repair_list as $index=>$item)
                <?php $style = ($index<count($repair_list)-1)?'border-top: 0 !important; border-bottom:0 !important;':'border-top: 0 !important;';?>
                <tr>
                    <td style="{{$style}}" class="desc1">
                                {{$index+1}}
                    </td>
                    <td style="{{$style}}" class="desc">{{$item->service_name}}</td>
                    <td style="{{$style}}" class="desc2">
                        @if($item->sub_service_name==null)
                            -
                        @else
                            {{$item->sub_service_name}}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
    </table>
    @endif

    <div id="project">
        <div><span>สถานะการทำงาน :</span></div>
        <div>{{$reservation_status->status_name}} (อัพเดตสถานะล่าสุดเมื่อวันที่ {{\App\Helepers\DateFormat::Date_Clock($reservation_status->created_at)}})</div>
    </div>
</main>
<footer>
    ข้อมูลจากวันที่ {{\App\Helepers\DateFormat::Date_Clock($date)}}

</footer>
</body>
</html>