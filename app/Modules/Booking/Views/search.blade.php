<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2">ค้นหาข้อมูลการจอง</h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/booking">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่อลูกค้า" type="text" name="first_name" class="form-control" value="{{$first_name}}">

                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  นามสกุลลูกค้า" type="text" name="last_name" class="form-control" value="{{$last_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  วันที่จอง" type="text" name="date" class="form-control datepicker" value="{{$date}}">
                </div>
            </div>
            <div class="row form-group"  style="margin-top: -48px;" >
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกสถานะ" name="status">
                        <option value="" >กรุณาเลือก</option>
                        @if($status_data)
                            @foreach($status_data as $item)
                                <option {{($status==$item->status_id)?' selected ':''}} value="{{$item->status_id}}">
                                    {{$item->status_name}}
                                </option>
                            @endforeach
                        @endif
                    </select>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกอู่ซ่อมรถยนต์" name="garage">
                        <option value="" >กรุณาเลือก</option>
                        @if(!empty($garage_data))
                            @foreach($garage_data as $item)
                                <option {{($garage==$item->garage_id)?' selected ':''}} value="{{$item->garage_id}}">
                                    {{$item->garage_name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="เรียงลำดับ" name="sort">
                        <option value="" ></option>
                        @if(!empty($sort_data))
                            @foreach($sort_data as $key=>$item)
                                <option {{($sort==$item)?' selected ':''}} value="{{$item}}">
                                    {{$key}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>