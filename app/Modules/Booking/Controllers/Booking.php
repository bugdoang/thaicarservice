<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Booking\Controllers;


use App\Helepers\DateFormat;
use App\Helepers\Download;
use App\Http\Controllers\Controller;
use App\Modules\Booking\Models\BookingModel;
use App\Modules\Management\Models\ManagementModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Profile\Models\ProfileModel;
use App\Modules\Status\Models\StatusModel;
use App\Services\MyExcel;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use SebastianBergmann\Comparator\Book;
use App\Services\MahaPDF;

class Booking extends Controller
{
    public function index()
    {
        $first_name     = Input::get('first_name');
        $last_name      = Input::get('last_name');
        $date           = Input::get('date');
        $sort           = Input::get('sort');
        $garage         = Input::get('garage');
        $status         = Input::get('status');
        $member_id      = Auth::user()->member_id;
        $garage_me      = ProfileModel::garage_me($member_id);
        $count          = BookingModel::booking_count();

//        dd($count);
        $booking        = BookingModel::booking([
            'first_name' =>$first_name,
            'last_name'  =>$last_name,
            'date'       =>$date,
            'status'=>$status,
            'garage'    =>$garage,
            'sort'      =>$sort,

        ]);

//        dd($booking);
        $booking->appends([
            'first_name'    =>$first_name,
            'last_name'     =>$last_name,
            'date'          =>$date,
            'sort'          =>$sort,
            'garage'        =>$garage,
        ]);
        return view ('booking::booking',[
            'garage_me'     =>$garage_me,
            'booking'       =>$booking,
            'count'         =>$count,
        ]);
    }
    public function view($reservation_id)
    {
        $reservation    =BookingModel::data_booking($reservation_id);
        $reservation_status    =BookingModel::data_booking_status($reservation_id);
        $data_status=BookingModel::data_status($reservation_id);//ข้อมูลสถานะแบบตาราง
        $repair_list    =StatusModel::repair_list($reservation_id);

//        dd($reservation);
//        dd($data_status);
//        $service_name=[];
//        if(isset($reservation->list_service_name))
//        {
//            $service_name=explode(',',$reservation->list_service_name);
//        }
        $html=view('booking::view-booking',compact('reservation'/*,'service_name'*/,'reservation_status','data_status','repair_list'));
        return ['title'=>'ดูข้อมูลการจองอู่ซ่อมรถยนต์','body'=>$html->render()];
    }
    public function edit($reservation_id)
    {
        $reservation    =BookingModel::data_booking($reservation_id);
        $reservation_status    =BookingModel::data_booking_status($reservation_id);
        $status     =BookingModel::status();
        $data_service    =BookingModel::data_service();
        $temp_sub_services=BookingModel::sub_services();
        $repair_lists=BookingModel::repair_list($reservation_id);
//        dd($status);
//        dd($chang_status);
        $data_repair_list=[];
        $data_repair_check=[];
        if(!empty($repair_lists))
        {
            foreach ($repair_lists as $item) {
                $sub = empty($item->sub_services_id)?'null':$item->sub_services_id;
                $data_repair_check[$item->service_id.'-'.$sub]=1;
                $data_repair_list[] = [
                    'service_id'        => $item->service_id,
                    'service_name'      => $item->service_name,
                    'sub_service_id'    => $item->sub_services_id,
                    'sub_service_name'  => $item->sub_service_name];
            }
        }
        $data_sub_services=[];
        if(!empty($temp_sub_services))
        {
            foreach ($temp_sub_services as $item) {
                $data_sub_services[$item->service_id][] = "<option value='{$item->sub_services_id}'>{$item->sub_service_name}</option>";
            }
        }
        return view ('booking::edit-booking',[
            'reservation'=>$reservation,
            'reservation_status'=>$reservation_status,
            'status'=>$status,
            'data_service'=>$data_service,
            'data_sub_services'=>$data_sub_services,
            'data_repair_list'=>$data_repair_list,
            'data_repair_check'=>$data_repair_check,
            'repair_lists'=>$repair_lists
        ]);
    }
    public function update_edit($reservation_id,Request $request)
    {
        $status_id=$request->get('contract_status');
        $service_ids=$request->get('service_ids');
        $sub_services_ids=$request->get('sub_service_ids');
        if ($status_id=='10')
        {
            $data_repair_list=[];
            DB::table('repair_list')->where('reservation_id',$reservation_id)->delete();
            if (!empty($service_ids) && is_array($service_ids))
            {
                foreach ($service_ids as $index=>$service_id)
                {
                    if (is_numeric($service_id))
                    {
                        $sub_services_id=isset($sub_services_ids[$index]) && is_numeric($sub_services_ids[$index])?$sub_services_ids[$index]:null;
                        $data_repair_list[] = array(
                            'reservation_id'=> $reservation_id,
                            'service_id'=> $service_id,
                            'sub_services_id'=> $sub_services_id,
                            'status_repair_list_garage'=>'Y',
                            'dob_status_garage'=>date('Y-m-d'),
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s')
                        );
                    }
                }
            }
            if (!empty($data_repair_list))
            {
                DB::table('repair_list')->insert($data_repair_list);
            }

        }
        if (is_numeric($status_id) && $status_id>0)
        {
            StatusModel::update_active($reservation_id,[
                'active'=>'N',
            ]);
            $data_reservation = array(
                'reservation_id'=> $reservation_id,
                'status_id'=> $status_id,
                'active'=>'Y',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            );
            $reservation_status_id=DB::table('reservations_status')->insertGetId($data_reservation);
        }

        return response(['อัพเดตสถานะเรียบร้อยแล้วค่ะ']);

    }
    public function cancel($reservation_id)
    {

        StatusModel::update_active($reservation_id,[
            'active'=>'N',
        ]);
        $data_status = array(
            'reservation_id'=> $reservation_id,
            'status_id'=> '2',//2= ยกเลิกการจอง
            'active'=>'Y',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),

        );
        $reservation_id=DB::table('reservations_status')->insertGetId($data_status);
        return response(['ยกเลิกการจองเรียบร้อยแล้วค่ะ']);

    }
    public static function search_reservation()
    {
        $member_id      =   Auth::user()->member_id;
        $first_name     =   Input::get('first_name');
        $last_name      =   Input::get('last_name');
        $date           =   Input::get('date');
        $status         =   Input::get('status');
        $sort           =   Input::get('sort');
        $status_data    =   BookingModel::status();
        $sort_data      =   config('myconfig.sort');
        $garage         =   Input::get('garage');
        $garage_data    =   ProfileModel::garage_me($member_id);
//        dd($status_data);
//        $garage_data->appends([
//            'first_name'=>$first_name,
//            'last_name'=>$last_name,
//            'date'=>$date,
//            'status'=>$status,
//            'sort'=>$sort,
//            'garage'=>$garage,
//
//        ]);

        return view ('booking::search',[
            'first_name'        =>  $first_name,
            'last_name'         =>  $last_name,
            'date'              =>  $date,
            'status'            =>  $status,
            'status_data'       =>  $status_data,
            'garage'            =>  $garage,
            'garage_data'       =>  $garage_data,
            'sort'              =>  $sort,
            'sort_data'         =>  $sort_data
        ]);
    }
    public function export()
    {
        $data_booking = BookingModel::booking([],true);
        $status_thai    = config('myconfig.status_thai');
        $data = [];
            if (!empty($data_booking))
            {
                foreach ($data_booking as $index => $item) {
                    $data[] = ['ลำดับ' => $index + 1,
                        'ชื่ออู่ซ่อมรถยนต์' => $item->garage_name,
                        'ชื่อ-นามสกุลลูกค้า' => $item->title_name . $item->first_name . ' ' . $item->last_name,
                        'วันที่จอง' => DateFormat::Date($item->created_at),
                        'วันที่นัดเข้าอู่' => DateFormat::thaiDate($item->date),
                        'เวลานัดเข้าอู่' => $item->time,
                        'สถานะ' => $item->status_name
                    ];
                }
            }
            $export = MyExcel::export('booking', $data);
            Download::get('ข้อมูลการจอง', $export['path']);

    }
//    public static function report($reservation_id)
//    {
//        $reservation    =BookingModel::data_booking($reservation_id);
//        $reservation_status    =BookingModel::data_booking_status($reservation_id);
//        $data_status=BookingModel::data_status($reservation_id);//ข้อมูลสถานะแบบตาราง
//        $repair_list    =BookingModel::repair_list_pdf($reservation_id);
//        return view ('booking::report',[
//            'reservation'=>$reservation,
//            'data_status'=>$data_status,
//            'reservation_status'=>$reservation_status,
//            'repair_list'=>$repair_list,
//        ]);
//    }
    public static function report($reservation_id)
    {
        $reservation    =BookingModel::data_booking_pdf($reservation_id);
        $reservation_status    =BookingModel::data_booking_status($reservation_id);
        $repair_list    =BookingModel::repair_list_pdf($reservation_id);
//        dd($reservation_status);
        $html = view('booking::report',[
            'reservation'=>$reservation,
            'reservation_status'=>$reservation_status,
            'repair_list'=>$repair_list,
        ]);
        // return $html->render();
        $test=MahaPDF::html($html,'preview');
        Download::pdf('ข้อมูลการจอง-'.date('YmdHis'), $test);
    }
}

