<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/8/2559
 * Time: 11:46
 */
Route::group(['middleware'=>['auth']],function(){
Route::get('/booking','\App\Modules\Booking\Controllers\Booking@index');
Route::get('/report/{reservation_id}','\App\Modules\Booking\Controllers\Booking@report');
Route::get('/viewbooking/{reservation_id}','\App\Modules\Booking\Controllers\Booking@view');
Route::get('/editbooking/{reservation_id}','\App\Modules\Booking\Controllers\Booking@edit');
Route::put('/editbooking/{reservation_id}','\App\Modules\Booking\Controllers\Booking@update_edit');
Route::get('/export/booking','\App\Modules\Booking\Controllers\Booking@export');
});

// PDF
//Route::get('project-plan/{id}/pdf', '\App\Modules\Plan\Controllers\ProjectPlanPdf@index');


