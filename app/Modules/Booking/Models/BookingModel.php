<?php
namespace App\Modules\Booking\Models;
use App\Helepers\DateFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/10/2559
 * Time: 17:31
 */
class BookingModel extends Model
{
    public static function  booking($param=[],$isall=false)
    {
        $member_id =Auth::id();
        $first_name=(isset($param['first_name']))?$param['first_name']:'';
        $last_name=(isset($param['last_name']))?$param['last_name']:'';
        $date=(isset($param['date']))?$param['date']:'';
        $status=(isset($param['status']))?$param['status']:'';
        $garage=(isset($param['garage']))?$param['garage']:'';
        $sort=(isset($param['sort']))?$param['sort']:'';
        $date=DateFormat::engDate($date);
        $data=DB::table('reservations')
            ->select([
                'reservations.*',
                'garage_name',
                'title_name',
                'first_name',
                'last_name',
                'reservations_status.status_id',
                'status.status_name',
            ])
            ->leftJoin('members','members.member_id','=','reservations.member_id')
            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
            ->leftJoin('reservations_status','reservations_status.reservation_id','=','reservations.reservation_id')
            ->leftJoin('status','status.status_id','=','reservations_status.status_id')
            ->where('reservations.member_id',$member_id)
            ->where('reservations_status.active','Y')
            ->whereNull('reservations.deleted_at')
            ->whereNull('garages.deleted_at');
        if(!empty($first_name))
        {
            $data->where('members.first_name','LIKE','%'.$first_name.'%');
        }
        if(!empty($last_name))
        {
            $data->where('members.last_name','LIKE','%'.$last_name.'%');
        }
        if(!empty($date))
        {
            $data->where('reservations.created_at','>=',$date.' 00:00:00');
            $data->where('reservations.created_at','<=',$date.' 23:59:59');
        }
        if(!empty($status))
        {
            $data->where('reservations_status.status_id',$status);
        }
        if(!empty($garage))
        {
            $data->where('reservations.garage_id',$garage);
        }
        if(!empty($sort))
        {
            $data->orderby('garages.garage_name',$sort);
        }
        if ($isall==true)
        {
            return $data->get();
        }
//        dd($status);
        return $data
            ->orderby('reservations.created_at','DESC')
            ->paginate(10);
    }
    public static function  data_booking($reservation_id='')
    {
        $data=DB::table('reservations')
            ->whereNull('reservations.deleted_at');
        if(!empty($reservation_id))
        {
            return $data
                ->where('reservations.reservation_id',$reservation_id)
                ->select([
                    'reservations.*',
                    'garages.garage_name',
                    'garages.address as garage_address',
                    'garages.email as garage_email',
                    'garages.mobile as garage_mobile',
                    'garages.province_id as garage_province',
                    'members.title_name',
                    'members.first_name',
                    'members.last_name',
                    'members.mobile',
                    'members.email',
                    'members.address',
                    'members.province_id',
                    'brands.brand_name',
                    'categories.category_name',
                    'insurances.insurance_name',
                    'provinces.province_name',
//                    DB::raw('GROUP_CONCAT(service_name) as list_service_name')
                ])
                ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
                ->leftJoin('members','members.member_id','=','reservations.member_id')
                ->leftJoin('brands','brands.brand_id','=','reservations.brand_id')
                ->leftJoin('categories','categories.category_id','=','reservations.category_id')
                ->leftJoin('insurances','insurances.insurance_id','=','reservations.insurance_id')
                ->leftJoin('provinces','provinces.province_id','=','members.province_id')
//                ->leftJoin('reservations_services','reservations_services.reservation_id','=','reservations.reservation_id')
//                ->leftJoin('services','services.service_id','=','reservations_services.service_id')
//                ->groupby('reservations_services.reservation_id')
                ->first();



        }
//        $data=[$data_garage,$data_member];
        return $data->get();
    }

    public static function  data_booking_pdf($reservation_id='')
    {
        $items = [];
        if(!empty($reservation_id))
        {
            $data_member=DB::table('reservations')
                ->whereNull('reservations.deleted_at')
                ->where('reservations.reservation_id',$reservation_id)
                ->select([
                    'reservations.*',
                    'members.title_name',
                    'members.first_name',
                    'members.last_name',
                    'members.mobile',
                    'members.email',
                    'members.address',
                    'brands.brand_name',
                    'categories.category_name',
                    'insurances.insurance_name',
                    'provinces.province_name',
                ])
                ->leftJoin('members','members.member_id','=','reservations.member_id')
                ->leftJoin('brands','brands.brand_id','=','reservations.brand_id')
                ->leftJoin('categories','categories.category_id','=','reservations.category_id')
                ->leftJoin('insurances','insurances.insurance_id','=','reservations.category_id')
                ->leftJoin('provinces','provinces.province_id','=','members.province_id')
                ->first();


            $data_garage=DB::table('reservations')
                ->whereNull('reservations.deleted_at')
            ->where('reservations.reservation_id',$reservation_id)
            ->select([
                'garages.garage_name',
                'garages.address as garage_address',
                'garages.email as garage_email',
                'garages.mobile as garage_mobile',
                'provinces.province_name',
            ])
            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
            ->leftJoin('provinces','provinces.province_id','=','garages.province_id')
            ->first();

            $items['member'] = $data_member;
            $items['garage'] = $data_garage;
        }
//        $data=[$data_garage,$data_member];


         return $items;
    }


    public static function  data_booking_status($reservation_id='')
    {
        $data=DB::table('reservations')
            ->whereNull('reservations.deleted_at')
            ->where('reservations_status.active','Y');
        if(!empty($reservation_id))
        {
            return $data
                ->where('reservations.reservation_id',$reservation_id)
                ->select([
                    'reservations.reservation_id',
                    'reservations_status.status_id',
                    'reservations_status.active',
                    'reservations_status.created_at',
                    'status.status_name'
                ])
                ->leftJoin('reservations_status','reservations_status.reservation_id','=','reservations.reservation_id')
                ->leftJoin('status','status.status_id','=','reservations_status.status_id')
                ->first();
        }
        return $data->get();
    }
    public static function  data_status($reservation_id)
    {
        $data=DB::table('reservations')
            ->whereNull('reservations.deleted_at');
            return $data
                ->where('reservations.reservation_id',$reservation_id)
                ->select([
                    'reservations.reservation_id',
                    'reservations_status.status_id',
                    'reservations_status.active',
                    'reservations_status.created_at',
                    'status.status_name'

                ])
                ->leftJoin('reservations_status','reservations_status.reservation_id','=','reservations.reservation_id')
                ->leftJoin('status','status.status_id','=','reservations_status.status_id')
                ->orderby('reservations_status.created_at')
                ->get();
    }
    public static function  data_service()
    {
        $data=DB::table('services')
            ->whereNull('deleted_at');
        return $data->get();
    }
    public static function  status()
    {
        $data=DB::table('status')
            ->whereNull('deleted_at');
        return $data->get();
    }
    public static function  sub_services()
    {
        $data=DB::table('sub_services')
            ->whereNull('deleted_at');
        return $data->get();
    }
    public static function  repair_list($reservation_id='')
    {
        $data=DB::table('repair_list')
            ->whereNull('repair_list.deleted_at');
        if(!empty($reservation_id))
        {
            return $data
                ->where('repair_list.reservation_id',$reservation_id)
                ->select([
                    'repair_list.*',
                    'services.service_name',
                    'sub_services.sub_service_name',
                ])
                ->leftJoin('services','services.service_id','=','repair_list.service_id')
                ->leftJoin('sub_services','sub_services.sub_services_id','=','repair_list.sub_services_id')
                ->get();
        }
        return $data->get();
    }
    public static function  repair_list_pdf($reservation_id='')
    {
        $data=DB::table('repair_list')
            ->whereNull('repair_list.deleted_at');
        if(!empty($reservation_id))
        {
            return $data
                ->where('repair_list.reservation_id',$reservation_id)
                ->where('repair_list.status_repair_list_member','Y')
                ->select([
                    'repair_list.*',
                    'services.service_name',
                    'sub_services.sub_service_name',
                ])
                ->leftJoin('services','services.service_id','=','repair_list.service_id')
                ->leftJoin('sub_services','sub_services.sub_services_id','=','repair_list.sub_services_id')
                ->get();
        }
        return $data->get();
    }
//    public static function update_status($reservation_id,$item)
//    {
//        return DB::table('reservations_status')
//            ->where('reservation_id',$reservation_id)
//            ->update($item);
//    }
    public static function booking_count()
    {
        $member_id =Auth::id();
//        $sql="select status ,count(status) as total from reservations
//              where deleted_at is null and EXISTS
//              (SELECT 1 FROM garages where garages.member_id=$member_id
//              AND reservations.garage_id=garages.garage_id AND deleted_at is null)
//              group by status";
        $sql="select reservations_status.status_id,status_name,count(reservations_status.status_id) as total from reservations_status
              left join status ON reservations_status.status_id = status.status_id
              where reservations_status.deleted_at is null and active='Y' and EXISTS
              (SELECT 1 FROM reservations where reservations.member_id=$member_id
              AND reservations_status.reservation_id=reservations.reservation_id AND deleted_at is null)
              group by reservations_status.status_id";
        return DB::select($sql);
    }
    public static function data_export()
    {
        $data=DB::table('reservations')
            ->whereNull('reservations.deleted_at')
            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
            ->leftJoin('members','members.member_id','=','reservations.member_id')
            ->select([
            'reservations.*',
            'garages.garage_name',
            'members.title_name',
            'members.first_name',
            'members.last_name',
            'members.mobile',
            'members.email',
        ]);
        return $data->get();
    }

}