<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Product\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductModel extends Model
{
    /**
     * @return mixed
     */


    public static function garage($id='')
    {
        $data=DB::table('garages')
            ->leftJoin('provinces', 'provinces.province_id', '=', 'garages.province_id');
        if(!empty($id))
        {
            return $data->where('garage_id',$id)->first();
        }
        return $data->get();
    }
    public static function brand()
    {
        return DB::table('brands')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function category()
    {
        return DB::table('categories')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function service()
    {
        return DB::table('services')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function province()
    {
        return DB::table('provinces')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function insurance()
    {
        return DB::table('insurances')
            ->whereNull('deleted_at')
            ->get();
    }

    public static function garages_insurances()
    {
        $insurance=DB::table('garages_insurances')
            ->leftJoin('insurances','insurances.insurance_id','=','garages_insurances.insurance_id')
            ->whereNull('garages_insurances.deleted_at')
            ->get();
        $data=array();
        if(!empty($insurance))
        {
            foreach ($insurance as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }

    public static function garages_services()
    {
        $service=DB::table('garages_services')
            ->leftJoin('services','services.service_id','=','garages_services.service_id')
            ->whereNull('garages_services.deleted_at')
            ->get();
        $data=array();
        if(!empty($service))
        {
            foreach ($service as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }

    public static function garages_categories()
{
    $category=DB::table('garages_categories')
        ->leftJoin('categories','categories.category_id','=','garages_categories.category_id')
        ->whereNull('garages_categories.deleted_at')
        ->get();
    $data=array();
    if(!empty($category))
    {
        foreach ($category as $item)
        {
            $data[$item->garage_id][]=$item;
        }
    }
    return $data;
}
    public static function garages_brands()
    {
        $brand=DB::table('garages_brands')
            ->leftJoin('brands','brands.brand_id','=','garages_brands.brand_id')
            ->whereNull('garages_brands.deleted_at')
            ->get();

        $data=array();
        if(!empty($brand))
        {
            foreach ($brand as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }
    public static function images($id)
    {
        $image=DB::table('images')
            ->where('garage_id',$id)
            ->whereNull('images.deleted_at')
            ->get();
        $data=array();
        if(!empty($image))
        {
            foreach ($image as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }
}