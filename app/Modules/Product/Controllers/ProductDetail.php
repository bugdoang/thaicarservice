<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Product\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Product\Models\ProductModel;

class ProductDetail extends Controller
{
    public function index($id)
    {
        $garage_detail=ProductModel::garage($id);
        $insurances=ProductModel::garages_insurances();
        $services=ProductModel::garages_services();
        $categories=ProductModel::garages_categories();
        $brands=ProductModel::garages_brands();
        $images=ProductModel::images($id);
        return view ('product::product-detail',[
            'garage_detail'=>$garage_detail,
            'insurances'=>$insurances,
            'services'=>$services,
            'categories'=>$categories,
            'brands'=>$brands,
            'images'=>$images
        ]);
    }
}