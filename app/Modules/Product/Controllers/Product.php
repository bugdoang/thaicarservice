<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Product\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Home\Models\HomeModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Profile\Models\ProfileModel;
use Illuminate\Support\Facades\Input;

class Product extends Controller
{
    public function index()
    {
        $garage_name    = Input::get('garage_name'); //ค้นหาจากชื่อ
        $province_id    = Input::get('province_id'); //ค้นหาจากรหัสจังหวัด
        $category_id    = Input::get('category_id');
        $service_id     = Input::get('service_id');
        $brand_id       = Input::get('brand_id');
        $insurance_id   = Input::get('insurance_id');
        $images=HomeModel::image();


        $garage_all = HomeModel::search([
            'garage_name' =>$garage_name,
            'province_id' =>$province_id,
            'category_id' =>$category_id,
            'service_id' =>$service_id,
            'brand_id' =>$brand_id,
            'insurance_id' =>$insurance_id
        ]);
        $garage_all->appends(Input::only(
            'garage_name' ,
            'province_id',
            'category_id',
            'service_id',
            'brand_id',
            'insurance_id'
        ));//เวลากดหน้า ต่อไป ข้อมูลจะตามไปด้วย appends
//        dd($garage_all);

        $service=HomeModel::garage_service();
        $brand=HomeModel::garage_brand();
        $category=HomeModel::garage_category();
        $insurance=HomeModel::garage_insurance();
        return view ('product::product',[
            'garage_all'=>$garage_all,
            'service'=>$service,
            'brand' =>$brand,
            'category'=> $category,
            'insurance'=>$insurance,
            'images'=>$images

        ]);
    }
    public function search($type='',$id='')
    {
        $garage_all = HomeModel::garage([
            'type'=>$type,
            'id'  =>$id
        ]);
        $service=HomeModel::garage_service();
        $brand=HomeModel::garage_brand();
        $category=HomeModel::garage_category();
        $insurance=HomeModel::garage_insurance();
        $images=HomeModel::image();


        return view ('product::product',[
            'garage_all'=>$garage_all,
            'service'=>$service,
            'brand'=>$brand,
            'category'=>$category,
            'insurance'=>$insurance,
            'images'=>$images

        ]);
    }

}