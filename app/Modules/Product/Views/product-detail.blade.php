@extends('masterlayout')
@section('title','รายละเอียด'.$garage_detail->garage_name)
@section('content')
    <!-- Page Content -->
    <div class="row">
        <div class="col-lg-9 col-md-9">
            <div class="row">
                <h2 class="header-text myfont"><i class="fa fa-car" aria-hidden="true"></i> {{$garage_detail->garage_name}}</h2>
                <div class="row item-border" style="margin-left: 0px;">
                <!--เริ่มข้อมุลคำแนะอู่-->
                        <div class="row">
                            @if(!empty($garage_detail->detail))
                                <div class="col-lg-12 col-md-12">
                                    <div class="header-detail myfont line1">
                                        รายละเอียดอู่
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        {!! $garage_detail->detail !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                            <!--การบริการ-->
                @if(!empty($services[$garage_detail->garage_id]))
                    <div class="row">
                            <div class="form-group col-lg-12 col-md-12">
                                <div class="header-detail myfont line1">
                                    งานที่ให้บริการ
                                </div>
                                <div class="row"  style="margin-left: 0px;">
                                    @if(isset($services[$garage_detail->garage_id]))
                                        <table class="table table-striped table-bordered" style="margin-left: 15px; width: auto;">
                                            @foreach($services[$garage_detail->garage_id] as $item)
                                                <tr>
                                                    <td style="width: 200px;"><i class="fa fa-wrench" aria-hidden="true"></i> {{$item->service_name}}</td>
                                                    <td>{{$item->service_detail}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                @endif

                @if(!empty($categories[$garage_detail->garage_id]))
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">
                            <div class="header-detail myfont line1">
                                ประเภทรถที่รับซ่อม
                            </div>
                            <div class="row">
                                @if(isset($categories[$garage_detail->garage_id]))
                                    @foreach($categories[$garage_detail->garage_id] as $item)
                                        <div class="row car-brand col-lg-3 col-md-3">
                                            <div style="position:relative;height:80px;width:100%;text-align:center;border:1px solid #ccc;margin-bottom:6px;">
                                                <a title="{{$item->category_name}}">
                                                    <img style="position: absolute;top:0;bottom:0;margin:auto;left:0;right:0;width: 120px;" src="{{$item->image_category}}">
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endif

                @if(!empty($brands[$garage_detail->garage_id]))
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">
                            <div class="header-detail myfont line1">
                                ยี่ห้อรถที่รับซ่อม
                            </div>
                            <div class="row">
                                @if(isset($brands[$garage_detail->garage_id]))
                                    @foreach($brands[$garage_detail->garage_id] as $item)
                                        <div class="row car-brand col-lg-2 col-md-2">
                                            <div style="position:relative;height:80px;width:100%;text-align:center;border:1px solid #ccc;margin-bottom:6px;">
                                                <a title="{{$item->brand_name}}">
                                                    <img style="position: absolute;top:0;bottom:0;margin:auto;left:0;right:0;" src="{{$item->image_brand}}">
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
                    @if(!empty($insurances[$garage_detail->garage_id]))
                        <div class="row">
                            <div class="form-group col-lg-12 col-md-12">
                                <div class="header-detail myfont line1">
                                    บริษัทประกันที่รับซ่อม
                                </div>
                                <div class="row">
                                    @if(isset($insurances[$garage_detail->garage_id]))
                                        @foreach($insurances[$garage_detail->garage_id] as $item)
                                            <div class="row car-brand col-lg-2 col-md-2">
                                                <div style="position:relative;height:80px;width:100%;text-align:center;border:1px solid #ccc;margin-bottom:6px;">
                                                    <a title="{{$item->insurance_name}}">
                                                        <img style="position: absolute;top:0;bottom:0;margin:auto;left:0;right:0;" src="{{$item->image_insurance}}">
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
                <!------------------Map---------------------------------------->
            <div class="row">
                @if(!empty($images))
                    <h2 class="header-text myfont"><i class="fa fa-picture-o" aria-hidden="true"></i> รูปภาพ</h2>
                    <div class="row item-border" style="margin-left: 0px;">
                        @if(isset($images[$garage_detail->garage_id]))
                            @foreach($images[$garage_detail->garage_id] as $item)
                                <img src="{{$item->image_path}}" style="margin-left: 10px; margin-bottom: 10px; width: 400px; height: auto;">
                            @endforeach
                        @endif
                </div>
                @endif
            </div>
            <div class="row">
                @if(!empty($garage_detail->latitude && $garage_detail->longitude))
                    <h2 class="header-text myfont"><i class="fa fa-map-marker" aria-hidden="true"></i> แผนที่ </h2>
                    <div class="row item-border" style="margin-left: 0px;">
                        <div class="col-lg-12 col-md-12">
                            <div id="map_canvas" style="height: 350px;width: 97%;margin: 0.6em;"></div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 register-container1" >
                <a href="/reservation/{{$garage_detail->garage_id}}" class="button-1">
                    <h2 class="header-text button-1 myfont"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>จองอู่</h2>
                </a>
                <div class="item-border2">


                @if(!empty($garage_detail->open_garages))
                    <div class="row"  style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            วันทำการ :
                        </div>
                        <div class="col-lg-9 col-md-9"  style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->open_garages}}
                        </div>
                    </div>
                @endif
                @if(!empty($garage_detail->open_time && $garage_detail->close_time))
                    <div class="row"  style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            เปิด-ปิด :
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->open_time}} - {{$garage_detail->close_time}} น.
                        </div>
                    </div>
                @endif
                @if(!empty($garage_detail->province_name))
                    <div class="row"  style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            จังหวัด :
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->province_name}}
                        </div>
                    </div>
                @endif
                @if(!empty($garage_detail->mobile))
                    <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            โทรศัพท์ :
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->mobile}}
                        </div>
                    </div>
                @endif
                @if(!empty($garage_detail->email))
                    <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            อีเมล์ :
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->email}}
                        </div>
                    </div>
                @endif
                @if(!empty($garage_detail->website))
                    <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            เว็บไซต์ :
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->website}}
                        </div>
                    </div>
                @endif
                @if(!empty($garage_detail->address))
                    <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 10px;">
                        <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 0px;">
                            ที่ตั้ง :
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding-left: 0px; padding-right: 0px;">
                            {{$garage_detail->address}}
                        </div>
                    </div>

                @endif
                </div>
            </div>

            {!! App\Services\SearchBox::get() !!}
        </div>



    </div>

@stop
@push('scripts')
<script>
    $(function () {
        var lat ={!! (isset($garage_detail->latitude) && !empty($garage_detail->latitude))?$garage_detail->latitude:13.868148791978353 !!},
                lng ={!! (isset($garage_detail->longitude) && !empty($garage_detail->longitude))?$garage_detail->longitude:100.48204421975242 !!},
                latlng = new google.maps.LatLng(lat, lng),
                image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
        var mapOptions = {
                    center: new google.maps.LatLng(lat, lng),
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    panControl: true,
                    panControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.TOP_left
                    }
                },
                map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: image
                });
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
            $('.MapLat').val(place.geometry.location.lat());
            $('.MapLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function (event) {
            $('.MapLat').val(event.latLng.lat());
            $('.MapLon').val(event.latLng.lng());
            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status) {
                console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                            lng = results[0].geometry.location.lng(),
                            placeName = results[0].address_components[0].long_name,
                            latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                    $("#searchTextField").val(results[0].formatted_address);
                }
            });
        });

        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            infowindow.open(map, marker);
        }
    });
</script>
@endpush