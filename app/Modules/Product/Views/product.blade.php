@extends('masterlayout')
@section('title','รายการอู่ทั้งหมด')
@section('content')
<!-- Page Content -->
<div class="row">
    <div class="col-lg-9 col-md-9">
        <div class="row">
            <h2 class="header-text myfont">รายการอู่ทั้งหมด <span style="font-size: 18px">( {{$garage_all->total()}} รายการ )</span></h2>
            @if(count($garage_all)>0)
                @foreach($garage_all as $item)
                    <div class="row item-border" style="margin-left: 0px;">
                        <div   style=" margin-top: 0px; padding-top: 5px;">
                            <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 5px;">
                                <a class="img-au " href="/product-detail/{{$item->garage_id}}">
                                        @if(isset($images[$item->garage_id]))
                                            <div style="width:100%;height:100px;background-image: url('{{$images[$item->garage_id][0]->image_path}}');background-size:100% 100%;background-position:center;background-repeat: no-repeat"></div>
                                        @else
                                            <img style="height:133px; width: auto;" src="/assets/img/No-image-found.jpg">
                                        @endif
                                </a>
                                <a class="searching myfont color2" style="height: auto; margin-top: 10px;" href="/reservation/{{$item->garage_id}}">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    จอง
                                </a>

                            </div>
                            <div class="col-lg-9 col-md-9" style="padding-left: 0px; font-size: 12px; padding-right: 0px;">
                                <table class="table table-striped table-bordered" >
                                    <tr>
                                        <td colspan="2">
                                            <a href="/product-detail/{{$item->garage_id}}">
                                                <h2 class="name-au myfont" style="margin-left: 0px;"> {{$item->garage_name}}</h2>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 130px;">
                                            <i class="fa fa-home" aria-hidden="true"></i> ที่ตั้ง
                                        </td>
                                        <td>
                                            {{$item->address}} จังหวัด {{$item->province_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-phone" aria-hidden="true"></i> เบอร์โทรศัพท์
                                        </td>
                                        <td>
                                            {{$item->mobile}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i> อีเมล
                                        </td>
                                        <td>
                                            {{$item->email}}
                                        </td>
                                    </tr>
                                    @if(!empty($item->open_garages))
                                        <tr>
                                            @if($item->open_garages=='เปิดทุกวัน')
                                                <td>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i> วันเปิดทำการ
                                                </td>
                                                <td>
                                                    {{$item->open_garages}}
                                                </td>
                                            @else
                                                <td>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i> วันเปิดทำการ
                                                </td>
                                                <td>
                                                    {{$item->open_garages}}
                                                </td>
                                            @endif

                                        </tr>
                                    @endif
                                    <tr>
                                        @if(!empty($item->open_time && $item->close_time))
                                            <td>
                                                <i class="fa fa-clock-o" aria-hidden="true"></i> เวลาเปิด - ปิดร้าน
                                            </td>
                                            <td>
                                                {{$item->open_time}} - {{$item->close_time}} น.
                                            </td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-offset-3 col-md-offset-3">
                                    @if(!empty($service[$item->garage_id]))
                                        <div class="dropdown pull-left" style="margin-left:15px; margin-top:5px;">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px; background: #B00000;">รายการที่รับซ่อม
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                @if(isset($service[$item->garage_id]))
                                                    @if(isset($service[$item->garage_id]))
                                                        @foreach($service[$item->garage_id] as $item)
                                                            <li>
                                                                <a title="{{$item->service_name}}" href="/product/services/{{$item->service_id}}" style="font-size: 12px;">
                                                                    <i class="fa fa-wrench" aria-hidden="true"></i> {{$item->service_name}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </ul>
                                        </div>
                                    @endif

                                    @if(!empty($brand[$item->garage_id]))
                                            <div class="dropdown pull-left" style="margin-left:15px; margin-top:5px;">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px;background: #B00000;">ยี่ห้อรถที่รับซ่อม
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                @if(isset($brand[$item->garage_id]))
                                                    @if(isset($brand[$item->garage_id]))
                                                        @foreach($brand[$item->garage_id] as $item)
                                                            <li>
                                                                <a title="{{$item->brand_name}}" href="/product/brands/{{$item->brand_id}}" style="font-size: 12px;">
                                                                    <i class="fa fa-shield" aria-hidden="true"></i> {{$item->brand_name}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </ul>
                                        </div>
                                    @endif
                                    @if(!empty($category[$item->garage_id]))
                                            <div class="dropdown pull-left" style="margin-left:15px; margin-top:5px;">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px;background: #B00000;">ประเภทรถที่รับซ่อม
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                @if(isset($category[$item->garage_id]))
                                                    @if(isset($category[$item->garage_id]))
                                                        @foreach($category[$item->garage_id] as $item)
                                                            <li>
                                                                <a title="{{$item->category_name}}" href="/product/categories/{{$item->category_id}}" style="font-size: 12px;">
                                                                    <i class="fa fa-car" aria-hidden="true"></i> {{$item->category_name}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </ul>
                                        </div>
                                    @endif
                                    @if(!empty($insurance[$item->garage_id]))
                                            <div class="dropdown pull-left" style="margin-left:15px; margin-top:5px;">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px;background: #B00000;">บริษัทประกันที่รับซ่อม
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                @if(isset($insurance[$item->garage_id]))
                                                    @if(isset($insurance[$item->garage_id]))
                                                        @foreach($insurance[$item->garage_id] as $item)
                                                            <li>
                                                                <a title="{{$item->insurance_name}}" href="/product/insurances/{{$item->insurance_id}}" style="font-size: 12px;">
                                                                    <i class="fa fa-tag" aria-hidden="true"></i> {{$item->insurance_name}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
    .
                        </div>
                    </div>
                @endforeach
            @else
                <div class="item-border row"  style="margin-left: 0px; width: 877px; margin-top: 0px; padding: 5px 20px;">
                <p style="margin-top: 10px;display: block; text-align: center; ">ไม่พบข้อมูล</p>
                </div>
            @endif
        </div>
        <div style="text-align: center">
            {!! $garage_all->render() !!}
        </div>
    </div>
    {!! App\Services\SearchBox::get() !!}

</div>
@stop