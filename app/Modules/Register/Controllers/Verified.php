<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/9/2559
 * Time: 14:38
 */

namespace App\Modules\Register\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Register\Models\RegisterModel;

class Verified extends Controller
{
    public function index($verified_key='')
    {
        if(strlen($verified_key)==32)//ถ้าความยาวเวอริไฟคีย์มีความยาวเท่ากับ 32 ตัวอักษร
        {
            $verified=RegisterModel::verified($verified_key);//ให้ส่งค่าเวอริไฟคียไปเช็คในฟังชั่นเวอริไฟในรีจิสเตอร์โมเดลแล้วนำค่าที่ได้กลับมาส่งคืนมาเก็บไวในตัวแปร
            if(!empty($verified))//เช็คค่าว่าถ้าตัวแปรเวอริไฟมีค่า ให้ส่งค่าเมมเบอไอดีในตัวแปรเวอริไฟไปทำงานตามฟังชั่น เซ็ทแอคทีฟ
            {
                RegisterModel::set_active($verified->member_id);
            }
            else
            {
                return view ('register::verifiederror',[
                    'verified'=>$verified
                ]);
            }
        }
        else
        {
            return view ('register::verifiederror');
        }
        return view ('register::verified',[
            'verified'=>$verified
        ]);
    }
}