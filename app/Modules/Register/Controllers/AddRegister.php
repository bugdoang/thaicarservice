<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19/9/2559
 * Time: 16:02
 */

namespace App\Modules\Register\Controllers;


use App\Helepers\DateFormat;
use App\Modules\Register\Models\RegisterModel;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AddRegister extends Controller
{
    public function index (Request $request)
    {

        $title_name =$request->get('title_name');
        $first_name =$request->get('first_name');
        $last_name  =$request->get('last_name');
        $dob        =$request->get('dob');
        $card_id    =$request->get('card_id');
        $gender     =$request->get('gender');
        $mobile     =$request->get('mobile');
        $address    =$request->get('address');
        $province_id=$request->get('province_id');
        $email      =$request->get('email');
        $password   =$request->get('password');
        $re_password   =$request->get('re_password');

        $is_exist   =RegisterModel::check_email($email);
        $check=array('title_name','first_name','last_name','dob','card_id','gender','mobile','address','province_id','email','password','re_password');
        foreach ($check as $item)
        {
            if ($request->get ($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ'],422);
            }
        }

        if (!is_numeric($province_id))
        {
            return response(['กรุณาป้อนจังหวัดให้ถูกต้องด้วยค่ะ']);
        }
        if(!empty($is_exist))
        {
            return response(['อีเมลนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        if($password!=$re_password)
        {
            return response(['การยืนยันรหัสผ่านไม่ถูกต้อง กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        $key=md5($email.microtime().config('app.key'));
        $data = array(
            'title_name'    => $title_name,
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'dob'           => DateFormat::engDate($dob),
            'card_id'       => $card_id,
            'gender'        => $gender,
            'address'       => $address,
            'mobile'        => $mobile,
            'province_id'   => $province_id,
            'email'         => $email,
            'verify_key'    => $key,
            'password'      => Hash::make($password),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );

        $member_id=DB::table('members')->insertGetId($data);
        return response(['สมัครสมาชิกเรียบร้อยแล้วค่ะ']);
    }
}