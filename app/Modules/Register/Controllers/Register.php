<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Register\Controllers;


use App\Modules\Province\Models\ProvinceModel;
use Illuminate\Routing\Controller;

class Register extends Controller
{
public function index()
{
    $title_names = config('myconfig.title_names');
    $provinces=ProvinceModel::getAll();
//    dd($provinces);
    return view ('register::register',[
        'title_names'=>$title_names,
        'provinces'=>$provinces
    ]);
}
}