<?php
namespace App\Modules\Register\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 19/9/2559
 * Time: 16:13
 */
class RegisterModel extends Model
{
    public static function check_email($email)
    {
        return DB::table('members')
            ->where('email',$email)
            ->first();
    }
    public static function member_name($first_name)
    {
        return DB::table('members')
            ->where('first_name',$first_name)
            ->first();
    }
//    public static function verified($verified_key)
//    {
//        return DB::table('members')
//            ->where('verify_key',$verified_key)//เลือกข้อมูลทั้งหมดของเมมเบอร์มาโดยค่า เวอริไฟคีย์(ตัวแรก ค่าในดาต้าเบส) ต้องเท่ากับค่า เวอริไฟคีย์ที่ส่งมา (ตัวที่2)
//            ->first();
//    }
//    public static function set_active($member_id)
//    {
//        return DB::table('members')
//            ->where('member_id',$member_id)
//            ->update(['status_member'=>'verified']);
//    }
}