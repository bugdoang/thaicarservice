@extends('masterlayout')
@section('title','ยืนยันการสมัครสมาชิก สำเร็จ')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มซ้าย-->
        <!--เริ่มกลาง-->

            <div id="box" style="margin-top:50px; padding-bottom: 238px;" class="mainbox col-md-6 col-md-offset-3  col-sm-8 col-sm-offset-2">
                <div class="panel panel-info" >
                    <div class="header-success myfont ">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        ยืนยันการสมัครสมาชิกสำเร็จ
                    </div>

                    <div style="padding-top:30px" class="panel-body" >
                        <div class="row" style="margin: 0px; font-size: 18px">
                            สวัสดีคุณ {{$verified->first_name}} {{$verified->last_name}}
                        </div>
                        <div style="border-top: 1px solid#888;" ></div>
                        <div class="row" style="margin: 0px">
                            <br>ระบบได้ทำการยืนยันการสมัครสมาชิกของคุณเรียบร้อยแล้ว ยินต้อนรับเข้าสู่ Thaicarservice !
                        </div>

                    </div>
                </div>
            </div>
    </div>
    @stop