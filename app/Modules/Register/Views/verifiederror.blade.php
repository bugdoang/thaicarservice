@extends('masterlayout')
@section('title','ยืนยันการสมัครสมาชิก ไม่สำเร็จ')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <div id="box" style="margin-top:50px; padding-bottom: 255px;" class="mainbox col-md-6 col-md-offset-3  col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="header-text myfont ">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    ยืนยันการสมัครสมาชิกไม่สำเร็จ
                </div>
                <div style="padding-top:30px" class="panel-body" >
                    <div class="row" style="margin: 0px; font-size: 18px; text-align: center;">
                        รหัสยืนยันการสมัครสมาชิกไม่ถูกต้อง กรุณาลองใหม่อีกครั้งค่ะ
                    </div>
                </div>
                <div class="row" style="margin: 0px; text-align: center;">
                    <a href="/">กลับหน้าหลัก</a>
                </div>
            </div>
        </div>
    </div>
    @stop