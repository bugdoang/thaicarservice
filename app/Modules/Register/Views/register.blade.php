@extends('masterlayout')
@section('title','สมัครสมาชิก')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มซ้าย-->
        <div class="register-container1 col-lg-3 col-md-3">
            <div class="panel-title">
                <h2 class="header-text myfont color2">คำแนะนำ</h2>
            </div>
            <div class="item-border">
                <ul class="register-suggestion">
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        กรุณากรอกข้อมูลให้ครบทุกช่อง เพื่อสิทธิประโยชน์ในการใช้งานของตัวคุณเอง
                    </li>

                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        กรุณาใช้อีเมลจริง เพราะต้องนำไปใช้ยืนยันการสมัครสมาชิก
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        คุณสามารถแก้ไขข้อมูลส่วนตัวได้ ภายหลังจากที่ได้สมัครสมาชิกเรียบร้อยแล้ว
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        เมื่อคุณสมัครสมาชิกเรียบร้อยแล้ว คุณสามารถใช้งานเว็บไซต์ในหมวดต่างๆ ได้ตามความต้องการ
                    </li>
                    <li>
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                        ไม่ควรให้ข้อมูล Login และ Password แก่ผู้อื่นเป็นอันขาด
                    </li>
                </ul>
            </div>
        </div>
        <!--เริ่มกลาง-->
        <div class="register-container col-lg-9 col-md-9">
            <div class="panel-title">
                <h2 class="header-text myfont color2"><i class="fa fa-user" aria-hidden="true"></i> สมัครสมาชิก</h2>
            </div>
                <form action="/register" method="post" id="fileForm" role="form" class="item-border1">
                    {{ csrf_field() }}
                        <!--เริ่ม-->
                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <h2 class="header1-text myfont line">ข้อมูลส่วนตัว</h2>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-2 col-lg-2">
                                    <label>คำนำหน้าชื่อ<span style="color:red;"> * </span> : </label>
                                    <select class="form-control" name="title_name" id="title_name">
                                        <option value="">กรุณาเลือก</option>
                                        @if(!empty($title_names))
                                            @foreach($title_names as $title_name)
                                                <option value="{{$title_name}}">
                                                    {{$title_name}}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-5 col-lg-5">
                                    <label>ชื่อ <span style="color:red;"> * </span> : </label>
                                    <input class="form-control" type="text" name="first_name" id = "first_name" />
                                </div>
                                <div class="form-group col-md-5 col-lg-5">
                                    <label>นามสกุล <span style="color:red;"> * </span> : </label>
                                    <input class="form-control" type="text" name="last_name" id = "last_name" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-2 col-lg-2">
                                    <label>เพศ <span style="color:red;"> * </span> : </label></br>
                                    <input name="gender"  type="radio" value="M"> ชาย &nbsp; &nbsp; &nbsp;
                                    <input name="gender"  type="radio" value="F"> หญิง
                                </div>
                                <div class="form-group col-md-5 col-lg-5" style="margin-bottom: 0px;">
                                    <label>วัน/เดือน/ปี เกิด <span style="color:red;"> * </span> : </label>
                                    <input class="form-control datepicker" type="text" name="dob" id = "dob"  />
                                </div>

                                <div class="form-group col-md-5 col-lg-5">
                                    <label>เลขบัตรประชาชน<span style="color:red;"> * </span> : </label>
                                    <input class="form-control" type="text" name="card_id" id = "card_id" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 col-lg-12">
                                    <label>ที่อยู่<span style="color:red;"> * </span> :</label>
                                    <input class="form-control" type="text" name="address" id = "address" />
                                    <div id="errLast"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3 col-lg-3">
                                    <label>จังหวัด<span style="color:red;"> * </span> : </label>
                                    <select class="form-control" name="province_id" id="province_id">
                                        <option value="">กรุณาเลือก</option>
                                        @if(!empty($provinces))
                                            @foreach($provinces as $province)
                                                <option value="{{$province->province_id}}">
                                                    {{$province->province_name}}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-lg-4">
                                    <label>เบอร์โทรศัพท์<span style="color:red;"> * </span> : </label>
                                    <input class="form-control" type="text" name="mobile" id = "mobile"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <h2 class="header1-text myfont line" style="margin-top: 0px;">ข้อมูลระบบ</h2>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-8 col-lg-8">
                                    <label>อีเมล์<span style="color:red;"> * </span> : </label>
                                    <input class="form-control" type="text" name="email" id = "email" /> </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-8 col-lg-8">
                                        <label>รหัสผ่าน<span style="color:red;"> * </span> : </label>
                                        <input name="password" type="password" class="form-control inputpass" minlength="4" maxlength="16"  id="pass1" /></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-8 col-lg-8">
                                    <label>ยืนยันรหัสผ่าน<span style="color:red;"> * </span> : </label>
                                    <input name="re_password" type="password" class="form-control inputpass" minlength="4" maxlength="16"  id="pass2" onkeyup="checkPass(); return false;" />
                                    <span id="confirmMessage" class="confirmMessage"></span>
                                </div>
                            </div>
                            <input type="hidden" value="0" name="activate" />
                            <input type="checkbox" required name="terms" onchange="this.setCustomValidity(validity.valueMissing ? 'Please indicate that you accept the Terms and Conditions' : '');" id="field_terms">  
                            <label for="terms">ยอมรับเงื่อนไขการใช้บริการ</label>
                            <div class="form-group">
                                <input class="btn btn-success" type="submit" name="submit_reg" value="สมัครสมาชิก">
                            </div>
                            <h5>โปรดกรอกแบบฟอร์มสมัครสมาชิกตามความเป็นจริง </h5>
                        </div>

                </form><!-- ends register form -->
            <script type="text/javascript">
                document.getElementById("field_terms").setCustomValidity("Please indicate that you accept the Terms and Conditions");
            </script>
        </div>
    </div>
    @stop
@push('scripts')
<script>
    function callback_ajax(data)
    {
        MessageBox.alert(data[0],function () {
            window.location.href='/login' ;
        });
    }
</script>
@endpush