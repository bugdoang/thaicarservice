<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Home\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class HomeModel extends Model
{
    /**
     * @return mixed
     */

    public static function garage($param=[],$limit=5)
    {
        $type=(isset($param['type']))?$param['type']:'';
        $id=(isset($param['id']))?$param['id']:'';
        $status_show = config('myconfig.garage_show_type');
        $data=DB::table('garages')
            ->whereNull('garages.deleted_at')
            ->leftJoin('provinces', 'provinces.province_id', '=', 'garages.province_id');

        if($status_show==2 || $type=='brands')
        {
            $data->whereExists(function ($query) use ($id,$type){
                $query->select(DB::raw(1))
                      ->from('garages_brands');
                if(is_numeric($id) && $type=='brands')
                {
                    $query->where('brand_id',$id);
                }
                $query->whereNull('deleted_at')
                      ->whereRaw('garages_brands.garage_id = garages.garage_id');
            });
        }
        if($status_show==2 || $type=='categories')
        {
            $data->whereExists(function ($query) use ($id,$type){
                $query->select(DB::raw(1))
                    ->from('garages_categories');
                      if(is_numeric($id) && $type=='categories')
                      {
                          $query->where('category_id',$id);
                      }
                $query->whereNull('deleted_at')
                    ->whereRaw('garages_categories.garage_id = garages.garage_id');
            });
        }
        if($type=='insurances')
        {
            $data->whereExists(function ($query) use ($id,$type){
                $query->select(DB::raw(1))
                      ->from('garages_insurances');
                       if(is_numeric($id))
                       {
                           $query->where('insurance_id',$id && $type=='insurances');
                       }
                $query ->whereNull('deleted_at')
                       ->whereRaw('garages_insurances.garage_id = garages.garage_id');
            });
        }
        if($status_show==2 || $type=='services')
        {
            $data->whereExists(function ($query) use ($id,$type){
                $query->select(DB::raw(1))
                    ->from('garages_services');
                        if(is_numeric($id) && $type=='services')
                        {
                            $query->where('service_id',$id);
                        }
                $query->whereNull('deleted_at')
                      ->whereRaw('garages_services.garage_id = garages.garage_id');
            });
        }

        $data->orderBy('garage_id','DESC');//DESC=ล่าสุด,ASC=เรียงจากน่้อยไปมาก ก-ฮ
//        dd('xxx');
        return $data->paginate($limit);
    }
    public static function search($param=[])
    {
        $garage_name=(isset($param['garage_name']))?$param['garage_name']:'';
        $province_id=(isset($param['province_id']) && is_numeric($param['province_id']))?$param['province_id']:'';
        $category_id=(isset($param['category_id']) && is_numeric($param['category_id']))?$param['category_id']:'';
        $service_id=(isset($param['service_id']) && is_numeric($param['service_id']))?$param['service_id']:'';
        $brand_id=(isset($param['brand_id']) && is_numeric($param['brand_id']))?$param['brand_id']:'';
        $insurance_id=(isset($param['insurance_id']) && is_numeric($param['insurance_id']))?$param['insurance_id']:'';
        $status_show = config('myconfig.garage_show_type');

        $data=DB::table('garages')
            ->whereNull('garages.deleted_at')
            ->leftJoin('provinces', 'provinces.province_id', '=', 'garages.province_id');



        if(!empty($garage_name))
        {
            $data->where('garages.garage_name','LIKE','%'.$garage_name.'%');
        }
        if(!empty($province_id))
        {
            $data->where('garages.province_id',$province_id);
        }
        if($status_show==2 || !empty($brand_id))
        {
            $data->whereExists(function ($query) use ($brand_id){
                $query->select(DB::raw(1))
                        ->from('garages_brands');
                if(is_numeric($brand_id))
                {
                    $query->where('brand_id',$brand_id);
                }
                $query->whereNull('deleted_at')
                    ->whereRaw('garages_brands.garage_id = garages.garage_id');
            });
        }

        if($status_show==2 || !empty($category_id))
        {
            $data->whereExists(function ($query) use ($category_id){
                $query->select(DB::raw(1))
                    ->from('garages_categories');
                      if(is_numeric($category_id))
                      {
                          $query->where('category_id',$category_id);
                      }
                $query->whereNull('deleted_at')
                    ->whereRaw('garages_categories.garage_id = garages.garage_id');
            });
        }
        if(!empty($insurance_id))
        {
            $data->whereExists(function ($query) use ($insurance_id){
                $query->select(DB::raw(1))
                    ->from('garages_insurances');
                     if(is_numeric($insurance_id))
                     {
                         $query->where('insurance_id',$insurance_id);
                     }
                $query->whereNull('deleted_at')
                    ->whereRaw('garages_insurances.garage_id = garages.garage_id');
            });
        }
        if($status_show==2 || !empty($service_id))
        {
            $data->whereExists(function ($query) use ($service_id){
                $query->select(DB::raw(1))
                    ->from('garages_services');
                     if(is_numeric($service_id))
                     {
                         $query->where('service_id',$service_id);
                     }
                $query->whereNull('deleted_at')
                    ->whereRaw('garages_services.garage_id = garages.garage_id');
            });
        }
        return $data->paginate(5);
    }


    public static function garage_service()
    {
        $service=DB::table('garages_services')
            ->leftJoin('services','services.service_id','=','garages_services.service_id')
            ->whereNull('garages_services.deleted_at')
            ->get();
        $data=array();
        if(!empty($service))
        {
            foreach ($service as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }
    public static function garage_brand()
    {
        $brand=DB::table('garages_brands')
            ->leftJoin('brands','brands.brand_id','=','garages_brands.brand_id')
            ->whereNull('garages_brands.deleted_at')
            ->get();
        $data=array();
        if(!empty($brand))
        {
            foreach ($brand as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }
    public static function garage_category()
    {
        $category=DB::table('garages_categories')
            ->leftJoin('categories','categories.category_id','=','garages_categories.category_id')
            ->whereNull('garages_categories.deleted_at')
            ->get();
        $data=array();
        if(!empty($category))
        {
            foreach ($category as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }
    public static function garage_insurance()
    {
        $insurance=DB::table('garages_insurances')
            ->leftJoin('insurances','insurances.insurance_id','=','garages_insurances.insurance_id')
            ->whereNull('garages_insurances.deleted_at')
            ->get();
        $data=array();
        if(!empty($insurance))
        {
            foreach ($insurance as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }

    public static function brand()
    {
        return DB::table('brands')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function category()
    {
        return DB::table('categories')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function insurance()
    {
        return DB::table('insurances')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function service()
    {
        return DB::table('services')
            ->whereNull('deleted_at')
            ->get();
    }
    public static function image()
    {
        $images=DB::table('images')
            ->whereNull('images.deleted_at')
            ->get();
        $data=array();
        if(!empty($images))
        {
            foreach ($images as $item)
            {
                $data[$item->garage_id][]=$item;
            }
        }
        return $data;
    }
}