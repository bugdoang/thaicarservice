@extends('masterlayout')
@section('title','หน้าหลัก')
@section('content')

<!-- Page Content -->
<!--แบนเนอร์-->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <img style="margin: 10px auto;display: block" width="728" height="90" border="0" class="img_ad" alt="" src="/uploads/default/banner.gif">
        </div>
    </div>
    <div class="row">
        <!-- เริ่มซ้าย-->
        <div class=" col-lg-9 col-md-9">
            <div class="row">
                <h2 class="header-text myfont color2"><i class="fa fa-bars" aria-hidden="true"></i> รายการอู่ล่าสุด</h2>
                @if(!empty($garage_all))
                @foreach($garage_all as $garage_item)
                    <div class="row item-border" style="margin-left: 0px;">
                        <div   style=" margin-top: 0px; padding-left: 20px; padding-right: 20px; padding-top: 5px;">
                            <div class="row">
                                <div class="col-lg-3 col-md-3" style="padding-left: 0px; padding-right: 5px;">
                                    <a class="img-au " href="/product-detail/{{$garage_item->garage_id}}">
                                        @if(isset($images[$garage_item->garage_id]))
                                            {{--<img style="height: auto;max-width:100%" src="{{$images[$garage_item->garage_id][0]->image_path}}">--}}
                                            <div style="width:100%;height:100px;background-image: url('{{$images[$garage_item->garage_id][0]->image_path}}');background-size:100% 100%;background-position:center;background-repeat: no-repeat"></div>
                                        @else
                                            <img style="height:133px; width: auto;" src="/assets/img/No-image-found.jpg">
                                        @endif
                                    </a>
                                    <a class="searching myfont color2" style="height: auto; margin-top: 10px;" href="/reservation/{{$garage_item->garage_id}}">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        จอง
                                    </a>
                                </div>
                                <div class="col-lg-9 col-md-9" style="padding-left: 0px; font-size: 12px; padding-right: 0px;">
                                    <table class="table table-striped table-bordered" >
                                        <tr>
                                            <td colspan="2">
                                                <a href="/product-detail/{{$garage_item->garage_id}}">
                                                    <h2 class="name-au myfont" style="margin-left: 0px;"> {{$garage_item->garage_name}}</h2>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 130px;">
                                                <i class="fa fa-home" aria-hidden="true"></i> ที่ตั้ง
                                            </td>
                                            <td>
                                                {{$garage_item->address}} จังหวัด {{$garage_item->province_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-phone" aria-hidden="true"></i> เบอร์โทรศัพท์
                                            </td>
                                            <td>
                                                {{$garage_item->mobile}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-envelope-o" aria-hidden="true"></i> อีเมล
                                            </td>
                                            <td>
                                                {{$garage_item->email}}
                                            </td>
                                        </tr>
                                        @if(!empty($garage_item->open_garages))
                                            <tr>
                                                @if($garage_item->open_garages=='เปิดทุกวัน')
                                                    <td>
                                                        <i class="fa fa-calendar" aria-hidden="true"></i> วันเปิดทำการ
                                                    </td>
                                                    <td>
                                                        {{$garage_item->open_garages}}
                                                    </td>
                                                @else
                                                    <td>
                                                        <i class="fa fa-calendar" aria-hidden="true"></i> วันเปิดทำการ
                                                    </td>
                                                    <td>
                                                        {{$garage_item->open_garages}}
                                                    </td>
                                                @endif

                                            </tr>
                                        @endif
                                        <tr>
                                            @if(!empty($garage_item->open_time && $garage_item->close_time))
                                            <td>
                                                <i class="fa fa-clock-o" aria-hidden="true"></i> เวลาเปิด - ปิดร้าน
                                            </td>
                                            <td>
                                                {{$garage_item->open_time}} - {{$garage_item->close_time}} น.
                                            </td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                                <div class="row" style="margin-left: 0px; margin-right: 0px;">
                                    <div class="col-lg-offset-3 col-md-offset-3">
                                        @if(!empty($service[$garage_item->garage_id]))
                                            <div class="dropdown pull-left" style="margin-left:15px; margin-top:5px;">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px; background: #B00000;">รายการที่รับซ่อม
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    @if(isset($service[$garage_item->garage_id]))
                                                        @if(isset($service[$garage_item->garage_id]))
                                                            @foreach($service[$garage_item->garage_id] as $item)
                                                                <li>
                                                                    <a title="{{$item->service_name}}" href="/product/services/{{$item->service_id}}" style="font-size: 12px;">
                                                                        <i class="fa fa-wrench" aria-hidden="true"></i> {{$item->service_name}}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </ul>
                                            </div>
                                        @endif

                                        @if(!empty($brand[$garage_item->garage_id]))
                                            <div class="dropdown pull-left"  style="margin-left:15px; margin-top:5px;">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px;background: #B00000;">ยี่ห้อรถที่รับซ่อม
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    @if(isset($brand[$garage_item->garage_id]))
                                                        @if(isset($brand[$garage_item->garage_id]))
                                                            @foreach($brand[$garage_item->garage_id] as $item)
                                                                <li>
                                                                    <a title="{{$item->brand_name}}" href="/product/brands/{{$item->brand_id}}" style="font-size: 12px;">
                                                                        <i class="fa fa-shield" aria-hidden="true"></i> {{$item->brand_name}}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </ul>
                                            </div>
                                        @endif
                                        @if(!empty($category[$garage_item->garage_id]))
                                            <div class="dropdown pull-left"  style="margin-left:15px; margin-top:5px;">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px;background: #B00000;">ประเภทรถที่รับซ่อม
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    @if(isset($category[$garage_item->garage_id]))
                                                        @if(isset($category[$garage_item->garage_id]))
                                                            @foreach($category[$garage_item->garage_id] as $item)
                                                                <li>
                                                                    <a title="{{$item->category_name}}" href="/product/categories/{{$item->category_id}}" style="font-size: 12px;">
                                                                        <i class="fa fa-car" aria-hidden="true"></i> {{$item->category_name}}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </ul>
                                            </div>
                                        @endif
                                        @if(!empty($insurance[$garage_item->garage_id]))
                                            <div class="dropdown pull-left" style="margin-left:15px; margin-top:5px;">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"  style="height: 30px;background: #B00000;">บริษัทประกันที่รับซ่อม
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    @if(isset($insurance[$garage_item->garage_id]))
                                                        @if(isset($insurance[$garage_item->garage_id]))
                                                            @foreach($insurance[$garage_item->garage_id] as $item)

                                                                    <li>
                                                                        <a title="{{$item->insurance_name}}" href="/product/insurances/{{$item->insurance_id}}" style="font-size: 12px;">
                                                                            <i class="fa fa-tag" aria-hidden="true"></i> {{$item->insurance_name}}
                                                                        </a>
                                                                    </li>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
                    <a class=" pull-right" href="/product"  style="margin-top: -16px;">
                        ดูรายการอู่ทั้งหมด
                    </a>
                @else
                    <p>ไม่พบรายการอู่ในระบบ</p>
                @endif
            </div>
            <div class="row">
                <h2 class="header1-text myfont">ค้นหาตามยี่ห้อ</h2>
                <h2 class="line"></h2>
                @for($i=0;$i<25;$i++)
                    @if(!isset($brands[$i]))
                        @break
                    @endif
                        @if($brands[$i]->brand_name =='ไม่ระบุ')
                        @else
                            <div class="row car-brand col-lg-2 col-md-2">
                                <div style="position:relative;height:80px;width:100%;text-align:center;border:1px solid #ccc;margin-bottom:6px;">
                                    <a title="{{$brands[$i]->brand_name}}" href="/product/brands/{{$brands[$i]->brand_id}}">
                                        <img style="position: absolute;top:0;bottom:0;margin:auto;left:0;right:0;" src="{{$brands[$i]->image_brand}}">
                                    </a>
                                </div>
                            </div>
                        @endif
                @endfor

            </div>
            <div class="row">
                <h2 class="header1-text myfont">ค้นหาตามประเภทรถ</h2>
                <h2 class="line"></h2>
                @for($i=0;$i<10;$i++)
                    @if(!isset($categories[$i]))
                        @break
                    @endif
                        @if($categories[$i]->category_name =='ไม่ระบุ')
                        @else
                    <div class="row car-brand col-lg-3 col-md-3">
                        <div style="position:relative;height:80px;width:100%;text-align:center;border:1px solid #ccc;margin-bottom:6px;">
                            <a title="{{$categories[$i]->category_name}}" href="/product/categories/{{$categories[$i]->category_id}}">
                                <img style="position: absolute;top:0;bottom:0;margin:auto;left:0;right:0;width: 120px;" src="{{$categories[$i]->image_category}}">
                            </a>
                        </div>
                    </div>
                        @endif
                @endfor
            </div>
            <div class="row">
                <h2 class="header1-text myfont">ค้นหาตามบริษัทประกันภัย</h2>
                <h2 class="line"></h2>
                @for($i=0;$i<25;$i++)
                    @if(!isset($insurances[$i]))
                        @break
                    @endif
                    <div class="row car-brand col-lg-2 col-md-2">
                        @if($insurances[$i]->insurance_name =='ไม่ระบุ')
                        @else
                        <div style="position:relative;height:80px;width:100%;text-align:center;border:1px solid #ccc;margin-bottom:6px;">
                            <a title="{{$insurances[$i]->insurance_name}}" href="/product/insurances/{{$insurances[$i]->insurance_id}}">
                                <img style="position: absolute;top:0;bottom:0;margin:auto;left:0;right:0;" src="{{$insurances[$i]->image_insurance}}">
                            </a>
                        </div>
                        @endif
                    </div>
                @endfor
            </div>
        </div>
        <!-- จบซ้าย-->
        {!! App\Services\SearchBox::get() !!}
    </div>

@stop