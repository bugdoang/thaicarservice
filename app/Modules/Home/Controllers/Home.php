<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Home\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Home\Models\HomeModel;
use App\Modules\Product\Controllers\ProductDetail;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Province\Models\ProvinceModel;
use DB;

class Home extends Controller
{
/*public function index()
{
    return view ('home::home');
}*/
    public function index()
    {
        $garage_all = HomeModel::garage([],4);
        $data=array(
            'garage_all'=>$garage_all,
        );
        $service=HomeModel::garage_service();
        $brand=HomeModel::garage_brand();
        $category=HomeModel::garage_category();
        $insurance=HomeModel::garage_insurance();
        $brands=HomeModel::brand();
        $categories=HomeModel::category();
        $insurances=HomeModel::insurance();
        $services=HomeModel::service();
        $images=HomeModel::image();
//        dd($insurances);
        return view('home::home',[
            'garage_all'=>$garage_all,
            'data'=>$data,
            'service'=>$service,
            'brand'=>$brand,
            'category'=>$category,
            'insurance'=>$insurance,
            'brands'=>$brands,
            'categories'=>$categories,
            'insurances'=>$insurances,
            'services'=>$services,
            'images'=>$images
        ]);
    }

}