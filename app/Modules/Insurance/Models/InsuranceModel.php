<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Insurance\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InsuranceModel extends Model
{
    /**
     * @return mixed
     */
    public static function getAllInsurance()
{
    return DB::table('insurances')->whereNull('deleted_at')->get();
}
}