<?php
namespace App\Modules\Contacts\Models;
use App\Helepers\DateFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/10/2559
 * Time: 17:31
 */
class ContactModel extends Model
{
    public static function  contact_me($param=[])
    {
        $first_name=(isset($param['first_name']))?$param['first_name']:'';
        $last_name=(isset($param['last_name']))?$param['last_name']:'';
        $created_at=(isset($param['created_at']))?$param['created_at']:'';
        $contact=(isset($param['contact']))?$param['contact']:'';
        $status=(isset($param['status']))?$param['status']:'';
        $created_at=DateFormat::engDate($created_at);
        $data=DB::table('contacts')
            ->whereNull('contacts.deleted_at')
            ->orderBy('contacts.contact_id','DESC');
        if(!empty($first_name))
        {
            $data->where('first_name','LIKE','%'.$first_name.'%');
        }
        if(!empty($last_name))
        {
            $data->where('last_name','LIKE','%'.$last_name.'%');
        }
        if(!empty($created_at))
        {
            $data->where('created_at','>=',$created_at.' 00:00:00');
            $data->where('created_at','<=',$created_at.' 23:59:59');
        }
        if(in_array($contact,['instructions','inform','add_data']))
        {
            $data->where('contact',$contact);
        }
        if(in_array($status,['unread','read']))
        {
            $data->where('status',$status);
        }
        return $data->paginate(10);
    }
    public static function  contact($contact_id)
    {
        return DB::table('contacts')
            ->where('contacts.contact_id',$contact_id)
            ->whereNull('contacts.deleted_at')
            ->first();
    }
    public static function update_contact($contact_id,$item)
    {
        return DB::table('contacts')
            ->where('contact_id',$contact_id)
            ->update($item);
    }
    public static function contact_count()
    {
        $sql="select status ,count(status) as total from contacts  where deleted_at is null  group by status";
        return DB::select($sql);
    }
}