<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/8/2559
 * Time: 11:46
 */
Route::group(['middleware'=>['auth']],function(){

Route::get('/contact/admin','\App\Modules\Contacts\Controllers\ContactAdmin@index');
Route::get('/contact/view/{contact_id}','\App\Modules\Contacts\Controllers\ContactAdmin@view');
Route::put('/contact/view/{contact_id}','\App\Modules\Contacts\Controllers\ContactAdmin@view_read');
});
Route::get('/contact','\App\Modules\Contacts\Controllers\Contact@index');
Route::post('/send_contact','\App\Modules\Contacts\Controllers\Contact@send');


