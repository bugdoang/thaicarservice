<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2"><i class="fa fa-search" aria-hidden="true"></i> ค้นหาข้อความ</h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/contact/admin">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่อผู้ส่ง" type="text" name="first_name" class="form-control" value="{{$first_name}}">

                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  นามสกุลผู้ส่ง" type="text" name="last_name" class="form-control" value="{{$last_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  วันที่ส่ง" type="text" name="created_at" class="form-control datepicker" value="{{$created_at}}">
                </div>
            </div>
            <div class="row form-group"  style="margin-top: -48px;" >
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกหัวข้อ" name="contact">
                        <option value="" ></option>
                        @if(!empty($contact_thai))
                            @foreach($contact_thai as $key=>$item)
                                <option {{($contact==$key)?' selected ':''}} value="{{$key}}">
                                    {{$item}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกสถานะ" name="status">
                        <option value="" ></option>
                        @if(!empty($read_thai))
                            @foreach($read_thai as $key=>$item)
                                <option {{($status==$key)?' selected ':''}} value="{{$key}}">
                                    {{$item}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>