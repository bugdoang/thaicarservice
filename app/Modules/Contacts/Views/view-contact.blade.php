
<div id="view-contact" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 40%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-comments-o" aria-hidden="true"></i> ข้อความจากคุณ{{$contact->first_name}} {{$contact->last_name}}</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px;">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-12">
                        <div class="row"  style="margin-top: 15px;">
                            <div class="col-md-3 col-lg-3">
                                <p><b>ชื่อผู้ส่ง : </b></p>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                คุณ{{$contact->first_name}} {{$contact->last_name}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <p><b>อีเมล : </b></p>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                {{$contact->email}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <p><b>วัน-เวลา ที่ส่ง : </b></p>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                {{\App\Helepers\DateFormat::Date_Clock($contact->created_at)}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <p><b>หัวข้อการติดต่อ : </b></p>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                {{$contact_thai[$contact->contact]}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <p><b>ข้อความ : </b></p>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                <textarea  class="text-area" name="message" cols="20" rows="3" disabled="disabled">{{$contact->message}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#view-contact').on('hidden.bs.modal', function () {
        window.location.reload(true);
    })
</script>