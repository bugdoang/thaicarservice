@extends('masterlayout')
@section('title','ติดต่อเรา')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มกลาง-->

        <div class="register-container col-lg-12 col-md-12"  >
            <div class="panel-title">
                <h2 class="header-text myfont color2"><i class="fa fa-comments-o" aria-hidden="true"></i> ติดต่อเรา</h2>
            </div>
            <form action="/send_contact" method="post" id="fileForm" role="form">
                {{ csrf_field() }}

                <div class="item-border">
                    <div class="col-lg-2 col-md-2"></div>
                    <div class="col-lg-8 col-md-8">
                        <div class="header-detail myfont line1">
                            <label style="text-align: center; display: block; color: black; font-size: 30px "> ฟอร์มการติดต่อ </label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label text-right">ชื่อ <span style="color:red;"> * </span> :</label>
                            <div class="col-md-4" style="padding-bottom: 10px;">
                                <input  class="form-control input-md" type="text" id="first_name" name="first_name">
                            </div>
                            <label class="col-md-5 control-label text-right">นามสกุล <span style="color:red;"> * </span> :</label>
                            <div class="col-md-4" style="padding-bottom: 10px;">
                                <input  class="form-control input-md" type="text" id="last_name" name="last_name">
                            </div>
                            <label class="col-md-5 control-label text-right">อีเมล <span style="color:red;"> * </span> :</label>
                            <div class="col-md-4" style="padding-bottom: 10px;">
                                <input  class="form-control input-md" type="text" id="email" name="email">
                            </div>
                            <label class="col-md-5 control-label text-right">ประเภทการติดต่อ <span style="color:red;"> * </span> :</label>
                            <div class="col-md-4" style="padding-bottom: 10px;">
                                <select class="form-control" name="contact" id="contact">
                                    <option value="">กรุณาเลือก</option>
                                    @if(isset($contact))
                                        @foreach($contact as $item)
                                            <option value="{{$item}}">
                                                {{$contact_thai[$item]}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <label class="col-md-5 control-label text-right">ข้อความ <span style="color:red;"> * </span> :</label>
                            <div class="col-md-3" style="padding-bottom: 10px;">
                                <textarea id="message" class="text-area" name="message" cols="20" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="button-garage myfont"  style="padding-left: 50px; padding-right: 50px;">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    ส่งข้อความ
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2"></div>
                </div>
            </form><!-- ends register form -->
        </div>
    </div>
    @stop