@extends('masterlayout')
@section('title','ข้อความจากผู้ใช้ระบบ')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มกลาง-->
        <div class="register-container1 col-lg-3 col-md-3">
            <div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
                <h2 class="header-text myfont color2">สถานะข้อความ </h2>
                <div class="item-border">
                    <div class="row">
                        <div class="col-lg-5 col-md-5"  style="padding-right: 0px;">
                            ข้อความทั้งหมด :
                        </div>
                        <div class="col-lg-1 col-md-1"  style="padding-left: 0px; padding-right: 0px;">
                            <span id="total_status"></span>
                        </div>
                        <div class="col-lg-5 col-md-5">
                            รายการ
                        </div>
                    </div>
                    <?php
                        $total_status = 0;
                    ?>
                        @foreach($count as $item_count)
                        <div class="row">
                            <div class="col-lg-5 col-md-5"  style="padding-right: 0px;">
                                {{$read_thai[$item_count->status]}} :
                            </div>
                            <div class="col-lg-1 col-md-1"  style="padding-left: 0px; padding-right: 0px;">
                                <?php
                                    $total_status +=$item_count->total;
                                ?>
                                    {{$item_count->total}}
                            </div>
                            <div class="col-lg-5 col-md-5">
                                รายการ
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
            {!! App\Modules\Contacts\Controllers\ContactAdmin::search() !!}
        </div>

        <div class="register-container col-lg-9 col-md-9"  >
            <div class="panel-title">
                <h2 class="header-text myfont color2"><i class="fa fa-comments-o" aria-hidden="true"></i> ข้อความจากผู้ใช้ระบบ</h2>
            </div>
            <form action="" method="post" id="fileForm" role="form">
                <div class="item-border" style="padding-top: 0px;">
                    <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px; padding-left: 0px;">{{$contact_me->total()}} ข้อความ</div>
                    @if(count($contact_me)>0)
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:60px;" class="text-center"> ลำดับที่</th>
                            <th class="text-center" style="width: 208px;"> ชื่อ-นามสกุล ผู้ส่ง</th>
                            <th class="text-center" style="width: 120px;"> วันที่ส่ง</th>
                            <th class="text-center" style="width: 165px;"> หัวข้อ</th>
                            <th class="text-center" style="width: 113px;"> สถานะ</th>
                            <th class="text-center"  style="width: 70px;" ></th>
                        </tr>
                        @foreach($contact_me as $index=>$item)
                            <tr>
                                <td class="text-center">
                                    {{($contact_me->firstItem()+$index)}}
                                </td>
                                <td>
                                    {{$item->first_name}} {{$item->last_name}}
                                </td>
                                <td class="text-center">{{\App\Helepers\DateFormat::Date_Clock($item->created_at)}}</td>
                                <td class="text-center">
                                    {{$contact_thai[$item->contact]}}
                                </td>
                                <td class="text-center">
                                    @if($read_thai[$item->status]=='อ่านแล้ว')
                                        <p style="color: green">
                                            <i class="fa fa-envelope-open-o" aria-hidden="true"></i> {{$read_thai[$item->status]}}
                                        </p>
                                        @elseif($read_thai[$item->status]=='ยังไม่ได้อ่าน')
                                        <p style="color: red">
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i> {{$read_thai[$item->status]}}
                                        </p>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="/contact/view/{{$item->contact_id}}" title="รายละเอียดการจอง" class="edit-content">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                        <div class="text-center">
                            {!!$contact_me->render()!!}
                        </div>
                    @else
                        <p style="text-align: center; margin-top: 10px;">ไม่พบข้อมูล</p>
                    @endif
                </div>
            </form><!-- ends register form -->
        </div>
    </div>
    @stop
    @push('scripts')
        <script>
            $('#total_status').text('{{$total_status}}');
        </script>
    @endpush
