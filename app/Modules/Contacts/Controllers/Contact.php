<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Contacts\Controllers;


use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class Contact extends Controller
{
    public function index()
    {
        $contact = config('myconfig.contact');
        $contact_thai= config('myconfig.contact_thai');

        return view ('contact::contact',[
            'contact'=>$contact,
            'contact_thai' =>$contact_thai
        ]);
    }
    public function send(Request $request)
    {
        $first_name     =   $request->get('first_name');
        $last_name      =   $request->get('last_name');
        $email          =   $request->get('email');
        $contact        =   $request->get('contact');
        $message        =   $request->get('message');
        $check=array('first_name','last_name','email','contact','message');
        foreach ($check as $item)
        {
            if ($request->get ($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ'],422);
            }
        }
        $data = array(
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'email'         => $email,
            'contact'       => $contact,
            'message'       => $message,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $contact_id=DB::table('contacts')->insertGetId($data);
        return response(['ส่งข้อความเรียบร้อยแล้วค่ะ']);
    }
}