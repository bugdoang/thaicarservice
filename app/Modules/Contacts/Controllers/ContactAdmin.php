<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Contacts\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Contacts\Models\ContactModel;
use Illuminate\Support\Facades\Input;

class ContactAdmin extends Controller
{
    public function index()
    {
        $first_name     = Input::get('first_name');
        $last_name      = Input::get('last_name');
        $created_at     = Input::get('created_at');
        $contact        = Input::get('contact');
        $status         = Input::get('status');
        $contact_me = ContactModel::contact_me([
            'first_name' =>$first_name,
            'last_name'  =>$last_name,
            'created_at' =>$created_at,
            'contact'    =>$contact,
            'status'    =>$status,
        ]);
        $contact_thai=config('myconfig.contact_thai');
        $read_thai=config('myconfig.read_thai');
        $count =ContactModel::contact_count();

//        dd($count);

        return view ('contact::contact-admin',[
            'contact_me'=>$contact_me,
            'contact_thai'=>$contact_thai,
            'read_thai'=>$read_thai,
            'count'=>$count
        ]);
    }
    public function view($contact_id)
    {
        $contact = ContactModel::contact($contact_id);
        if(!empty($contact))
        {
            $contact_thai=config('myconfig.contact_thai');
            $html=view('contact::view-contact',compact('contact','contact_thai'));
            $read= ContactModel::update_contact($contact_id,[
                'status'=>'read'
            ]);
            return ['title'=>'ข้อความจากผู้ใช้ระบบ','body'=>$html->render()];
        }
        else
        {
            return response(['ไม่พบข้อมูลที่ท่านต้องการค่ะ'],422);
        }
    }
    public static function search()
    {
        $first_name     = Input::get('first_name');
        $last_name      = Input::get('last_name');
        $created_at     = Input::get('created_at');
        $contact        = Input::get('contact');
        $status         = Input::get('status');
        $contact_data   = config('myconfig.contact');
        $contact_thai   = config('myconfig.contact_thai');
        $status_data    = config('myconfig.status');
        $read_thai      = config('myconfig.read_thai');
//        dd($garage_data);
        return view ('contact::search',[
            'first_name'        => $first_name,
            'last_name'         => $last_name,
            'created_at'        => $created_at,
            'contact'           => $contact,
            'status'            => $status,
            'contact_data'      => $contact_data,
            'contact_thai'      => $contact_thai,
            'status_data'       => $status_data,
            'read_thai'         => $read_thai
        ]);
    }
}