<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Image\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ImageModel extends Model
{
    /**
     * @return mixed
     */
    public static function getAllImage($garage_id)
{
    return DB::table('images')
        ->whereNull('deleted_at')
        ->where('garage_id',$garage_id)
        ->get();
}
}