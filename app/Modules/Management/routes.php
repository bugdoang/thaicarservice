<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/8/2559
 * Time: 11:46
 */
Route::group(['middleware'=>['auth']],function(){

    Route::get('/management','\App\Modules\Management\Controllers\Management@index');


    ///////////////////////////  ข้อมูลสมาชิก ///////////////////////////////////
    Route::get('/viewmember/{member_id}','\App\Modules\Management\Controllers\MnmMember@index');
    Route::get('/editmember/{member_id}','\App\Modules\Management\Controllers\MnmMember@update');
    Route::put('/updatemember/{member_id}','\App\Modules\Management\Controllers\MnmMember@update_data');
    Route::delete('deletemember/{member_id}', '\App\Modules\Management\Controllers\MnmMember@delete');
    Route::get('/export-member/{data_member}','\App\Modules\Management\Services\Export@export');


    ///////////////////////////  ข้อมูลอู่ซ่อมรถ///////////////////////////////////
    Route::get('/viewgarage/{garage_id}','\App\Modules\Management\Controllers\MnmGarage@index');
    Route::delete('/deletegarage/{garage_id}', '\App\Modules\Management\Controllers\MnmGarage@delete');
    Route::get('/export-garage/{data_garage}','\App\Modules\Management\Services\Export@export');

    ///////////////////////////  ข้อมูลประเภทงานบริการ ///////////////////////////////////
    Route::get('/editservice/{service_id}','\App\Modules\Management\Controllers\MnmService@update');
    Route::put('/updateservice/{service_id}','\App\Modules\Management\Controllers\MnmService@update_data');
    Route::get('/addservice','\App\Modules\Management\Controllers\MnmService@index');
    Route::post('/addservice','\App\Modules\Management\Controllers\MnmService@add_service');
    Route::delete('/deleteservice/{service_id}', '\App\Modules\Management\Controllers\MnmService@delete');
    Route::get('/export-service/{data_service}','\App\Modules\Management\Services\Export@export');

    ///////////////////////////  ข้อมูลงานบริการ ///////////////////////////////////
    Route::get('/editsubservice/{sub_services_id}','\App\Modules\Management\Controllers\MnmSubService@update');
    Route::put('/updatesubservice/{sub_services_id}','\App\Modules\Management\Controllers\MnmSubService@update_data');
    Route::get('/addsubservice','\App\Modules\Management\Controllers\MnmSubService@index');
    Route::post('/addsubservice','\App\Modules\Management\Controllers\MnmSubService@add_sub_service');
    Route::delete('/deletesubservice/{sub_services_id}', '\App\Modules\Management\Controllers\MnmSubService@delete');
    Route::get('/export-sub-service/{data_sub_service}','\App\Modules\Management\Services\Export@export');

    ///////////////////////////  ข้อมูลประเภทรถยนต์ ///////////////////////////////////
    Route::get('/editcategory/{category_id}','\App\Modules\Management\Controllers\MnmCategory@update');
    Route::put('/updatecategory/{category_id}','\App\Modules\Management\Controllers\MnmCategory@update_data');
    Route::delete('/deletecategory/{category_id}', '\App\Modules\Management\Controllers\MnmCategory@delete');
    Route::get('/addcategory','\App\Modules\Management\Controllers\MnmCategory@index');
    Route::post('/addcategory','\App\Modules\Management\Controllers\MnmCategory@add_category');
    Route::get('/export-category/{data_category}','\App\Modules\Management\Services\Export@export');

    ///////////////////////////  ข้อมูลยี่ห้อรถ ///////////////////////////////////
    Route::get('/editbrand/{brand_id}','\App\Modules\Management\Controllers\MnmBrand@update');
    Route::put('/updatebrand/{brand_id}','\App\Modules\Management\Controllers\MnmBrand@update_data');
    Route::get('/addbrand','\App\Modules\Management\Controllers\MnmBrand@index');
    Route::post('/addbrand','\App\Modules\Management\Controllers\MnmBrand@add_brand');
    Route::delete('/deletebrand/{brand_id}', '\App\Modules\Management\Controllers\MnmBrand@delete');
    Route::get('/export-brand/{data_brand}','\App\Modules\Management\Services\Export@export');

    ///////////////////////////  ข้อมูลบริษํทประกัน ///////////////////////////////////
    Route::get('/editinsurance/{insurance_id}','\App\Modules\Management\Controllers\MnmInsurance@update');
    Route::put('/updateinsurance/{insurance_id}','\App\Modules\Management\Controllers\MnmInsurance@update_data');
    Route::get('/addinsurance','\App\Modules\Management\Controllers\MnmInsurance@index');
    Route::post('/addinsurance','\App\Modules\Management\Controllers\MnmInsurance@add_insurance');
    Route::delete('/deleteinsurance/{insurance_id}', '\App\Modules\Management\Controllers\MnmInsurance@delete');
    Route::get('/export-insurance/{data_insurance}','\App\Modules\Management\Services\Export@export');

    ///////////////////////////     ข้อมูลการจองอู่ซ่อมรถยนต์  //////////////////////////////////
    Route::get('/view-reservation/{reservation_id}','\App\Modules\Management\Controllers\MnmReservation@index');
    Route::get('/export-reservation/{data_reservation}','\App\Modules\Management\Services\Export@export');

});







