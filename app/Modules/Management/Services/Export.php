<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31/10/2559
 * Time: 17:17
 */

namespace App\Modules\Management\Services;


use App\Helepers\Download;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use App\Services\MyExcel;

class Export extends Controller
{
    public function export($data_type)
    {
        if($data_type=='data_member')
        {
            $data_member=ManagementModel::data_member('',[],true);
            $data=[];
                if (!empty($data_member))
                {
                    foreach ($data_member as $index=>$item)
                    {
                        $data[]=['ลำดับ'=>$index+1,
                            'ชื่อ-นามสกุล'=>$item->title_name.$item->first_name.' '.$item->last_name,
                            'อีเมล์'=>$item->email,
                            'ประเภทสมาชิก'=>$item->type_of_member
                        ];
                    }
                }
            $export= MyExcel::export('member',$data);
            Download::get('ข้อมูลสมาชิก',$export['path']);
        }
        elseif ($data_type=='data_garage')
        {
            $data_garage=ManagementModel::data_garage('',[],true);
            $data=[];
                if (!empty($data_garage))
                {
                    foreach ($data_garage as $index=>$item)
                    {

                        $data[]=['ลำดับ'=>$index+1,
                            'ชื่ออู่ซ่อมรถยนต์'=>$item->garage_name,
                            'ที่ตัั้ง'=>$item->address,
                            'จังหวัด'=>$item->garage_province
                        ];
                    }
                }
            $export= MyExcel::export('garage',$data);
            Download::get('ข้อมูลอู่ซ่อมรถยนต์',$export['path']);
        }
        elseif ($data_type=='data_service')
        {
            $data_service=ManagementModel::data_service('',[],true);
            $data=[];
                if (!empty($data_service))
                {
                    foreach ($data_service as $index=>$item)
                    {
                        $data[]=['ลำดับ'=>$index+1,
                                 'ชื่อประเภทงานบริการ'=>$item->service_name
                                ];
                    }
                }
            $export= MyExcel::export('service',$data);
            Download::get('ข้อมูลประเภทงานบริการ',$export['path']);
        }
        elseif ($data_type=='data_sub_service')
        {
            $data_sub_service=ManagementModel::data_sub_service('',[],true);
            $data=[];
            if (!empty($data_sub_service))
            {
                foreach ($data_sub_service as $index=>$item)
                {
                    $data[]=['ลำดับ'=>$index+1,
                        'ชื่องานบริการ'=>$item->sub_service_name,
                        'ประเภทงานบริการ'=>$item->name_service
                    ];
                }
            }
            $export= MyExcel::export('subservice',$data);
            Download::get('ข้อมูลงานบริการ',$export['path']);
        }
        elseif ($data_type=='data_category')
        {
            $data_category=ManagementModel::data_category('',[],true);
            $data=[];
                if (!empty($data_category))
                {
                    foreach ($data_category as $index=>$item)
                    {
                        $data[]=['ลำดับ'=>$index+1,
                            'ชื่อประเภทรถยนต์'=>$item->category_name
                        ];
                    }
                }
            $export= MyExcel::export('category',$data);
            Download::get('ข้อมูลประเภทรถยนต์',$export['path']);
        }
        elseif ($data_type=='data_brand')
        {
            $data_brand=ManagementModel::data_brand('',[],true);
            $data=[];
                if (!empty($data_brand))
                {
                    foreach ($data_brand as $index=>$item)
                    {
                        $data[]=['ลำดับ'=>$index+1,
                            'ชื่อยี่ห้อรถยนต์'=>$item->brand_name
                        ];
                    }
                }
            $export= MyExcel::export('brand',$data);
            Download::get('ข้อมูลยี่ห้อรถยนต์',$export['path']);
        }
        elseif ($data_type=='data_insurance')
        {
            $data_insurance=ManagementModel::data_insurance('',[],true);
            $data=[];
                if (!empty($data_insurance))
                {
                    foreach ($data_insurance as $index=>$item)
                    {
                        $data[]=['ลำดับ'=>$index+1,
                            'ชื่อบริษัทประภัย'=>$item->insurance_name
                        ];
                    }
                }
            $export= MyExcel::export('insurance',$data);
            Download::get('ข้อมูลบริษัทประกัน',$export['path']);
        }
        elseif ($data_type=='data_reservation')
        {
            $data_reservation=ManagementModel::data_reservation('',[],true);
            $data=[];
            $status_thai=['wait'=>'รอการอนุมัติ','approve'=>'อนุมัติการจอง','reject'=>'ปฏิเสธการจอง','manage'=>'ดำเนินการซ่อม','cancel'=>'ยกเลิกการซ่อม','finish'=>'เสร็จสิ้น'];

            if (!empty($data_reservation))
            {
                foreach ($data_reservation as $index=>$item)
                {
                    $data[]=['ลำดับ'=>$index+1,
                        'ชื่ออู่ซ่อมรถยนต์'=>$item->garage_name,
                        'ชื่อผู้จอง'=>$item->title_name.$item->first_name.' '.$item->last_name,
                        'สถานะ'=>$status_thai[$item->status]
                    ];
                }
            }
            $export= MyExcel::export('insurance',$data);
            Download::get('ข้อมูลการจองอู่',$export['path']);
        }
    }
}