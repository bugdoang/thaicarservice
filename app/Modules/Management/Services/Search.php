<?php
namespace App\Modules\Management\Services;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/10/2559
 * Time: 12:10
 */
use App\Modules\Booking\Models\BookingModel;
use Illuminate\Support\Facades\Input;
use App\Modules\Product\Models\ProductModel;

class Search
{
    public static function search_brand()
    {
        $brand_name     = Input::get('brand_name');
        return view ('management::search.search-brand',[
            '_brand_name'        => $brand_name,
        ]);
    }
    public static function search_category()
    {
        $category_name     = Input::get('category_name');
        return view ('management::search.search-category',[
            '_category_name'        => $category_name,
        ]);
    }
    public static function search_garage()
    {
        $garage_name    = Input::get('garage_name');
        $address        = Input::get('address');
        $province_id    = Input::get('province_id');
        $province   = ProductModel::province();
        return view ('management::search.search-garage',[
            '_garage_name'          => $garage_name,
            '_address'              => $address,
            '_province_id'          => $province_id,
            '_province'             => $province,
        ]);
    }
    public static function search_insurance()
    {
        $insurance_name     = Input::get('insurance_name');
        return view ('management::search.search-insurance',[
            '_insurance_name'        => $insurance_name,
        ]);
    }
    public static function search_member()
    {

        $title_name     = Input::get('title_name');
        $first_name     = Input::get('first_name');
        $last_name      = Input::get('last_name');
        $email          = Input::get('email');
        $type_of_member = Input::get('type_of_member');
        $data_type_of_member =config('myconfig.type_of_member');

        return view ('management::search.search-member',[
            '_title_name'        => $title_name,
            '_first_name'        => $first_name,
            '_last_name'         => $last_name,
            '_email'             => $email,
            '_type_of_member'    => $type_of_member,
            '_data_type_of_member'=> $data_type_of_member,
        ]);
    }
    public static function search_service()
    {
        $service_name     = Input::get('service_name');
        return view ('management::search.search-service',[
            '_service_name'        => $service_name,
        ]);
    }
    public static function search_reservation()
    {
        $garage_name    = Input::get('garage_name');
        $title_name     = Input::get('title_name');
        $first_name     = Input::get('first_name');
        $last_name      = Input::get('last_name');
        $status         = Input::get('status');
        $status_data    =   BookingModel::status();

        return view ('management::search.search-reservation',[
            '_garage_name'       => $garage_name,
            '_title_name'        => $title_name,
            '_first_name'        => $first_name,
            '_last_name'         => $last_name,
            '_status_data'       => $status_data,
            '_status'            =>$status,
        ]);
    }
}