<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/11/2559
 * Time: 10:43
 */

namespace App\Modules\Management\Services;


use App\Modules\Booking\Models\BookingModel;
use App\Modules\Management\Models\ManagementModel;

class ManagementList
{
    public static function get($type,$params=[])
    {
        switch ($type)
        {
            case 'member': $items=ManagementModel::data_member('',$params);
            break;
            case 'garage': $items=ManagementModel::data_garage('',$params);
            break;
            case 'service': $items=ManagementModel::data_service('',$params);
            break;
            case 'subservice': $items=ManagementModel::data_sub_service('',$params);
            break;
            case 'category': $items=ManagementModel::data_category('',$params);
            break;
            case 'insurance': $items=ManagementModel::data_insurance('',$params);
            break;
            case 'brand': $items=ManagementModel::data_brand('',$params);
            break;
            case 'reservation': $items=ManagementModel::data_reservation('',$params);
            break;
        }
        $request=$params->all();//เพราะ พาราม ที่ส่งมาให้เป็นรีเควส เลยต้องใช้คำสั่งนี้ แปลง
        $request['type']=$type;
        $items->appends($request);//เวลากดหน้า ต่อไป ข้อมูลจะตามไปด้วย appends
//        dd($reservation_status);
        return view('management::'.$type.'.mnm',[
            'data_items'=>$items,
        ]);
    }
}