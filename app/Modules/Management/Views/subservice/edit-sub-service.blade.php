<form id="fm-edit-sub-service" method="PUT" action="/updatesubservice/{{$sub_service->sub_services_id}}">
    {{ csrf_field() }}
<div id="edit-member" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขข้อมูลงานบริการ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>ชื่อข้อมูลงานบริการ : <span style="color: red"> * </span></label>
                    </div>
                    <div class="form-group col-md-7 col-lg-7" style="margin-left: -40px;">
                        <input class="form-control" type="text" name="sub_service_name" id="sub_service_name" value="{{$sub_service->sub_service_name}}"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>ประเภทงานบริการ : <span style="color: red"> * </span></label>
                    </div>
                    <div class="form-group col-md-7 col-lg-7" style="margin-left: -40px;">
                        <select class="form-control" name="service_id" id="service_id">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($service_all))
                                @foreach($service_all as $item)
                                    <option {{($sub_service->service_id==$item->service_id)?' selected ':''}} value="{{$item->service_id}}">
                                        {{$item->service_name}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>

    </div>
</div>
</form>