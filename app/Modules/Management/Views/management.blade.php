@extends('masterlayout')
@section('title','การจัดการข้อมูล')
@section('content')
<!-- Page Content -->

<div class="row">
    <div class="register-container1 col-lg-3 col-md-3">
        <div class="panel-title">
            <h2 class="header-text myfont color2"> รายการข้อมูล</h2>
        </div>
        <div class="item-border " style="padding-top: 0px; padding-left: 0px; padding-bottom: 0px;">
            <div id="rootwizard" class="tabbable tabs-left">
                    <ul class="manage-menu button-mnm nav-tabs" style="border-bottom: 0px;">
                      <li class="{{request('type')=='member' || request('type')==''?'active':''}}">
                          <a href="/management?type=member" style="margin-right: 0px; border-width: 0px;">
                              <i class="fa fa-user" aria-hidden="true"></i> ข้อมูลผู้ใช้ระบบ
                          </a>
                      </li >

                        <li class="{{request('type')=='garage'?'active':''}}">
                        <a href="/management?type=garage" style="margin-right: 0px; border-width: 0px;">
                                  <i class="fa fa-car" aria-hidden="true"></i> ข้อมูลอู่ซ่อมรถยนต์
                          </a>
                      </li>
                        <li class="{{request('type')=='reservation'?'active':''}}">
                              <a href="/management?type=reservation" style="margin-right: 0px; border-width: 0px;">
                                  <i class="fa fa-book" aria-hidden="true"></i> ข้อมูลการจอง
                              </a>
                      </li>
                        <li class="{{request('type')=='service'?'active':''}}">
                        <a href="/management?type=service" style="margin-right: 0px; border-width: 0px;">
                              <i class="fa fa-wrench" aria-hidden="true"></i> ข้อมูลประเภทงานบริการ
                       </a>
                      </li>
                        <li class="{{request('type')=='subservice'?'active':''}}">
                            <a href="/management?type=subservice" style="margin-right: 0px; border-width: 0px;">
                                <i class="fa fa-wrench" aria-hidden="true"></i> ข้อมูลงานบริการ
                            </a>
                        </li>
                        <li class="{{request('type')=='category'?'active':''}}">
                        <a href="/management?type=category" style="margin-right: 0px; border-width: 0px;">
                                <i class="fa fa-tachometer" aria-hidden="true"></i> ข้อมูลประเภทรถยนต์
                            </a>
                        </li>
                        <li class="{{request('type')=='brand'?'active':''}}">
                        <a href="/management?type=brand" style="margin-right: 0px; border-width: 0px;">
                                <i class="fa fa-shield" aria-hidden="true"></i> ข้อมูลยี่ห้อรถยนต์
                            </a>
                        </li>
                        <li class="{{request('type')=='insurance'?'active':''}}">
                        <a href="/management?type=insurance" style="margin-right: 0px; border-width: 0px;">
                                    <i class="fa fa-university" aria-hidden="true"></i> ข้อมูลบริษัทประกันภัย
                            </a>
                        </li>
                    </ul>
            </div>
        </div>
        @if($type=='member')
            {!! App\Modules\Management\Services\Search::search_member() !!}
        @elseif($type=='garage')
            {!! App\Modules\Management\Services\Search::search_garage() !!}
        @elseif($type=='service')
            {!! App\Modules\Management\Services\Search::search_service() !!}
        @elseif($type=='category')
            {!! App\Modules\Management\Services\Search::search_category() !!}
        @elseif($type=='brand')
            {!! App\Modules\Management\Services\Search::search_brand() !!}
        @elseif($type=='insurance')
            {!! App\Modules\Management\Services\Search::search_insurance() !!}
        @elseif($type=='reservation')
            {!! App\Modules\Management\Services\Search::search_reservation() !!}
        @endif
    </div>

    <div class="register-container1 col-lg-9 col-md-9">
        {!! $list_view !!}
    </div>
</div>

@stop

