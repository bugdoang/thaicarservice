<form id="fm-add-service" action="/addservice" method="POST">
    {{ csrf_field() }}
<div id="edit-member" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูลงานบริการ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>ชื่อข้อมูลงานบริการ : <span style="color: red"> * </span></label>
                    </div>
                    <div class="form-group col-md-7 col-lg-7" style="margin-left: -40px;">
                        <input class="form-control" type="text" name="service_name" id="service_name" value=""/>
                    </div>

                </div>

            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>

    </div>
</div>
</form>