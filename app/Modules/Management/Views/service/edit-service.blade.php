<form id="fm-edit-service" method="PUT" action="/updateservice/{{$service->service_id}}">
    {{ csrf_field() }}
<div id="edit-member" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขข้อมูลงานบริการ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>ชื่อข้อมูลงานบริการ : </label>
                    </div>
                    <div class="form-group col-md-7 col-lg-7" style="margin-left: -40px;">
                        <input class="form-control" type="text" name="service_name" id="service_name" value="{{$service->service_name}}"/>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>

    </div>
</div>
</form>