<div id="tab4" class="tab-pane">
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <h2 class="header-text myfont"><i class="fa fa-wrench" aria-hidden="true"></i> ข้อมูลประเภทงานบริการ</h2>
            <div class="item-border" style="padding-top: 0px;">
                @if(count($data_items)>0)
                    <div class="row">
                        <div class=" pull-right"  >
                            <div class="col-lg-12 col-md-12">
                                <a href="/export-service/data_service" class="button-delete myfont" title="excel"  style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="col-lg-12 col-md-12">
                                <a href="/addservice" class="button-delete myfont edit-content"  style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    เพิ่มข้อมูลประเภทงานบริการ
                                </a>
                            </div>
                        </div>
                        <div class="pull-left">
                            <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px;">
                                ประเภทงานบริการทั้งหมด {{$data_items->total()}} รายการ
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center"  style="width: 70px;"> ลำดับที่</th>
                            <th class="text-center"> ชื่อประเภทงานบริการ</th>
                            <th class="text-center"  style="width: 90px;"> </th>
                        </tr>
                        @foreach($data_items as $index=>$item)
                            <tr>
                                <td class="text-center">
                                    {{($data_items->firstItem()+$index)}}
                                </td>
                                <td>{{$item->service_name}}</td>
                                <td class="text-center">
                                    <a href="/editservice/{{$item->service_id}}" title="แก้ไขข้อมูลประเภทงานบริการ" class="edit-content">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <a href="/deleteservice/{{$item->service_id}}"  class="delete-content">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="text-center">
                        {!! $data_items->render() !!}
                    </div>
                @else
                    <p style="margin-top: 10px;display: block; text-align: center; ">ไม่พบข้อมูล</p>
                @endif
            </div>
        </div>
    </div>
</div>

