<div id="view-reservation" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 60%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-book" aria-hidden="true"></i> ข้อมูลการจอง</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px;">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-12">
                        <div class="header-detail myfont line1">
                            ข้อมูลลูกค้า
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <p><b>ชื่อลูกค้า : </b><span>{{$reservation->title_name}}{{$reservation->first_name}} {{$reservation->last_name}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>เบอร์โทรศัพท์ : </b><span>{{$reservation->mobile}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>อีเมล : </b><span>{{$reservation->email}}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            @if(!empty($reservation->type_category))
                                <div class="col-md-4 col-lg-4">
                                    <p><b>ประเภทรถยนต์ : </b><span>{{$reservation->type_category}}</span></p>
                                </div>
                            @endif
                            @if(!empty($reservation->type_brand))
                                <div class="col-md-4 col-lg-4">
                                    <p><b>ยี่ห้อรถยนต์ : </b><span>{{$reservation->type_brand}}</span></p>
                                </div>
                            @endif
                            @if(!empty($reservation->type_insurance))
                                <div class="col-md-4 col-lg-4">
                                    <p><b>บริษัทประกันภัย : </b><span>{{$reservation->type_insurance}}</span></p>
                                </div>
                            @endif

                        </div>

                            <div class="header-detail myfont line1">
                                ข้อมูลอู่ซ่อมรถยนต์
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <p><b>ชื่ออู่ : </b><span>{{$reservation->garage_name}}</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <p><b>อีเมล์ : </b><span>{{$reservation->email}}</span></p>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <p><b>เบอร์โทรศัพท์ : </b><span>{{$reservation->mobile}}</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <p><b>ที่ตั้ง : </b><span>{{$reservation->address}}</span></p>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <p><b>จังหวัด : </b><span>{{$reservation->province_name}}</span></p>
                                </div>
                            </div>
                            <div class="row">
                                @if(!empty($reservation->open_garages))
                                    <div class="col-lg-6 col-md-6">
                                        <p><b>วันเปิดทำการ : </b><span>{{$reservation->open_garages}}</span></p>
                                    </div>
                                @endif
                                @if(!empty($reservation->open_time && $reservation->close_time))
                                    <div class="col-lg-6 col-md-6">
                                        <p><b>เวลาเปิด-ปิด : </b><span>{{$reservation->open_time}} - {{$reservation->close_time}} น.</span></p>
                                    </div>
                                @endif
                            </div>
                        <div class="header-detail myfont line1">
                            รายละเอียดการจองอู่
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <p><b>วันที่ทำการจอง : </b><span>{{\App\Helepers\DateFormat::Date_Clock($reservation->created_at)}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>วันที่-เวลาที่นัดเข้าอู่ : </b><span>{{\App\Helepers\DateFormat::thaiDate($reservation->date)}} {{$reservation->time}} น.</span></p>
                            </div>
                        </div>
                        @if(!empty($reservation->reservation_detail))
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <p>
                                        <b>รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม  : </b>
                                    </p>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <p>
                                        <input class="form-control"  disabled="disabled"  value=" {{$reservation->reservation_detail}}"/>
                                    </p>
                                </div>
                            </div>
                        @endif
                        @if(!empty($repair_list))
                            <div class="header-detail myfont line">
                                รายการซ่อมเบื้องต้น
                            </div>
                            <div class="row"   style=" margin-left: 15px; width: 90%;">
                                <table class="table table-bordered" >
                                    <tr>
                                        <th class="text-center" > ลำดับที่</th>
                                        <th class="text-center" > ประเภทรายการซ่อม</th>
                                        <th class="text-center" > รายการซ่อม</th>
                                        <th class="text-center" > รายการที่ยืนยันการซ่อม</th>
                                        <th class="text-center" > วันที่ยืนยันรายการซ่อม</th>
                                    </tr>
                                    @foreach($repair_list as $index=>$item)
                                        <tr>
                                            <td class="text-center">
                                                {{$index+1}}
                                            </td>
                                            <td class="text-center">
                                                {{$item->service_name}}
                                            </td>

                                            <td class="text-center">
                                                @if($item->sub_service_name==null)
                                                    -
                                                @else
                                                    {{$item->sub_service_name}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($item->status_repair_list_member=='Y')
                                                    <i class="fa fa-check" aria-hidden="true" style="color: green"></i>
                                                @elseif($item->status_repair_list_member=='N')
                                                    <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($item->dob_status_member==null)
                                                    -
                                                @else
                                                    {{\App\Helepers\DateFormat::thaiDate($item->dob_status_member)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        @endif
                        <div class="header-detail myfont line1">
                            สถานะการทำงาน
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <p><b>สถานะล่าสุด : </b><span> {{$reservation_status->status_name}} (แก้ไขสถานะล่าสุดวันที่ {{\App\Helepers\DateFormat::Date_Clock($reservation_status->created_at)}})</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-bordered" style="width: 80%; margin-left: 30px;">
                                @if(!empty($data_status))
                                    <tr>
                                        <th class="text-center" > ลำดับที่</th>
                                        <th class="text-center" > ชื่อสถานะ</th>
                                        <th class="text-center" > วันที่เปลี่ยนสถานะ</th>
                                    </tr>
                                    @foreach($data_status as $index=>$item)
                                        @if($item->active == 'Y')
                                            <tr style="background-color:#F8D486">
                                        @else
                                            <tr>
                                                @endif
                                                <td class="text-center">
                                                    {{$index+1}}
                                                </td>
                                                <td class="text-center">
                                                    {{$item->status_name}}
                                                </td>

                                                <td class="text-center">
                                                    {{\App\Helepers\DateFormat::Date_Clock($item->created_at)}}
                                                </td>
                                            </tr>
                                            @endforeach
                            </table>
                            @endif
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>