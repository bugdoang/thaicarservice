    <div class="col-lg-12 col-md-12">
        <div class="row">
            <h2 class="header-text myfont"><i class="fa fa-book" aria-hidden="true"></i> ข้อมูลการจอง</h2>
            <div class="item-border" style="padding-top: 0px;">

            @if(count($data_items)>0)
                    <div class="row">
                        <div class="pull-right">
                            <div class="col-lg-12 col-md-12 ">
                                <a href="/export-reservation/data_reservation" class="button-delete myfont" title="excel"   style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="pull-left">
                            <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px;">
                                รายการจองทั้งหมด {{$data_items->total()}} รายการ
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center"  > ลำดับที่</th>
                            <th class="text-center"> ชื่ออู่ซ่อมรถยนต์</th>
                            <th class="text-center"  > ชื่อผู้จอง</th>
                            <th class="text-center" > สถานะ</th>
                            <th class="text-center"  > </th>


                        </tr>
                        @foreach($data_items as $index=>$item)
                            <tr>
                                <td class="text-center">
                                    {{($data_items->firstItem()+$index)}}
                                </td>
                                <td>
                                    {{$item->garage_name}}
                                </td>
                                <td>
                                    {{$item->title_name}}{{$item->first_name}} {{$item->last_name}}
                                </td>
                                <td>
                                    {{--{{$status_thai[$item->status_id]}}--}}
                                    {{$item->status_name}}
                                </td>
                                <div id="rootwizard" class="tabbable tabs-left">
                                    <td class="text-center">
                                        <a href="/view-reservation/{{$item->reservation_id}}" title="รายละเอียดข้อมูลสมาชิก" class="edit-content">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </div>
                            </tr>
                        @endforeach
                    </table>
                    <div class="text-center">
                        {!! $data_items->render() !!}
                    </div>
            @else
                        <p style="margin-top: 10px;display: block; text-align: center; ">ไม่พบข้อมูล</p>
            @endif

            </div>
        </div>

    </div>
