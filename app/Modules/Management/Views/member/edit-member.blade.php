<form id="fm-edit-member" method="PUT" action="/updatemanagement/{{$member->member_id}}">
    {{ csrf_field() }}
<div id="edit-member" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขประเภทผู้ใช้ระบบ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-3 col-lg-3">
                        <label>ประเภทผู้ใช้ระบบ : </label>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        <select class="form-control" name="type_of_member" id="type_of_member">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($type_of_member))
                                @foreach($type_of_member as $item)
                                    <option {{($member->type_of_member==$item)?' selected ':''}} value="{{$item}}">
                                        {{$item}}
                                    </option>
                                @endforeach
                            @endif

                        </select>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>

    </div>
</div>
</form>