<div id="view-member" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user" aria-hidden="true"></i> ข้อมูลผู้ใช้ระบบ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6 col-lg-6">
                        <label>คำนำหน้า: </label>
                        <input class="form-control" type="text" name="title_name" id="title_name" disabled="disabled" value=" {{$member->title_name}}"/>
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-lg-6">
                        <label>ชื่อ: </label>
                        <input class="form-control" type="text" name="first_name" id="first_name" disabled="disabled" value=" {{$member->first_name}}"/>
                    </div>

                    <div class="form-group col-md-6 col-lg-6">
                        <label>นามสกุล: </label>
                        <input class="form-control" type="text" name="last_name" id="last_name" disabled="disabled" value=" {{$member->last_name}}"/>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6 col-lg-6">
                        <label>วัน/เดือน/ปี เกิด: </label>
                        <input class="form-control datepicker" type="text" name="dob" id="dob" disabled="disabled" value=" {{\App\Helepers\DateFormat::thaiDate($member->dob)}}"/>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        <label>เพศ: </label></br>
                        <input class="form-control" type="text" name="gender" id="gender" disabled="disabled" value=" {{$member->gender}}"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-lg-6">
                        <label>เลขบัตรประชาชน: </label>
                        <input class="form-control" type="text" name="card_id" id="card_id" disabled="disabled" value=" {{$member->card_id}}"/>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        <label>เบอร์โทรศัพท์: </label>
                        <input class="form-control" type="text" name="mobile" id="mobile" disabled="disabled" value=" {{$member->mobile}}"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12 col-lg-12">
                        <div class="form-group">
                            <label>ที่อยู่:</label>
                            <input class="form-control" type="text" name="address" id="address" disabled="disabled" value=" {{$member->address}}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <label>จังหวัด: </label>
                        <input class="form-control" type="text" name="province" id="province" disabled="disabled" value=" {{$member->province_name}}"/>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <label>ประเภทผู้ใช้ระบบ : </label>
                        <input class="form-control" type="text" name="province" id="province" disabled="disabled" value=" {{$member->type_of_member}}"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>