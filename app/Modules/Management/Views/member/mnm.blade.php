    <div class="col-lg-12 col-md-12">
        <div class="row">
            <h2 class="header-text myfont"><i class="fa fa-user" aria-hidden="true"></i> ข้อมูลผู้ใช้ระบบ</h2>
            <div class="item-border" style="padding-top: 0px;">

            @if(count($data_items)>0)
                <div class="row">
                    <div class="pull-right">
                        <div class="col-lg-12 col-md-12 ">
                            <a href="/export-member/data_member" class="button-delete myfont" title="excel"   style="margin-bottom: 0px; padding-top: 6px;">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="pull-left">
                        <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px;">
                            ผู้ใช้ระบบทั้งหมด {{$data_items->total()}} คน
                        </div>
                    </div>
                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center"  style="width: 70px;"> ลำดับที่</th>
                            <th class="text-center"> ชื่อ-นามสกุล</th>
                            <th class="text-center"  style="width: 230px;"> อีเมล์</th>
                            <th class="text-center" style="width: 120px;"> ประเภทผู้ใช้ระบบ</th>
                            <th class="text-center"  style="width: 90px;"> </th>


                        </tr>
                        @foreach($data_items as $index=>$item)
                            <tr>
                                <td class="text-center">
                                    {{($data_items->firstItem()+$index)}}
                                </td>
                                <td>
                                    {{$item->title_name}} {{$item->first_name}} {{$item->last_name}}
                                </td>
                                <td>
                                    {{$item->email}}
                                </td>
                                <td class="text-center">{{$item->type_of_member}}</td>
                                <div id="rootwizard" class="tabbable tabs-left">
                                    <td class="text-center">
                                        <a href="/viewmember/{{$item->member_id}}" title="รายละเอียดข้อมูลสมาชิก" class="edit-content">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </a>
                                        <a href="/editmember/{{$item->member_id}}" title="แก้ไขข้อมมูลสมาชิก" class="edit-content">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        <a href="deletemember/{{$item->member_id}}"  class="delete-content">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </div>
                            </tr>
                        @endforeach
                    </table>
                    <div class="text-center">
                        {!! $data_items->render() !!}
                    </div>
            @else
                        <p style="margin-top: 10px;display: block; text-align: center; ">ไม่พบข้อมูล</p>
            @endif

            </div>
        </div>

    </div>
