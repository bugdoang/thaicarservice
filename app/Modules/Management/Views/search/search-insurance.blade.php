<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2"><i class="fa fa-university" aria-hidden="true"></i> ค้นหาข้อมูลบริษัทประกันภัย </h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/management">
            <input type="hidden" name="type" value="insurance">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่อบริษัทประกันภัย" type="text" name="insurance_name" class="form-control" value="{{$_insurance_name}}">
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>