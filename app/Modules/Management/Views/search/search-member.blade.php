<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2"><i class="fa fa-search" aria-hidden="true"></i> ค้นหาข้อมูลผู้ใช้ระบบ</h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/management">
            <input type="hidden" name="type" value="member">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่อ" type="text" name="first_name" class="form-control" value="{{$_first_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  นามสกุล" type="text" name="last_name" class="form-control" value="{{$_last_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  อีเมล์" type="text" name="email" class="form-control" value="{{$_email}}">
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกประเภทสมาชิก" name="type_of_member">
                        <option value="" ></option>
                        @if(!empty($_data_type_of_member))
                            @foreach($_data_type_of_member as $item)
                                <option {{($_type_of_member==$item)?'selected':''}} value="{{$item}}">
                                    {{$item}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>