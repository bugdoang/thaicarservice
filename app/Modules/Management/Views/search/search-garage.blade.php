<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2"><i class="fa fa-car" aria-hidden="true"></i> ค้นหาข้อมูลอู่ซ่อมรถยนต์ </h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/management">
            <input type="hidden" name="type" value="garage">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่ออู่ซ่อมรถยนต์" type="text" name="garage_name" class="form-control" value="{{$_garage_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ที่ตั้งอู่ซ่อมรถยนต์" type="text" name="address" class="form-control" value="{{$_address}}">
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกจังหวัด" name="province_id">
                        <option value="" ></option>
                        @if(!empty($_province))
                            @foreach($_province as $item)
                                <option {{($_province_id==$item->province_id)?'selected':''}} value="{{$item->province_id}}">
                                    {{$item->province_name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>