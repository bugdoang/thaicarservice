<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2"><i class="fa fa-book" aria-hidden="true"></i> ค้นหาข้อมูลการจอง</h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/management">
            <input type="hidden" name="type" value="reservation">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่ออู่ซ่อมรถยนต์" type="text" name="garage_name" class="form-control" value="{{$_garage_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่อผู้จอง" type="text" name="first_name" class="form-control" value="{{$_first_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  นามสกุลผู้จอง" type="text" name="last_name" class="form-control" value="{{$_last_name}}">
                </div>
            </div>


            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกสถานะ" name="status">
                        <option value="" ></option>
                        @if($_status_data)
                            @foreach($_status_data as $item)
                                <option {{($_status==$item->status_id)?' selected ':''}} value="{{$item->status_id}}">
                                    {{$item->status_name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>