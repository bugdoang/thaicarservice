<div class="register-container1"  style="padding-left: 0px; padding-right: 0px;">
    <h2 class="header-text myfont color2"><i class="fa fa-wrench" aria-hidden="true"></i> ค้นหาข้อมูลงานบริการ </h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/management">
            <input type="hidden" name="type" value="service">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่องานบริการ" type="text" name="service_name" class="form-control" value="{{$_service_name}}">
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>