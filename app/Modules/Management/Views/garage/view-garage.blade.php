<div id="view-garage" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 70%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-car" aria-hidden="true"></i> ข้อมูลอู่ซ่อมรถยนต์</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px;">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-12">
                        <div class="header-detail myfont line1">
                           ข้อมูลพื้นฐาน
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <p><b>ชื่ออู่ : </b><span>{{$garage['garage_name']}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>เจ้าของอู่ : </b><span>{{$garage['fullname']}}</span></p>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <p><b>เบอร์โทรศัพท์ : </b><span>{{$garage['mobile_garage']}}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <p><b>อีเมล์ : </b><span>{{$garage['email_au']}}</span></p>
                            </div>
                            @if(!empty($garage['website']))
                            <div class="col-md-4 col-lg-4">
                                <p><b>website : </b><span>{{$garage['website']}}</span></p>
                            </div>
                            @endif
                            @if(!empty($garage['open_time'] && $garage['close_time']))
                                <div class="col-md-4 col-lg-4">
                                    <p><b>เวลาเปิด-ปิด : </b><span>{{$garage['open_time']}} - {{$garage['close_time']}} น.</span></p>
                                </div>
                            @endif

                        </div>
                        <div class="row">
                            @if(!empty($garage['open_garages']))
                                <div class="col-md-12 col-lg-12">
                                    <p><b>วันเปิดทำการ : </b><span>{{$garage['open_garages']}}</span></p>
                                </div>
                            @endif
                            <div class="col-md-12 col-lg-12">
                                <p><b>ที่ตั้งอู่ : </b><span>{{$garage['address']}}</span><b style="margin-left: 35px;">จังหวัด : </b><span>{{$garage['garage_province']}}</span></p>
                            </div>
                        </div>

                        @if(!empty($services))
                            <div class="header-detail myfont line1">
                                งานที่ให้บริการ
                            </div>
                            <div class="row">
                                @foreach($services as $item)
                                    <div class="col-md-3 col-lg-3">
                                        <p style="margin-left: 15px;">
                                            - {{$item}}
                                        </p>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        @if(!empty($categories))
                            <div class="header-detail myfont line1">
                                ประเภทรถที่รับซ่อม
                            </div>
                            <div class="row">
                                @foreach($categories as $item)
                                    <div class="col-md-3 col-lg-3">
                                        <p style="margin-left: 15px;">
                                            - {{$item}}
                                        </p>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        @if(!empty($brands))
                            <div class="header-detail myfont line1">
                                ยี่ห้อรถที่รับซ่อม
                            </div>
                            <div class="row">
                                @foreach($brands as $item)
                                    <div class="col-md-3 col-lg-3">
                                        <p style="margin-left: 15px;">
                                            - {{$item}}
                                        </p>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        @if(!empty($insurances))
                                <div class="header-detail myfont line1">
                                    บริษัทประกัน
                                </div>
                                <div class="row">
                                    @foreach($insurances as $item)
                                        <div class="col-md-3 col-lg-3">
                                            <p style="margin-left: 15px;">
                                                - {{$item}}
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>