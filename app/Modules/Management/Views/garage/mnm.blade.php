<div id="tab2" class="tab-pane">
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <h2 class="header-text myfont"><i class="fa fa-car" aria-hidden="true"></i> ข้อมูลอู่ซ่อมรถยนต์</h2>
            <div class="item-border" style="padding-top: 0px;">
                @if(count($data_items)>0)
                    <div class="row">
                        <div class="pull-right">
                            <div class="col-lg-12 col-md-12 ">
                                <a href="/export-garage/data_garage" class="button-delete myfont" title="excel"   style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="pull-left">
                            <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px;">
                                รายการอู่ทั้งหมด {{$data_items->total()}} อู่
                            </div>
                        </div>
                    </div>
                <table class="table table-bordered">
                    <tr>
                        <th style="width:60px;" class="text-center"> ลำดับที่</th>
                        <th class="text-center"  style="width: 200px;"> ชื่ออู่</th>
                        <th class="text-center"> ที่ตั้งอู่</th>
                        <th class="text-center"  style="width: 160px;"> จังหวัด</th>
                        <th class="text-center"  style="width: 90px;"> </th>


                    </tr>
                    @foreach($data_items as $index=>$item)
                    <tr>
                            <td class="text-center">
                                {{($data_items->firstItem()+$index)}}
                            </td>
                            <td>
                                {{$item->garage_name}}
                            </td>
                            <td>{{$item->address}}</td>
                        <td class="text-center">{{$item->garage_province}}</td>
                        <td class="text-center">
                            <a href="/viewgarage/{{$item->garage_id}}" title="รายละเอียดข้อมูลอู่ซ่อมรถยนต์" class="edit-content">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                            <a href="/deletegarage/{{$item->garage_id}}"  class="delete-content">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                    <div class="text-center">
                        {!! $data_items->render() !!}
                    </div>
                @else
                    <p style="margin-top: 10px;display: block; text-align: center; ">ไม่พบข้อมูล</p>
                @endif

            </div>
        </div>
    </div>
</div>
