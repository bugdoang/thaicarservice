
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <h2 class="header-text myfont"><i class="fa fa-university" aria-hidden="true"></i> ข้อมูลบริษัทประกันภัย</h2>
            <div class="item-border" style="padding-top: 0px;">
                @if(count($data_items)>0)
                    <div class="row">
                        <div class=" pull-right"  >
                            <div class="col-lg-12 col-md-12">
                                <a href="/export-insurance/data_insurance" class="button-delete myfont" title="excel"  style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="col-lg-12 col-md-12">
                                <a href="/addinsurance" class="button-delete myfont edit-content"  style="margin-bottom: 0px; padding-top: 6px;">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    เพิ่มข้อมูลบริษัทประกันภัย
                                </a>
                            </div>
                        </div>
                        <div class="pull-left">
                            <div class="col-lg-12 col-md-12 myfont" style="font-size: 22px;">
                                บริษัทประกันภัยทั้งหมด {{$data_items->total()}} บริษัท
                            </div>
                        </div>
                    </div>


                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center"  style="width: 70px;"> ลำดับที่</th>
                            <th class="text-center"> ชื่อบริษัทประกันภัย</th>
                            <th class="text-center"  style="width: 125px;"> รูปภาพ</th>
                            <th class="text-center"  style="width: 90px;"> </th>
                        </tr>
                        @foreach($data_items as $index=>$item)
                        <tr>
                            <td class="text-center">
                                {{($data_items->firstItem()+$index)}}
                            </td>
                            <td>{{$item->insurance_name}}</td>
                            <td class="text-center"  >
                                <img id="image_insurance" src="{{$item->image_insurance}}" class="img-logo">
                            </td>
                            <td class="text-center">
                                <a href="/editinsurance/{{$item->insurance_id}}" title="แก้ไขข้อมูลงานบริการ" class="edit-content">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a href="/deleteinsurance/{{$item->insurance_id}}"  class="delete-content">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                </table>
                <div class="text-center">
                    {!! $data_items->render() !!}
                </div>
                @else
                    <p style="margin-top: 10px;display: block; text-align: center; ">ไม่พบข้อมูล</p>
                @endif
            </div>
        </div>
    </div>


