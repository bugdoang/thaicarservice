<form id="fm-add-insurance" action="/addinsurance" method="POST">
    {{ csrf_field() }}
<div id="edit-insurance" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-university" aria-hidden="true"></i> เพิ่มข้อมูลบริษัทประกันภัย</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label style="margin-left: -6px;">ชื่อข้อมูลบริษัทประกันภัย : <span style="color: red"> * </span> </label>
                    </div>
                    <div class="form-group col-md-7 col-lg-7" style="margin-left: -40px;">
                        <input class="form-control" type="text" name="insurance_name" id="insurance_name" value=""/>
                    </div>
                    <div class="row" align="center">
                        <img id="image_insurance"  class="img-profile">
                        <div class="row">
                            <div class="form-group col-md-4 col-lg-4" style="padding-left: 16px;">
                                <label>โลโก้บริษัทประกันภัย : <span style="color: red"> * </span> </label>
                            </div>
                            <div class="col-lg-4 col-md-4"  style="padding-left: 0px; padding-right: 0px; margin-top: -22px; margin-left: -66px;">
                                <button  data-profile="insurances" class="button-picture myfont uploadFile">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                    เลือกรูปภาพ
                                </button>
                            </div>
                            <div class="col-lg-12 col-md-12" style="margin-left: 300px; margin-top: -50px;">
                                <div id="image-lists"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>

    </div>
</div>
</form>