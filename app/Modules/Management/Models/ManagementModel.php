<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class ManagementModel extends Model
{
    /**
     * @return mixed
     */
    //////////////////////////////// ข้อมูลสมาชิก ///////////////////////////////////
    public static function  data_member($id='',$param=[],$is_all=false)
    {
        $first_name=(isset($param['first_name']))?$param['first_name']:'';
        $last_name=(isset($param['last_name']))?$param['last_name']:'';
        $email=(isset($param['email']))?$param['email']:'';
        $type_of_member=(isset($param['type_of_member']))?$param['type_of_member']:'';
        $data=DB::table('members')
            ->whereNull('members.deleted_at');
        if(!empty($first_name))
        {
            $data->where('members.first_name','LIKE','%'.$first_name.'%');
        }
        if(!empty($last_name))
        {
            $data->where('members.last_name','LIKE','%'.$last_name.'%');
        }
        if(!empty($email))
        {
            $data->where('members.email','LIKE','%'.$email.'%');
        }

        if(in_array($type_of_member,['admin','member']))
        {
            $data->where('members.type_of_member',$type_of_member);
        }

        if(!empty($id))
        {
            return $data
                ->where('member_id',$id)
                ->leftJoin('provinces','provinces.province_id','=','members.province_id')
                ->select([
                    'members.*',
                    'province_name'
                ])
                ->first();
        }
        if($is_all==true)
        {
            return $data->get();

        }
        return $data->paginate(10);
    }
    public static function update_type($member_id,$item)
    {
        return DB::table('members')
            ->where('member_id',$member_id)
            ->update($item);
    }
    public static function delete_member($member_id)
    {
        return DB::table('members')
            ->where('member_id', '=', $member_id)
            ->update(['members.deleted_at'=>date('Y-m-d H:i:s')]);
    }

    //////////////////////////////// ข้อมูลอู่ซ่อม ///////////////////////////////////

    public static function  data_garage($id='',$param=[],$is_all=false)
    {
        $garage_name=(isset($param['garage_name']))?$param['garage_name']:'';
        $address=(isset($param['address']))?$param['address']:'';
        $province_id=(isset($param['province_id']) && is_numeric($param['province_id']))?$param['province_id']:'';
        $data=DB::table('garages')
            ->whereNull('garages.deleted_at')
            ->leftJoin('provinces','provinces.province_id','=','garages.province_id')
            ->select([
                'garages.*',
                'provinces.province_name as garage_province'
            ]);
        if(!empty($garage_name))
        {
            $data->where('garages.garage_name','LIKE','%'.$garage_name.'%');
        }
        if(!empty($address))
        {
            $data->where('garages.address','LIKE','%'.$address.'%');
        }
        if(!empty($province_id))
        {
            $data->where('garages.province_id',$province_id);
        }
        if($is_all==true)
        {
            return $data->get();

        }
        return $data->paginate(10);
    }

    public static function show_garage($id)
    {
        $data=DB::table('garages')
            ->leftJoin('members','members.member_id','=','garages.member_id')
            ->leftJoin('provinces','provinces.province_id','=','garages.province_id')
            ->select([
                'garages.*',
                'members.*',
                'provinces.province_name as garage_province',//เปลี่ยนชื่อ เพราะไม่ให้ค่าซ้ำ
                'garages.mobile as mobile_garage',
                'garages.email as email_au',
            ])
            ->where('garages.garage_id',$id)->first();

        $services=DB::table('garages_services')
            ->leftJoin('services','services.service_id','=','garages_services.service_id')
            ->select([
                'service_name'
            ])
            ->where('garages_services.garage_id',$id)
            ->whereNull('garages_services.deleted_at')->get();
        $categories=DB::table('garages_categories')
            ->leftJoin('categories','categories.category_id','=','garages_categories.category_id')
            ->select([
                'category_name'
            ])
            ->where('garages_categories.garage_id',$id)
            ->whereNull('garages_categories.deleted_at')->get();
        $brands=DB::table('garages_brands')
            ->leftJoin('brands','brands.brand_id','=','garages_brands.brand_id')
            ->select([
                'brand_name'
            ])
            ->where('garages_brands.garage_id',$id)
            ->whereNull('garages_brands.deleted_at')->get();
        $insurances=DB::table('garages_insurances')
            ->leftJoin('insurances','insurances.insurance_id','=','garages_insurances.insurance_id')
            ->select([
                'insurance_name'
            ])
            ->where('garages_insurances.garage_id',$id)
            ->whereNull('garages_insurances.deleted_at')->get();
        $service='';
        $category='';
        $brand='';
        $insurance='';
        if(!empty($services))
        {
            foreach ($services as $index => $item) {
                if ($index != 0)
                {
                    $service.=',';//มันจะเป็น ช่วงล่าง,เครื่องยนต์,  แล้วก้อค่อยไปตัดในคอนโทเลอร์
                }
                $service.=$item->service_name;
            }
        }
        if(!empty($categories))
        {
            foreach ($categories as $index => $item) {
                if ($index != 0)
                {
                    $category.=',';
                }
                $category.=$item->category_name;
            }
        }
        if(!empty($brands))
        {
            foreach ($brands as $index => $item) {
                if ($index != 0)
                {
                    $brand.=',';
                }
                $brand.=$item->brand_name;
            }
        }
        if(!empty($insurances))
        {
            foreach ($insurances as $index => $item) {
                if ($index != 0)
                {
                    $insurance.=',';//มันจะเป็น ช่วงล่าง,เครื่องยนต์,  แล้วก้อค่อยไปตัดในคอนโทเลอร์
                }
                $insurance.=$item->insurance_name;
            }
        }
        $data->service_list=$service;
        $data_view=[
            'garage_name'   =>$data->garage_name,
            'fullname'      =>$data->title_name.$data->first_name.' '.$data->last_name,
            'address'       =>$data->address,
            'garage_province'=>$data->garage_province,
            'mobile_garage' =>$data->mobile_garage,
            'email_au'      =>$data->email_au,
            'service_list'  =>$service,
            'category_list'=>$category,
            'brand_list'    =>$brand,
            'insurance_list'=>$insurance,
            'open_garages'  =>$data->open_garages,
            'open_time'     =>$data->open_time,
            'close_time'    =>$data->close_time,
            'website'       =>$data->website,

        ];
        return $data_view;
    }
    public static function delete_garage($garage_id)
    {
        return DB::table('garages')
            ->where('garage_id', '=', $garage_id)
            ->update(['garages.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    //////////////////////////////// ข้อมูลงานบริการ ///////////////////////////////////
    public static function  data_service($service_id='',$param=[],$is_all=false)
    {
        $service_name=(isset($param['service_name']))?$param['service_name']:'';
        $data=DB::table('services')
            ->whereNull('services.deleted_at');

        if(!empty($service_name))
        {
            $data->where('services.service_name','LIKE','%'.$service_name.'%');
        }

        if(!empty($service_id))
        {
            return $data
                ->where('service_id',$service_id)
                ->select([
                    'services.*'
                ])
                ->first();
        }
        if($is_all==true)
        {
            return $data->get();
        }
        return $data->paginate(10);
    }
    public static function update_service($service_id,$item)
    {
        return DB::table('services')
            ->where('service_id',$service_id)
            ->update($item);
    }
    public static function  add_service()
    {
        $data=DB::table('services')
            ->whereNull('services.deleted_at');
        return $data->get();
    }

    public static function check_service($service_name,$service_id=0)
    {
        $service=DB::table('services')
            ->where('service_name',$service_name)
            ->whereNull('services.deleted_at');
        if($service_id!=0)
        {
            $service->where('service_id','!=',$service_id);
        }
        return $service->first();
    }
    public static function delete_service($service_id)
    {
        return DB::table('services')
            ->where('service_id', '=', $service_id)
            ->update(['services.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    //////////////////////////////// ข้อมูลงานบริการ /////////////////////////////////////

    public static function  data_sub_service($sub_services_id='',$param=[],$is_all=false)
    {
        $sub_service_name=(isset($param['sub_service_name']))?$param['sub_service_name']:'';
        $data=DB::table('sub_services')
            ->whereNull('sub_services.deleted_at')
            ->leftJoin('services','services.service_id','=','sub_services.service_id')
            ->select([
                'sub_services.*',
                'services.service_name as name_service',
            ]);
        if(!empty($sub_service_name))
        {
            $data->where('sub_services.sub_service_name','LIKE','%'.$sub_service_name.'%');
        }

        if(!empty($sub_services_id))
        {
            return $data
                ->where('sub_services_id',$sub_services_id)
                ->select([
                    'sub_services.*',
                    'services.service_name as name_service',
                ])
                ->first();
        }
        if($is_all==true)
        {
            return $data->get();
        }
        return $data->paginate(10);
    }
    public static function  add_sub_service()
    {
        $data=DB::table('sub_services')
            ->whereNull('sub_services.deleted_at');
        return $data->get();
    }
    public static function  data_service_all()
    {
        $data=DB::table('services')
            ->whereNull('services.deleted_at');
        return $data->get();
    }
    public static function check_sub_service($sub_service_name,$sub_services_id=0)
    {
        $service=DB::table('sub_services')
            ->where('sub_service_name',$sub_service_name)
            ->whereNull('sub_services.deleted_at');
        if($sub_services_id!=0)
        {
            $service->where('sub_services_id','!=',$sub_services_id);
        }
        return $service->first();
    }
    public static function update_sub_service($sub_services_id,$item)
    {
        return DB::table('sub_services')
            ->where('sub_services_id',$sub_services_id)
            ->update($item);
    }
    public static function delete_sub_service($sub_services_id)
    {
        return DB::table('sub_services')
            ->where('sub_services_id', '=', $sub_services_id)
            ->update(['sub_services.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    //////////////////////////////// ข้อมูลประเภทรถยนต์ ///////////////////////////////////
    public static function  data_category($category_id='',$param=[],$is_all=false)
    {
        $category_name=(isset($param['category_name']))?$param['category_name']:'';

        $data=DB::table('categories')
            ->whereNull('categories.deleted_at');
        if(!empty($category_name))
        {
            $data->where('categories.category_name','LIKE','%'.$category_name.'%');
        }
        if(!empty($category_id))
        {
            return $data
                ->where('category_id',$category_id)
                ->select([
                    'categories.*'
                ])
                ->first();
        }
        if($is_all==true)
        {
            return $data->get();
        }
        return $data->paginate(10);
    }
    public static function update_category($category_id,$item)
    {
        return DB::table('categories')
            ->where('category_id',$category_id)
            ->update($item);
    }
    public static function delete_category($category_id)
    {
        return DB::table('categories')
            ->where('category_id', '=', $category_id)
            ->update(['categories.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    public static function  add_category()
    {
        $data=DB::table('categories')
            ->whereNull('categories.deleted_at');
        return $data->get();
    }

    public static function check_category($category_name,$category_id=0)
    {
        $category=DB::table('categories')
            ->where('category_name',$category_name)
            ->whereNull('categories.deleted_at');
        if($category_id!=0)
        {
            $category->where('category_id','!=',$category_id);
        }
        return $category->first();
    }
    //////////////////////////////// ข้อมูลยี่ห้อรถ ///////////////////////////////////
    public static function  data_brand($brand_id='',$param=[],$is_all=false)
    {
        $brand_name=(isset($param['brand_name']))?$param['brand_name']:'';
        $data=DB::table('brands')
            ->whereNull('brands.deleted_at');
        if(!empty($brand_name))
        {
            $data->where('brands.brand_name','LIKE','%'.$brand_name.'%');
        }
        if(!empty($brand_id))
        {
            return $data
                ->where('brand_id',$brand_id)
                ->select([
                    'brands.*'
                ])
                ->first();
        }
        if($is_all==true)
        {
            return $data->get();
        }
        return $data->paginate(10);

    }
    public static function update_brand($brand_id,$item)
    {
        return DB::table('brands')
            ->where('brand_id',$brand_id)
            ->update($item);
    }
    public static function  add_brand()
    {
        $data=DB::table('brands')
            ->whereNull('brands.deleted_at');
        return $data->get();
    }
    public static function check_brand($brand_name,$brand_id=0)
    {
        $brand=DB::table('brands')
            ->where('brand_name',$brand_name)
            ->whereNull('brands.deleted_at');
        if($brand_id!=0)
        {
            $brand->where('brand_id','!=',$brand_id);
        }
        return $brand->first();
    }
    public static function delete_brand($brand_id)
    {
        return DB::table('brands')
            ->where('brand_id', '=', $brand_id)
            ->update(['brands.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    //////////////////////////////// ข้อมูลบริษัทประกันภัย ///////////////////////////////////
    public static function  data_insurance($insurance_id='',$param=[],$is_all=false)
    {
        $insurance_name=(isset($param['insurance_name']))?$param['insurance_name']:'';
        $data=DB::table('insurances')
            ->whereNull('insurances.deleted_at');
        if(!empty($insurance_name))
        {
            $data->where('insurances.insurance_name','LIKE','%'.$insurance_name.'%');
        }
        if(!empty($insurance_id))
        {
            return $data
                ->where('insurance_id',$insurance_id)
                ->select([
                    'insurances.*'
                ])
                ->first();
        }
        if($is_all==true)
        {
            return $data->get();
        }
        return $data->paginate(10);

    }
    public static function update_insurance($insurance_id,$item)
    {
        return DB::table('insurances')
            ->where('insurance_id',$insurance_id)
            ->update($item);
    }
    public static function  add_insurance()
    {
        $data=DB::table('insurances')
            ->whereNull('insurances.deleted_at');
        return $data->get();
    }
    public static function check_insurance($insurance_name,$insurance_id=0)
    {
        $insurance=DB::table('insurances')
            ->where('insurance_name',$insurance_name)
            ->whereNull('insurances.deleted_at');
        if($insurance_id!=0)
        {
            $insurance->where('insurance_id','!=',$insurance_id);
        }
        return $insurance->first();
    }
    public static function delete_insurance($insurance_id)
    {
        return DB::table('insurances')
            ->where('insurance_id', '=', $insurance_id)
            ->update(['insurances.deleted_at'=>date('Y-m-d H:i:s')]);
    }
    /////////////////////////////////////////////////////////
    public static function  data_reservation($reservation_id='',$param=[],$is_all=false)
    {
        $garage_name=(isset($param['garage_name']))?$param['garage_name']:'';
        $first_name=(isset($param['first_name']))?$param['first_name']:'';
        $last_name=(isset($param['last_name']))?$param['last_name']:'';
        $status=(isset($param['status']))?$param['status']:'';
        $data=DB::table('reservations_status')
            ->leftJoin('reservations','reservations_status.reservation_id','=','reservations.reservation_id')
            ->whereNull('reservations.deleted_at')
            ->where('reservations_status.active','Y')
            ->leftJoin('members','members.member_id','=','reservations.member_id')
            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
            ->leftJoin('provinces','provinces.province_id','=','garages.province_id')
            ->leftJoin('status','status.status_id','=','reservations_status.status_id');
        if(!empty($garage_name))
        {
            $data->where('garages.garage_name','LIKE','%'.$garage_name.'%');
        }
        if(!empty($first_name))
        {
            $data->where('members.first_name','LIKE','%'.$first_name.'%');
        }
        if(!empty($last_name))
        {
            $data->where('members.last_name','LIKE','%'.$last_name.'%');
        }
        if(!empty($status))
        {
            $data->where('reservations_status.status_id',$status);
        }
//        dd($status);

        if(!empty($reservation_id))
        {
            return $data
                ->where('reservations.reservation_id',$reservation_id)
                ->select([
                    'reservations.*',
                    'reservations.created_at',
                    'title_name',
                    'first_name',
                    'last_name',
                    'members.mobile',
                    'members.email',
                    'garages.garage_name',
                    'garages.address',
                    'garages.email',
                    'garages.mobile',
                    'garages.province_id',
                    'garages.open_time',
                    'garages.close_time',
                    'garages.open_garages',
                    'provinces.province_name',
                    DB::raw('GROUP_CONCAT(service_name) as list_service_name')
                ])
                ->leftJoin('reservations_services','reservations_services.reservation_id','=','reservations.reservation_id')
                ->leftJoin('services','services.service_id','=','reservations_services.service_id')
                ->groupby('reservations_services.reservation_id')
                ->first();
        }
        $data->select([
            'reservations.*',
            'title_name',
            'first_name',
            'last_name',
            'garage_name',
            'reservations_status.status_id',
            'status.status_name',

        ]);

        if($is_all==true)
        {
            return $data->get();
        }
        return $data->paginate(10);

    }
}