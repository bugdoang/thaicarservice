<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class MnmService  extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $service=ManagementModel::add_service();
        $html=view('management::service.add-service',compact('service'));
        return ['title'=>'เพิ่มข้อมูลงานบริการ','body'=>$html->render()];
    }


    public function add_service (Request $request)
    {
        $service_name    =$request->get('service_name');
        $is_exist   =ManagementModel::check_service($service_name);
        $check=array('service_name');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อข้อมูลงานบริการด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อข้อมูลงานบริการนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        $data = array(
            'service_name'   => $service_name,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );

        $service_id=DB::table('services')->insertGetId($data);
        return response(['เพิ่มข้อมูลงานบริการเรียบร้อยแล้้วค่ะ']);
    }
    public function delete($service_id)
    {
        ManagementModel::delete_service($service_id);
        return response(['ลบข้อมูลงานบริการเรียบร้อยแล้วค่ะ']);
    }
    public function update($service_id)
    {
        $service=ManagementModel::data_service($service_id);
        $html=view('management::service.edit-service',compact('service'));
        return ['title'=>'แก้ไขข้อมูลงานบริการ','body'=>$html->render()];
    }
    public function update_data($service_id,Request $request)
    {
        $service_name=$request->get('service_name');
        $is_exist   =ManagementModel::check_service($service_name,$service_id);
        $check=array('service_name');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อข้อมูลงานบริการด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อข้อมูลงานบริการนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        ManagementModel::update_service($service_id,[
            'service_name'=>$service_name,
            'updated_at' => date('Y-m-d H:i:s')

        ]);
        return response(['แก้ไขข้อมูลงานบริการเรียบร้อยแล้วค่ะ']);
    }

}