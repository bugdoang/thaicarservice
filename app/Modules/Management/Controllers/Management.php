<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Management\Controllers;


use App\Http\Controllers\Controller;

use App\Modules\Management\Models\ManagementModel;
use App\Modules\Management\Services\ManagementList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Management extends Controller
{
    public function index(Request $request)
    {
        $type       = Input::get('type');
        if (!in_array($type,['member','garage','brand','category','insurance','service','subservice','reservation']))
        {
            $type = 'member';
        }
        $list_view=ManagementList::get($type,$request);

        return view('management::management',compact('type','list_view'));
    }
}