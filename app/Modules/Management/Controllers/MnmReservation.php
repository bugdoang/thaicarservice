<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Booking\Models\BookingModel;
use App\Modules\Management\Models\ManagementModel;
use App\Modules\Status\Models\StatusModel;
use Illuminate\Support\Facades\Input;


class MnmReservation extends Controller
{
    public function index($reservation_id)
    {
        $reservation=ManagementModel::data_reservation($reservation_id);
        $reservation_status    =BookingModel::data_booking_status($reservation_id);
        $data_status=BookingModel::data_status($reservation_id);//ข้อมูลสถานะแบบตาราง
        $repair_list    =StatusModel::repair_list($reservation_id);

//        $service_name=[];
//        if(isset($reservation->list_service_name))
//        {
//            $service_name=explode(',',$reservation->list_service_name);
//        }
//        dd($data_status);
        $html=view('management::reservation.view-reservation',compact('reservation','status','data_status','reservation_status','repair_list'));
        return ['title'=>'ดูข้อมูลการจอง','body'=>$html->render()];
    }
}