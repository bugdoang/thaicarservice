<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class MnmSubService  extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $sub_service=ManagementModel::add_sub_service();
        $service_all=ManagementModel::data_service_all();
//        dd($service_all);
        $html=view('management::subservice.add-sub-service',compact('sub_service','service_all'));
        return ['title'=>'เพิ่มข้อมูลงานบริการ','body'=>$html->render()];
    }
    public function add_sub_service (Request $request)
    {
        $sub_service_name    =$request->get('sub_service_name');
        $service_id=$request->get('service_id');
        $is_exist   =ManagementModel::check_sub_service($sub_service_name);
        $check=array('sub_service_name','service_id');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อข้อมูลงานบริการนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        $data = array(
            'sub_service_name'   => $sub_service_name,
            'service_id'   => $service_id,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $sub_services_id=DB::table('sub_services')->insertGetId($data);
        return response(['เพิ่มข้อมูลงานบริการเรียบร้อยแล้วค่ะ']);
    }
    public function delete($sub_services_id)
    {
        ManagementModel::delete_sub_service($sub_services_id);
        return response(['ลบข้อมูลงานบริการเรียบร้อยแล้วค่ะ']);
    }
    public function update($sub_services_id)
    {
        $sub_service=ManagementModel::data_sub_service($sub_services_id);
        $service_all=ManagementModel::data_service_all();

//        dd($service_all);
        $html=view('management::subservice.edit-sub-service',compact('sub_service','service_all'));
        return ['title'=>'แก้ไขข้อมูลงานบริการ','body'=>$html->render()];
    }
    public function update_data($sub_services_id,Request $request)
    {
        $sub_service_name    =$request->get('sub_service_name');
        $service_id          =$request->get('service_id');
        $is_exist   =ManagementModel::check_sub_service($sub_service_name,$sub_services_id);
        $check=array('sub_service_name','service_id');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อข้อมูลงานบริการด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อข้อมูลงานบริการนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }

        ManagementModel::update_sub_service($sub_services_id,[
            'sub_service_name'   => $sub_service_name,
            'service_id'         => $service_id,
            'updated_at'         => date('Y-m-d H:i:s')
        ]);
        return response(['แก้ไขข้อมูลงานบริการเรียบร้อยแล้วค่ะ']);

//        $data = array(
//            'sub_service_name'   => $sub_service_name,
//            'service_id'   => $service_id,
//            'created_at'    => date('Y-m-d H:i:s'),
//            'updated_at'    => date('Y-m-d H:i:s')
//        );
//        $sub_services_id=DB::table('sub_services')->insertGetId($data);
//        return response(['อัพเดตสถานะเรียบร้อยแล้วค่ะ']);
    }

}