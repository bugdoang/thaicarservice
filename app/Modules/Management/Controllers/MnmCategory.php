<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class MnmCategory  extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $category=ManagementModel::add_category();
        $html=view('management::category.add-category',compact('category'));
        return ['title'=>'เพิ่มข้อมูลประเภทรถยนต์','body'=>$html->render()];
    }


    public function add_category (Request $request)
    {
        $category_name    =$request->get('category_name');
        $image            =$request->get('image');
        $is_exist   =ManagementModel::check_category($category_name);
        $check=array('category_name','image');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อ/รูปภาพประเภทรถยนต์ด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อประเภทรถยนต์นี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        $data = array(
            'category_name'   => $category_name,
            'image_category'   => $image,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );

        $category_id=DB::table('categories')->insertGetId($data);
        return response(['เพิ่มข้อมูลงานบริการเรียบร้อยแล้้วค่ะ']);
    }
    public function delete($category_id)
    {
        ManagementModel::delete_category($category_id);
        return response(['ลบข้อมูลประเภทรถยนต์เรียบร้อยแล้วค่ะ']);
    }
    public function update($category_id)
    {
        $category=ManagementModel::data_category($category_id);
        $html=view('management::category.edit-category',compact('category'));
        return ['title'=>'แก้ไขข้อมูลประเภทรถยนต์','body'=>$html->render()];
    }
    public function update_data($category_id,Request $request)
    {
        $category_name=$request->get('category_name');
        $image=$request->get('image');
        $is_exist   =ManagementModel::check_category($category_name,$category_id);
        $check=array('category_name','image');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อ/รูปภาพประเภทรถยนต์ด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อประเภทรถยนต์นี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        ManagementModel::update_category($category_id,[
            'category_name'=>$category_name,
            'image_category'=>$image,
            'updated_at' => date('Y-m-d H:i:s')

        ]);
        return response(['แก้ไขข้อมูลประเภทรถยนต์เรียบร้อยแล้วค่ะ']);
    }

}