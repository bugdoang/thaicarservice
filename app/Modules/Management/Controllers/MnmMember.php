<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use Illuminate\Support\Facades\Input;


class MnmMember extends Controller
{

    public function delete($member_id)
    {
        ManagementModel::delete_member($member_id);
        return response(['ลบข้อมูลสมาชิกเรียบร้อยแล้วค่ะ']);
    }
    public function update($member_id)
    {
        $member=ManagementModel::data_member($member_id);
        $type_of_member=config('myconfig.type_of_member');
        $html=view('management::member.edit-member',compact('member','type_of_member'));
        return ['title'=>'แก้ไขข้อมูลสมาชิก','body'=>$html->render()];
    }
    public function update_data($member_id,Request $request)
    {
        $type_of_member=$request->get('type_of_member');
        ManagementModel::update_type($member_id,[
            'type_of_member'=>$type_of_member,
            'updated_at' => date('Y-m-d H:i:s')

        ]);
        return response(['แก้ไขข้อมูลเรียบร้อยแล้วค่ะ']);
    }
    public function index($member_id)
    {
        $gender=['M'=>'ชาย','F'=>'หญิง'];
        $member=ManagementModel::data_member($member_id);
        $member->gender=$gender[$member->gender];
        $html=view('management::member.view-member',compact('member'));
        return ['title'=>'ดูข้อมูลสมาชิก','body'=>$html->render()];
    }
}