<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use Illuminate\Support\Facades\Input;


class MnmGarage extends Controller
{
    public function delete($garage_id)
    {
        ManagementModel::delete_garage($garage_id);
        return response(['ลบข้อมูลอู่เรียบร้อยแล้วค่ะ']);
    }
    public function index($garage_id)
    {
        $garage=ManagementModel::show_garage($garage_id);
        $services=[];
        if(isset($garage['service_list']) && !empty($garage['service_list']))
        {
            $services=explode(',',$garage['service_list']);
        }
        $categories=[];
        if(isset($garage['category_list']) && !empty($garage['category_list']))
        {
            $categories=explode(',',$garage['category_list']);
        }
        $brands=[];
        if(isset($garage['brand_list']) && !empty($garage['brand_list']))
        {
            $brands=explode(',',$garage['brand_list']);
        }
        $insurances=[];
        if(isset($garage['insurance_list']) && !empty($garage['insurance_list']))
        {
            $insurances=explode(',',$garage['insurance_list']);
        }
        $html=view('management::garage.view-garage',compact('garage','services','categories','brands','insurances'));
        return ['title'=>'ดูข้อมูลสมาชิก','body'=>$html->render()];
    }
}