<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class MnmInsurance  extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $insurance=ManagementModel::add_insurance();
        $html=view('management::insurance.add-insurance',compact('insurance'));
        return ['title'=>'เพิ่มข้อมูลบริษัทประกัน','body'=>$html->render()];
    }


    public function add_insurance (Request $request)
    {
        $insurance_name    =$request->get('insurance_name');
        $image              =$request->get('image');
        $is_exist   =ManagementModel::check_insurance($insurance_name);
        $check=array('insurance_name','image');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อ/โลโก้บริษัทประกันภัยด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อบริษัทประกันภัยนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        $data = array(
            'insurance_name'   => $insurance_name,
            'image_insurance'   => $image,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );

        $service_id=DB::table('insurances')->insertGetId($data);
        return response(['เพิ่มข้อมูลบริษัทประกันภัยเรียบร้อยแล้้วค่ะ']);
    }

    public function delete($insurance_id)
    {
        ManagementModel::delete_insurance($insurance_id);
        return response(['ลบข้อมูลบริษัทประกันภัยเรียบร้อยแล้วค่ะ']);
    }

    public function update($insurance_id)
    {
        $insurance=ManagementModel::data_insurance($insurance_id);
        $html=view('management::insurance.edit-insurance',compact('insurance'));
        return ['title'=>'แก้ไขข้อมูลบริษัทประภัย','body'=>$html->render()];
    }
    public function update_data($insurance_id,Request $request)
    {
        $insurance_name=$request->get('insurance_name');
        $image=$request->get('image');
        $is_exist   =ManagementModel::check_insurance($insurance_name,$insurance_id);
        $check=array('insurance_name','image');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อ/โลโก้บริษัทประกันภัยด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อบริษัทประกันภัยนี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        ManagementModel::update_insurance($insurance_id,[
            'insurance_name'=>$insurance_name,
            'image_insurance'=>$image,
            'updated_at' => date('Y-m-d H:i:s')


        ]);
        return response(['แก้ไขข้อมูลบริษัทประกันภัยเรียบร้อยแล้วค่ะ']);
    }

}