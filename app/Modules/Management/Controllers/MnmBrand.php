<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/8/2559
 * Time: 12:56
 */

namespace App\Modules\Management\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Management\Models\ManagementModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class MnmBrand  extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $brand=ManagementModel::add_brand();
        $html=view('management::brand.add-brand',compact('brand'));
        return ['title'=>'เพิ่มข้อมูลยี่ห้อรถยนต์','body'=>$html->render()];
    }


    public function add_brand (Request $request)
    {
        $brand_name    =$request->get('brand_name');
        $image              =$request->get('image');
        $is_exist   =ManagementModel::check_brand($brand_name);
        $check=array('brand_name','image');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อ/โลโก้ยี่ห้อรถยนต์ด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อยี่ห้อรถยนต์นี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        $data = array(
            'brand_name'   => $brand_name,
            'image_brand'   => $image,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $brand_id=DB::table('brands')->insertGetId($data);
        return response(['เพิ่มข้อมูลยี่ห้อรถยนต์เรียบร้อยแล้วค่ะ']);
    }

    public function delete($brand_id)
    {
        ManagementModel::delete_brand($brand_id);
        return response(['ลบข้อมูลยี่ห้อรถยนต์เรียบร้อยแล้วค่ะ']);
    }

    public function update($brand_id)
    {
        $brand=ManagementModel::data_brand($brand_id);
        $html=view('management::brand.edit-brand',compact('brand'));
        return ['title'=>'แก้ไขข้อมูลยี่ห้อรถยนต์','body'=>$html->render()];
    }

    public function update_data($brand_id,Request $request)
    {
        $brand_name=$request->get('brand_name');
        $image=$request->get('image');
        $is_exist   =ManagementModel::check_brand($brand_name,$brand_id);
//        dd($is_exist);
        $check=array('brand_name','image');
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณาใส่ชื่อ/โลโก้ยี่ห้อรถยนต์ด้วยค่ะ'],422);
            }
        }
        if(!empty($is_exist))
        {
            return response(['ชื่อยี่ห้อรถยนต์นี้มีในระบบแล้ว กรุณาป้อนใหม่อีกครั้งค่ะ'], 422);
        }
        ManagementModel::update_brand($brand_id,[
            'brand_name'=>$brand_name,
            'image_brand'=>$image,
            'updated_at' => date('Y-m-d H:i:s')


        ]);
        return response(['แก้ไขข้อมูลยี่ห้อรถยนต์เรียบร้อยแล้วค่ะ']);
    }

}