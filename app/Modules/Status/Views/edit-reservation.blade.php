<form id="fm-edit-member" method="PUT" action="/status/edit/{{$reservation->reservation_id}}">
    {{ csrf_field() }}
<div id="edit-reservation" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไขการจองอู่ซ่อมรถ</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px;">
                <div class="row">
                    <div class="form-group col-lg-12 col-md-12">
                        <div class="header-detail myfont line1">
                            ข้อมูลอู่
                        </div>
                        <div class="col-lg-3 col-md-3">
                            ชื่ออู่  :
                        </div>
                        <div class="col-lg-9 col-md-9">
                            {{$reservation->garage_name}}
                        </div>
                        <div class="col-lg-3 col-md-3">
                            ที่ตั้ง  :
                        </div>
                        <div class="col-lg-9 col-md-9">
                            {{$reservation->address}}
                        </div>
                        <div class="col-lg-3 col-md-3">
                            จังหวัด  :
                        </div>
                        <div class="col-lg-9 col-md-9">
                            {{$reservation->province_name}}
                        </div>
                        <div class="col-lg-3 col-md-3">
                            อีเมล์  :
                        </div>
                        <div class="col-lg-9 col-md-9">
                            {{$reservation->email}}
                        </div>
                        <div class="col-lg-3 col-md-3">
                            เบอร์โทร :
                        </div>
                        <div class="col-lg-9 col-md-9">
                            {{$reservation->mobile}}
                        </div>
                        @if(!empty($garage_detail->open_garages))
                            <div class="col-lg-3 col-md-3">
                                วันเปิดทำการ :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$reservation->open_garages}}
                            </div>
                        @endif
                        @if(!empty($reservation->open_time && $reservation->close_time))
                            <div class="col-lg-3 col-md-3">
                                เวลาเปิด-ปิด :
                            </div>
                            <div class="col-lg-9 col-md-9">
                                {{$reservation->open_time}} - {{$reservation->close_time}} น.
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12 col-lg-12 ">
                        <div class="row set">
                            <div class="header-detail myfont line">
                                รายละเอียดการจองอู่
                            </div>
                            <div class="row"  style="margin-left: 15px;">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2">
                                        วันที่นัด <span style="color:red;"> * </span> :
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <input value="{{$reservation->date}}" type="text" name="_date" id="_date" class="form-control datepicker" style="width: 200px;">
                                    </div>
                                    @if(!empty($reservation->open_time && $reservation->close_time))
                                        <div class="col-lg-2 col-md-2" >
                                            เวลานัด <span style="color:red;"> * </span> :
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <select class="form-control" name="_time" id="_time">
                                                <option value="">กรุณาเลือก</option>
                                                @if(isset($times))
                                                    @foreach($times as $time)
                                                        <option {{($reservation->time==$time)?'selected':''}} value="{{$time}}">
                                                            {{$time}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    @else
                                        <div class="col-lg-2 col-md-2" >
                                            เวลานัด <span style="color:red;"> * </span> :
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <select class="form-control" name="_time" id="_time">
                                                <option value="">กรุณาเลือก</option>
                                                @if(isset($time_all))
                                                    @foreach($time_all as $time)
                                                        <option {{($reservation->time==$time)?'selected':''}} value="{{$time}}">
                                                            {{$time}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    @endif
                                </div>

                                @if(!empty($categories[$reservation->garage_id]))
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 listservice">
                                            ประเภทรถ :
                                        </div>
                                        <div class="col-lg-5 col-md-5"  style="margin-top: 10px;">
                                            <select class="form-control" name="_chk_category" id="_chk_category">
                                                <option value="">กรุณาเลือก</option>
                                                @if(isset($categories[$reservation->garage_id]))
                                                    @foreach($categories[$reservation->garage_id] as $item)
                                                        <option {{($reservation->category_name==$item->category_name)?'selected':''}} value="{{$item->category_id}}">
                                                            {{$item->category_name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($brands[$reservation->garage_id]))
                                    <div class="row" >
                                        <div class="col-lg-2 col-md-2 listservice">
                                            ยี่ห้อรถ :
                                        </div>
                                        <div class="col-lg-5 col-md-5"  style="margin-top: 10px;">
                                            <select class="form-control" name="_chk_brand" id="_chk_brand">
                                                <option value="">กรุณาเลือก</option>
                                                @if(isset($brands[$reservation->garage_id]))
                                                    @foreach($brands[$reservation->garage_id] as $item)
                                                        <option {{($reservation->brand_name==$item->brand_name)?'selected':''}} value="{{$item->brand_name}}">
                                                            {{$item->brand_name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($insurances[$reservation->garage_id]))
                                    <div class="row" >
                                        <div class="col-lg-2 col-md-2 listservice">
                                            บริษัทประกันภัย :
                                        </div>
                                        <div class="col-lg-5 col-md-5"  style="margin-top: 10px;">
                                            <select class="form-control" name="_chk_insurance" id="_chk_insurance">
                                                <option value="">กรุณาเลือก</option>
                                                @if(isset($insurances[$reservation->garage_id]))
                                                    @foreach($insurances[$reservation->garage_id] as $item)
                                                        <option {{($reservation->insurance_name==$item->insurance_name)?'selected':''}} value="{{$item->insurance_name}}">
                                                            {{$item->insurance_name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($services[$reservation->garage_id]))
                                    <div class="row">
                                        {{--<div class="col-lg-3 col-md-3 listservice">--}}
                                            {{--รายการซ่อม <span style="color:red;"> * </span> :--}}
                                        {{--</div>--}}
                                        {{--<div class="col-lg-9 col-md-9">--}}
                                            {{--@if(!empty($services[$reservation->garage_id]))--}}
                                                {{--@foreach($services[$reservation->garage_id] as $service)--}}
                                                    {{--<div class="row" style="margin-right: 30px; height: auto; width: 486px;">--}}
                                                        {{--<div class="col-lg-5 col-md-5" style="padding-right: 0px; padding-top: 7px;">--}}
                                                            {{--<label class="font-original">--}}
                                                                {{--<input type="checkbox" name="_chk_service[]" {{(in_array($service->service_id,$service_selected))?'checked':''}} value="{{$service->service_id}}">--}}
                                                                {{--{{$service->service_name}}--}}
                                                                {{--@if(!empty($service->service_detail))--}}
                                                                    {{--<div class="row">--}}
                                                                        {{--<div class="col-lg-8 col-md-8 listservice">--}}
                                                                            {{--<textarea name="service_detail"  rows="1" class="text-area" disabled="disabled" style="height: 24px;">{{$service->service_detail}}</textarea>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--@endif--}}
                                                            {{--</label>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                        @endif
                                        <div class="row set">
                                            <div class="col-lg-12 col-md-12 checkbox1">
                                                รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม <span style="color:red;"> * </span>  :
                                            </div>

                                        </div>
                                        <div class="row set">
                                            <div class="col-lg-2 col-md-2 listservice"></div>
                                            <div class="col-lg-10 col-md-10 listservice">
                                                <textarea name="_reservation_detail" id="_reservation_detail" cols="20" rows="3" class="text-area">{{$reservation->reservation_detail}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</form>