<form id="fm-edit-status-confirm" method="PUT" action="/status/edit/confirm/{{$reservation->reservation_id}}">
    {{ csrf_field() }}

    <div id="edit-status-confirm" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 50%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> ยืนยันการจอง</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>สถานะการดำเนินงาน : </label>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        {{$reservation->status_name}}
                    </div>
                </div>
                @if(!empty($repair_list))
                <div class="row">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>รายการซ่อมเบื้องต้น : </label>
                    </div>

                    <table class="table table-bordered">

                        <tr>
                            <th class="text-center" > #</th>
                            <th class="text-center"  style="width: 316px;"> ประเภทการซ่อม</th>
                            <th class="text-center"  style="width: 316px;"> รายการซ่อม</th>
                            <th class="text-center"  style="width: 181px;"> ยืนยันรายการ</th>
                        </tr>
                        @foreach($repair_list as $index=>$item)
                        <tr>
                            <td class="text-center">
                                {{$index+1}}
                            </td>
                            <td class="text-center">
                                {{$item->service_name}}
                            </td>
                            <td class="text-center">
                                @if(!empty($item->sub_service_name))
                                 {{$item->sub_service_name}}
                                @else
                                 -
                                @endif
                            </td>
                            <td class="text-center">
                                <input type="checkbox" name="status_repair_list_member[{{$item->repair_list_id}}]" value="Y">
                            </td>
                        </tr>
                        @endforeach
                        </table>
                </div>
                @endif
                <div class="row ">
                    <div class="form-group col-md-4 col-lg-4">
                        <label>ตอบรับการจอง : </label>
                    </div>
                    <div class="form-group col-md-4 col-lg-4">
                        <select class="form-control" name="confirm_select" id="confirm_select">
                            <option value="">กรุณาเลือก</option>
                            @if(!empty($reservation))
                                @if($reservation->status_id==10)
                                    @foreach($status as $index=>$item)
                                        @if($index=='3' || $index=='4')
                                            <option  value="{{$item->status_id}}">
                                                {{$item->status_name}}
                                            </option>
                                        @endif
                                    @endforeach
                                @endif
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    </form>



