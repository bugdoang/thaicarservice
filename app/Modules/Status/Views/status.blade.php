@extends('masterlayout')
@section('title','รายการจอง')
@section('content')
    <!-- Page Content -->
    <div class="row">
        <!--เริ่มกลาง-->
        <div class="register-container col-lg-12 col-md-12"  style="left: 0px; margin-left: 15px; padding-right: 30px;">
            <div class="panel-title">
                <h2 class="header-text myfont color2">รายการจอง</h2>
            </div>
            <form action="" method="post" id="fileForm" role="form">
                <div class="item-border">
                    @if(!empty($reservation_me))
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center" > ลำดับที่</th>
                            <th class="text-center"  style="width: 316px;"> ชื่ออู่ซ่อมรถ</th>
                            <th class="text-center"  style="width: 181px;"> วันที่จอง</th>
                            <th class="text-center" style="width: 181px;"> วันที่นัดเข้าอู่</th>
                            <th class="text-center"  > สถานะ</th>
                            <th class="text-center"  > </th>

                        </tr>
                        @foreach($reservation_me as $index=>$item)
                            <tr>
                                <td class="text-center">
                                    {{$index+1}}
                                </td>
                                <td>
                                    {{$item->garage_name}}
                                </td>
                                <td class="text-center">{{\App\Helepers\DateFormat::Date_Clock($item->created_at)}}</td>
                                <td class="text-center">{{\App\Helepers\DateFormat::thaiDate($item->date)}}</td>
                                <td class="text-center">
                                    @if($item->status_id=='10')
                                        {{$item->status_name}}<br>
                                        <a href="/status/edit/confirm/{{$item->reservation_id}}" style="color: red" title="ตอบรับการจอง" class="edit-content">
                                            คลิกที่นี่เพื่อตอบรับรายการซ่อม
                                        </a>
                                    @else
                                        {{$item->status_name}}
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="/status/{{$item->reservation_id}}" title="รายละเอียดการจอง" class="edit-content">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </a>
                                    @if($item->status_id=='1')
                                        <a href="/status/edit/{{$item->reservation_id}}" title="แก้ไขการจอง" class="edit-content">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                    @endif

                                    <a href="/status/cancel/{{$item->reservation_id}}" title="ยกเลิกการจอง" class="cancel-content" name="cancel" id="cancel">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                    </table>
                        @else
                        <p style="text-align: center; margin-top: 10px;">ไม่มีรายการจอง</p>
                    @endif
                </div>
            </form><!-- ends register form -->
        </div>

    </div>
    @stop