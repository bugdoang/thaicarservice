<div id="view-reservation" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 60%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> ข้อมูลการจองอู่ซ่อมรถ</h4>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">
                            <div class="header-detail myfont line1">
                                ข้อมูลอู่
                            </div>
                            <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                <b >ชื่ออู่ : </b>
                            </div>
                            <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                {{$reservation->garage_name}}
                            </div>
                            <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                <b>ที่ตั้ง : </b>
                            </div>
                            <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                {{$reservation->address}}
                            </div>
                            <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                <b>จังหวัด : </b>
                            </div>
                            <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                {{$reservation->province_name}}
                            </div>
                            <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                <b>อีเมล์ : </b>
                            </div>
                            <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                {{$reservation->email}}
                            </div>
                            <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                <b>เบอร์โทร : </b>
                            </div>
                            <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                {{$reservation->mobile}}
                            </div>
                            @if(!empty($garage_detail->open_garages))
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>วันเปิดทำการ : </b>
                                </div>
                                <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                    {{$reservation->open_garages}}
                                </div>
                            @endif
                            @if(!empty($reservation->open_time && $reservation->close_time))
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>เวลาเปิด-ปิด : </b>
                                </div>
                                <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                    {{$reservation->open_time}} - {{$reservation->close_time}} น.
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-lg-12 "  style="margin-bottom: 5px;">
                            <div class="row set">
                                <div class="header-detail myfont line">
                                    รายละเอียดการจองอู่
                                </div>
                            </div>
                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>วันที่ทำการจอง :</b>
                                </div>
                                <div class="col-lg-9 col-md-9"  style="margin-bottom: 5px;">
                                    {{\App\Helepers\DateFormat::Date_Clock($reservation->created_at)}}
                                </div>
                            </div>
                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>วันที่นัดเข้าอู่ : </b>
                                </div>
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    {{$reservation->date}}
                                </div>
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>เวลา :</b>
                                </div>
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    {{$reservation->time}} น.
                                </div>
                            </div>
                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                                @if(!empty($reservation->category_id))
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>ประเภทรถยนต์ : </b>
                                </div>
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    {{$reservation->category_name}}
                                </div>
                                @endif
                                @if(!empty($reservation->brand_id))
                                    <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>ยี่ห้อรถยนต์ : </b>
                                </div>
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    {{$reservation->brand_name}}
                                </div>
                                @endif
                            </div>
                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                                @if(!empty($reservation->insurance_id))
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    <b>บริษัทประกันภัย : </b>
                                </div>
                                <div class="col-lg-3 col-md-3"  style="margin-bottom: 5px;">
                                    {{$reservation->insurance_name}}
                                </div>
                                @endif
                            </div>
                            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                                @if(!empty($reservation->reservation_detail))
                                    <div class="col-lg-12 col-md-12"  style="margin-bottom: 5px;">
                                        <b>รายละเอียดอาการรถ/จุดที่ต้องการจะซ่อม : </b>
                                    </div>
                                    <div class="col-lg-12 col-md-12"  style="margin-bottom: 5px; color: red">
                                        <input class="form-control"  disabled="disabled"  value=" {{$reservation->reservation_detail}}"/>
                                    </div>
                                @endif
                                @if(!empty($repair_list))
                                <div class="header-detail myfont line">
                                    รายการซ่อมเบื้องต้น
                                </div>
                                <div class="row"   style="width: 90%; margin-left: 15px;">
                                    <table class="table table-bordered" >
                                        <tr>
                                                <th class="text-center" > ลำดับที่</th>
                                                <th class="text-center" > ประเภทรายการซ่อม</th>
                                                <th class="text-center" > รายการซ่อม</th>
                                                <th class="text-center" > รายการที่ยืนยันการซ่อม</th>
                                            </tr>
                                            @foreach($repair_list as $index=>$item)

                                            <tr>
                                                        <td class="text-center">
                                                            {{$index+1}}
                                                        </td>
                                                        <td class="text-center">
                                                            {{$item->service_name}}
                                                        </td>

                                                        <td class="text-center">
                                                            @if($item->sub_service_name==null)
                                                                -
                                                            @else
                                                                {{$item->sub_service_name}}
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($item->status_repair_list_member=='Y')
                                                                <i class="fa fa-check" aria-hidden="true" style="color: green"></i>
                                                            @elseif($item->status_repair_list_member=='N')
                                                                <i class="fa fa-times" aria-hidden="true" style="color: red"></i>
                                                            @endif
                                                        </td>
                                                    </tr>
                                            @endforeach
                                    </table>
                                </div>
                                @endif

                                <div class="header-detail myfont line">
                                    สถานะการทำงาน
                                </div>
                            <div class="col-md-12 col-lg-12">
                                <p><b>สถานะล่าสุด : </b>
                                    <span>
                                        {{$reservation_status->status_name}} (แก้ไขสถานะล่าสุดวันที่ {{$reservation_status->created_at}})
                                    </span>
                                </p>
                            </div>
                                <div class="row"   style="width: 80%; margin-left: 15px;">
                                    <table class="table table-bordered" >
                                        @if(!empty($data_status))
                                            <tr>
                                                <th class="text-center" > ลำดับที่</th>
                                                <th class="text-center" > ชื่อสถานะ</th>
                                                <th class="text-center" > วันที่เปลี่ยนสถานะ</th>
                                                <th class="text-center" > อัพเดตโดย</th>

                                            </tr>
                                            @foreach($data_status as $index=>$item)
                                                @if($item->active == 'Y')
                                                    <tr style="background-color:#F8D486">
                                                @else
                                                    <tr>
                                                        @endif
                                                        <td class="text-center">
                                                            {{$index+1}}
                                                        </td>
                                                        <td class="text-center">
                                                            {{$item->status_name}}
                                                        </td>

                                                        <td class="text-center">
                                                            {{\App\Helepers\DateFormat::Date_Clock($item->created_at)}}
                                                        </td>
                                                        <td class="text-center">
                                                            @if($item->status_id==3 || $item->status_id==4 || $item->status_id==6 || $item->status_id==7 || $item->status_id==8 || $item->status_id==9 || $item->status_id==10)
                                                                อู่ซ่อมรถยนต์
                                                            @else
                                                                คุณ{{$reservation->first_name}} {{$reservation->last_name}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                    </table>
                                    @endif

                                </div>

                            </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
