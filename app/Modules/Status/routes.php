<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/8/2559
 * Time: 11:46
 */
Route::group(['middleware'=>['auth']],function(){
Route::get('/status','\App\Modules\Status\Controllers\Status@index');
Route::get('/status/{id}','\App\Modules\Status\Controllers\Status@viewReservation');

Route::get('/status/edit/{id}','\App\Modules\Status\Controllers\Editreservation@index');
Route::put('/status/edit/{reservation_id}','\App\Modules\Status\Controllers\Editreservation@edit');
Route::get('/status/edit/confirm/{reservation_id}','\App\Modules\Status\Controllers\Editreservation@edit_confirm');
Route::put('/status/edit/confirm/{reservation_id}','\App\Modules\Status\Controllers\Editreservation@send_edit_confirm');

Route::delete('/status/delete/{reservation_id}', '\App\Modules\Status\Controllers\DeleteStatus@delete');
Route::put('/status/cancel/{reservation_id}', '\App\Modules\Status\Controllers\Editreservation@cancel');

});


