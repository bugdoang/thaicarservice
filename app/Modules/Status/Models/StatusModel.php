<?php
namespace App\Modules\Status\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/10/2559
 * Time: 17:31
 */
class StatusModel extends Model
{
    public static function  reservation_me($member_id)
    {
        return DB::table('reservations')
            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
            ->leftJoin('reservations_status','reservations_status.reservation_id','=','reservations.reservation_id')
            ->leftJoin('status','status.status_id','=','reservations_status.status_id')
            ->select([
                'reservations.*',
                'garage_name',
                'reservations_status.status_id',
                'status.status_name'
            ])
            ->where('reservations.member_id',$member_id)
            ->where('reservations_status.active','Y')
            ->whereNull('reservations.deleted_at')
            ->orderBy('reservations.created_at','DESC')
            ->get();
        }


    public static function  reservation_view($id='')
    {
        $data=DB::table('reservations')
            ->leftJoin('members','members.member_id','=','reservations.member_id')
            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
            ->leftJoin('provinces', 'provinces.province_id', '=', 'garages.province_id')
            ->leftJoin('brands','brands.brand_id','=','reservations.brand_id')
            ->leftJoin('categories','categories.category_id','=','reservations.category_id')
            ->leftJoin('insurances','insurances.insurance_id','=','reservations.insurance_id')
            ->select([
                'garages.garage_name',
                'garages.address',
                'garages.mobile',
                'garages.open_time',
                'garages.close_time',
                'garages.open_garages',
                'garages.email',
                'provinces.province_name',
                'members.first_name',
                'members.last_name',
                'reservations.*',
                'brands.brand_name',
                'categories.category_name',
                'insurances.insurance_name',
                'provinces.province_name',
            ]);

             if(!empty($id))
        {
            return $data
                ->where('reservations.reservation_id',$id)
                ->first();
        }
        return $data->get();
    }
    public static function  reservation_view_status($id='')
    {
        $data=DB::table('reservations')
            ->leftJoin('reservations_status','reservations_status.reservation_id','=','reservations.reservation_id')
            ->leftJoin('status','status.status_id','=','reservations_status.status_id')
            ->select([
                'reservations.reservation_id',
                'reservations_status.status_id',
                'reservations_status.active',
                'reservations_status.created_at',
                'status.status_name'
            ]);

        if(!empty($id))
        {
            return $data
                ->where('reservations.reservation_id',$id)
                ->where('reservations_status.active','Y')
                ->first();
        }
        return $data
            ->get();
    }
    public static function  data_status($id='')
    {
        $data=DB::table('reservations')
            ->whereNull('reservations.deleted_at');
        return $data
            ->where('reservations.reservation_id',$id)
            ->select([
                'reservations.reservation_id',
                'reservations_status.status_id',
                'reservations_status.active',
                'reservations_status.created_at',
            ])
            ->leftJoin('reservations_status','reservations_status.reservation_id','=','reservations.reservation_id')
            ->get();
    }
    //    public static function  reservation_view($id='')
//    {
//        $data=DB::table('reservations_services')
//            ->leftJoin('reservations','reservations_services.reservation_id','=','reservations.reservation_id')
//            ->leftJoin('services','services.service_id','=','reservations_services.service_id')
//            ->leftJoin('garages','garages.garage_id','=','reservations.garage_id')
//            ->leftJoin('provinces', 'provinces.province_id', '=', 'garages.province_id')
//            ->select([
//                'reservations.*',
//                'garages.garage_name',
//                'garages.address',
//                'garages.mobile',
//                'garages.open_time',
//                'garages.close_time',
//                'garages.open_garages',
//                'garages.email',
//                'provinces.province_name',
//                DB::raw('GROUP_CONCAT(service_name) as list_service_name')
//            ])
//            ->groupby('reservations_services.reservation_id');
//        if(!empty($id))
//        {
//            return $data->where('reservations.reservation_id',$id)
//            ->first();
//        }
//        return $data->get();
//    }
    public static function  reservation_services($reservation_id)
    {
        return DB::table('reservations_services')
            ->whereNull('deleted_at')
            ->where('reservation_id',$reservation_id)
            ->get();
    }

    public static function update_reservation($reservation_id,$item)
    {
        return DB::table('reservations')
            ->where('reservation_id',$reservation_id)
            ->update($item);
    }
    public static function update_active($reservation_id,$item)
    {
        return DB::table('reservations_status')
            ->where('reservation_id',$reservation_id)
            ->update($item);
    }
    public static function update_repair_list($reservation_id,$items)
    {
        if(isset($items['status_repair_list_member']) && !empty($items['status_repair_list_member'])
            && is_array($items['status_repair_list_member']))
        {
            foreach ($items['status_repair_list_member'] as $key => $item) {
                DB::table('repair_list')
                    ->where('repair_list_id', $key)
                    ->update([
                        'status_repair_list_member' => $item,
                    ]);
            }
            DB::table('repair_list')
                ->where('reservation_id', $reservation_id)
                ->update([
                    'dob_status_member' => date('Y-m-d')
                ]);
        }

        return true;
    }
    public static function  repair_list($reservation_id='')
    {
        $data=DB::table('repair_list')
            ->whereNull('repair_list.deleted_at');
        if(!empty($reservation_id))
        {
            return $data
                ->where('repair_list.reservation_id',$reservation_id)
                ->select([
                    'repair_list.*',
                    'services.service_name',
                    'sub_services.sub_service_name',
                ])
                ->leftJoin('services','services.service_id','=','repair_list.service_id')
                ->leftJoin('sub_services','sub_services.sub_services_id','=','repair_list.sub_services_id')
                ->get();
        }
        return $data->get();
    }
    public static function get_service($reservation_id,$delete=null)
    {
        if(empty($delete))
        {
            return DB::table('reservations_services')
                ->where('reservation_id',$reservation_id)
                ->whereNull('deleted_at')//ดึงข้อมุลเก่าว่าอันเก่ามีอะไรและอันใหม่มีอะไร จะได้ไม่ซ้ำ
                ->get();
        }
        else
        {
            return DB::table('reservations_services')
                ->where('reservation_id',$reservation_id)
                ->whereNotNull('deleted_at')//ดึงข้อมุลเก่าว่าอันเก่ามีอะไรและอันใหม่มีอะไร จะได้ไม่ซ้ำ
                ->get();
        }
    }
    public static function deleted_service($ids)
    {
        if(!empty($ids))
        {
            foreach ($ids as $id)
            {
                DB::table('reservations_services')
                    ->where('reservation_service_id',$id)
                    ->update(['deleted_at'=>date('Y-m-d H:i:s')]);
            }
        }
    }
    public static function insert_service($item)
    {
        return DB::table('reservations_services')
            ->insert($item);
    }
    public static function update_service($items)
    {
        if(!empty($items))
        {
            foreach ($items as $id=>$item)
            {
                DB::table('reservations_services')
                    ->where('reservation_service_id',$id)
                    ->update($item);
            }
        }
    }
    public static function delete_reservation($reservation_id)
    {
        DB::table('reservations_services')
            ->where('reservation_id', '=', $reservation_id)
            ->update(['reservations_services.deleted_at'=>date('Y-m-d H:i:s')]);
        return DB::table('reservations')
            ->where('reservation_id', '=', $reservation_id)
            ->update(['reservations.deleted_at'=>date('Y-m-d H:i:s')]);
    }
}