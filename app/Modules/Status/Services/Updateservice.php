<?php
namespace App\Modules\Status\Services;
use App\Modules\Status\Models\StatusModel;
use DB;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 17/10/2559
 * Time: 12:42
 */
class Updateservice
{
    public static function update($chk_service,$reservation_id)
    {
        $service = DB::table('reservations_services')->where('reservation_id', $reservation_id)->get();
        $services=[];
        $insert=[];
        $update=[];

        if(!empty($service))
        {
            foreach ($service as $item)
            {
                $services[]=$item->service_id;
                if(!in_array($item,$chk_service))
                {
                    $update[$item->service_id]=[
                        'deleted_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                }
            }
        }

        if(!empty($chk_service))
        {
            foreach ($chk_service as $item)
            {
                if(in_array($item,$services))
                {
                    $update[$item]=[
                        'deleted_at'=>NULL,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                }
                else
                {
                    $insert[]=[
                        'reservation_id'=>$reservation_id,
                        'service_id'    =>$item,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                }
            }
        }

        if(!empty($update))
        {
            foreach ($update as $service_id => $item)
            {
                DB::table('reservations_services')
                    ->where('reservation_id',$reservation_id)
                    ->where('service_id',$service_id)
                    ->update($item);
            }
        }

        if(!empty($insert))
        {
            DB::table('reservations_services')->insert($insert);
        }

    }
}