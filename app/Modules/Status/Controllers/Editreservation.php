<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14/10/2559
 * Time: 11:46
 */

namespace App\Modules\Status\Controllers;


use App\Helepers\DateFormat;
use App\Modules\Booking\Models\BookingModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Status\Models\StatusModel;
use App\Modules\Status\Services\Updateservice;
use DB;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;


class Editreservation extends Controller
{
    public function edit($reservation_id,Request $request)
    {
        $_date=$request->get('_date');
        $_time=$request->get('_time');
        $_chk_category=$request->get('_chk_category');
        $_chk_brand=$request->get('_chk_brand');
        $_chk_insurance=$request->get('_chk_insurance');
//        $_chk_service=$request->get('_chk_service');
        $_reservation_detail=$request->get('_reservation_detail');
        if($_chk_category=='')
        {
            $_chk_category='6';
        }
        if($_chk_brand=='')
        {
            $_chk_brand='18';
        }
        if($_chk_insurance=='')
        {
            $_chk_insurance='17';
        }
        StatusModel::update_reservation($reservation_id,[
            'date'=>DateFormat::engDate($_date),
            'time'=>$_time,
            'reservation_detail'=>$_reservation_detail,
            'category_id'  => $_chk_category,
            'brand_id'     => $_chk_brand,
            'insurance_id' => $_chk_insurance,
//            'type_insurance'=>$_chk_insurance,
        ]);
        $check=array('_date','_time','_reservation_detail'/*,'_chk_service'*/);
        foreach ($check as $item)
        {
            if($request->get($item)=='')
            {
                return response(['กรุณากรอกข้อมูลให้ครบทุกช่องด้วยค่ะ'],422);
            }
        }

        $date_reservation = strtotime(DateFormat::engDate($_date));
        $date_ymd = strtotime(date('Y-m-d'));
        if ($date_reservation<=$date_ymd)
        {
            return response(['กรุณากรอกวันที่ให้ถูกต้องด้วยค่ะ'],422);
        }
//        Updateservice::update($_chk_service,$reservation_id);
        return response(['บันทึกข้อมูลเรียบร้อยแล้วค่ะ']);
    }
    public function edit_confirm($reservation_id)
    {
        $reservation    =BookingModel::data_booking_status($reservation_id);
        $repair_list    =StatusModel::repair_list($reservation_id);
        $status     =BookingModel::status();
//dd($reservation);
        $html=view('status::edit-status-confirm',compact('reservation','status','repair_list'));
        return ['title'=>'ตอบรับการจอง','body'=>$html->render()];
    }
    public function send_edit_confirm($reservation_id,Request $request)
    {
        $confirm_select=$request->get('confirm_select');
        $status_repair_list_member=$request->get('status_repair_list_member');
//        dd($confirm_select);
        if($confirm_select=='5')
        {
            StatusModel::update_repair_list($reservation_id,[
                'status_repair_list_member'=>$status_repair_list_member,
                'dob_status_member' =>date('Y-m-d')
            ]);
        }

        if ($confirm_select=='')
        {
            return response(['กรุณาตอบรับการจองก่อนค่ะ'],422);
        }
        else
        {
            StatusModel::update_active($reservation_id,[
                'active'=>'N',
            ]);
            $data_status = array(
                'reservation_id'=> $reservation_id,
                'status_id'=> $confirm_select,
                'active'=>'Y',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            );
//            dd($confirm_select);
            $reservation_status_id=DB::table('reservations_status')->insertGetId($data_status);
        }
        return response(['ตอบรับการซ่อมเรียบร้อยแล้วค่ะ']);
    }
    public function index ($id)
    {
        $reservation    = StatusModel::reservation_view($id);
//        dd($reservation);
        $insurances=ProductModel::garages_insurances();
        $services=ProductModel::garages_services();

        $service=StatusModel::reservation_services($id);
        $service_selected=[];
        if(!empty($service))
        {
            foreach ($service as $item)
            {
                $service_selected[]=$item->service_id;//ที่ดึงเซอร์วิสไอดีมาเพราะว่า หน้าวิวเราเรียกใช้เป็น in_array  ซึ่งไม่สามารถสั่งค่าเป็นออปเจ็กหรือเป็นก้อขข้อมูลได้
            }
        }

        $categories=ProductModel::garages_categories();
        $brands=ProductModel::garages_brands();
        $time_all=config('myconfig.open_time');
        $time=DateFormat::get_time();
        $times=[];
        foreach ($time as $item)
        {
            if($reservation->open_time <= $item && $reservation->close_time >= $item)
            {
                $times[]=$item;
            }
        }
        if(!empty($reservation->date))//เรียกใช้ฟังก์ชั่น วันที่ที่แปลงค่าไว้ข้างบน
        {
            $reservation->date=DateFormat::thaiDate($reservation->date);
        }
        $html=view('status::edit-reservation',compact(
            'reservation',
            'insurances',
            'services',
            'service_selected',
            'categories',
            'brands',
            'times',
            'time_all'
        ));
        return ['title'=>'แก้ไขข้อมูลการจอง','body'=>$html->render()];
    }

    public function cancel($reservation_id)
    {

        StatusModel::update_active($reservation_id,[
            'active'=>'N',
        ]);
        $data_status = array(
            'reservation_id'=> $reservation_id,
            'status_id'=> '2',//2= ยกเลิกการจอง
            'active'=>'Y',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $reservation_id=DB::table('reservations_status')->insertGetId($data_status);
        return response(['ยกเลิกการจองเรียบร้อยแล้วค่ะ']);
    }

}