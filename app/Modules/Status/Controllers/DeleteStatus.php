<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18/10/2559
 * Time: 15:59
 */

namespace App\Modules\Status\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Status\Models\StatusModel;

class DeleteStatus extends Controller
{
    public function delete($reservation_id)
    {
        StatusModel::delete_reservation($reservation_id);

        return response(['ลบข้อมูลการจองเรียบร้อยแล้วค่ะ']);

    }
}