<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/8/2559
 * Time: 17:17
 */

namespace App\Modules\Status\Controllers;


use App\Helepers\DateFormat;
use App\Http\Controllers\Controller;
use App\Modules\Booking\Models\BookingModel;
use App\Modules\Product\Models\ProductModel;
use App\Modules\Status\Models\StatusModel;
use Auth;
use Illuminate\Http\Request;

class Status extends Controller
{
    public function index()
    {
        $member_id      = Auth::user()->member_id;
        $reservation_me = StatusModel::reservation_me($member_id);
//        dd($reservation_me);
        return view ('status::status',[
            'reservation_me'=> $reservation_me,
        ]);
    }
    public function viewReservation($reservation_id)
    {
        $reservation    = StatusModel::reservation_view($reservation_id);
        $reservation_status  = StatusModel::reservation_view_status($reservation_id);
        $data_status=BookingModel::data_status($reservation_id);//ข้อมูลสถานะแบบตาราง
        $repair_list    =StatusModel::repair_list($reservation_id);
//dd($reservation);

        if(!empty($reservation->date))//เรียกใช้ฟังก์ชั่น วันที่ที่แปลงค่าไว้ข้างบน
        {
            $reservation->date=DateFormat::thaiDate($reservation->date);
        }
        if(!empty($reservation_status->created_at))//เรียกใช้ฟังก์ชั่น วันที่ที่แปลงค่าไว้ข้างบน
        {
            $reservation_status->created_at=DateFormat::Date_Clock($reservation_status->created_at);
        }
        $status_id=config('myconfig.status');
        $status_thai=config('myconfig.status_thai');
//        $service_name=[];
//        if(isset($reservation->list_service_name))
//        {
//            $service_name=explode(',',$reservation->list_service_name);
//        }
//        dd($reservation_date_status);
        $html=view('status::view-reservation',compact('reservation','status_id','status_thai','reservation_status','data_status','repair_list'/*,'service_name'*/));
        return ['title'=>'ดูข้อมูลสมาชิก','body'=>$html->render()];
    }

}