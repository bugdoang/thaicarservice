<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/11/2559
 * Time: 14:44
 */

namespace App\Services;

use DB;
use Illuminate\Support\Facades\Auth;

class MyGarage
{
    public static function check()
    {
        $member_id =Auth::id();
        $data = DB::table('garages')
            ->whereNull('deleted_at')
            ->where('member_id',$member_id)
            ->first();
        return !empty($data);
    }
}