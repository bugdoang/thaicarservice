<?php
namespace App\Services;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/10/2559
 * Time: 12:10
 */
use Illuminate\Support\Facades\Input;
use App\Modules\Product\Models\ProductModel;

class SearchBox
{
    public static function get()
    {
        $garage_name    = Input::get('garage_name');
        $province_id    = Input::get('province_id');
        $category_id    = Input::get('category_id');
        $service_id     = Input::get('service_id');
        $brand_id       = Input::get('brand_id');
        $insurance_id   = Input::get('insurance_id');

        $province   = ProductModel::province();
        $category   = ProductModel::category();
        $service    = ProductModel::service();
        $brand      = ProductModel::brand();
        $insurance  = ProductModel::insurance();

        return view ('include.search',[
            '_garage_name'   => $garage_name,
            '_province_id'   => $province_id,
            '_category_id'   => $category_id,
            '_service_id'    => $service_id,
            '_brand_id'      => $brand_id,
            '_insurance_id'  => $insurance_id,
            '_province'      => $province,
            '_category'      => $category,
            '_service'       => $service,
            '_brand'         => $brand,
            '_insurance'     => $insurance

        ]);
    }

}