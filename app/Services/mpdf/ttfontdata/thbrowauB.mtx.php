<?php
$name='AngsanaUPC-Bold';
$type='TTF';
$desc=array (
  'CapHeight' => 437,
  'XHeight' => 283,
  'FontBBox' => '[-501 -433 783 995]',
  'Flags' => 262148,
  'Ascent' => 964,
  'Descent' => -421,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 750,
);
$unitsPerEm=4096;
$up=-50;
$ut=25;
$strp=176;
$strs=25;
$ttffile='/vagrant/websites/room-management-system/application/third_party/mpdf/ttfonts/browaub.ttf';
$TTCfontID='0';
$originalsize=106236;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='thbrowauB';
$panose=' 1 5 2 2 8 3 7 5 5 2 3 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 888, -239, 0
// usWinAscent/usWinDescent = 964, -421
// hhea Ascent/Descent/LineGap = 888, -239, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>