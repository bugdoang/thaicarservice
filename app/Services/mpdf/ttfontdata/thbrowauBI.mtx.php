<?php
$name='AngsanaUPC-BoldItalic';
$type='TTF';
$desc=array (
  'CapHeight' => 437,
  'XHeight' => 283,
  'FontBBox' => '[-458 -433 790 994]',
  'Flags' => 262212,
  'Ascent' => 993,
  'Descent' => -421,
  'Leading' => 0,
  'ItalicAngle' => -10,
  'StemV' => 165,
  'MissingWidth' => 500,
);
$unitsPerEm=4096;
$up=-50;
$ut=25;
$strp=171;
$strs=50;
$ttffile='/vagrant/websites/room-management-system/application/third_party/mpdf/ttfonts/browauz.ttf';
$TTCfontID='0';
$originalsize=105592;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='thbrowauBI';
$panose=' 1 5 2 2 7 3 6 5 5 9 3 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 928, -206, 0
// usWinAscent/usWinDescent = 993, -421
// hhea Ascent/Descent/LineGap = 928, -206, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>