<?php
$name='AngsanaUPC-Italic';
$type='TTF';
$desc=array (
  'CapHeight' => 437,
  'XHeight' => 283,
  'FontBBox' => '[-434 -410 797 941]',
  'Flags' => 68,
  'Ascent' => 941,
  'Descent' => -399,
  'Leading' => 0,
  'ItalicAngle' => -10,
  'StemV' => 87,
  'MissingWidth' => 500,
);
$unitsPerEm=4096;
$up=-50;
$ut=25;
$strp=171;
$strs=25;
$ttffile='/vagrant/websites/room-management-system/application/third_party/mpdf/ttfonts/browaui.ttf';
$TTCfontID='0';
$originalsize=103408;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='thbrowauI';
$panose=' 1 5 2 2 5 3 5 4 5 9 3 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 923, -239, 0
// usWinAscent/usWinDescent = 941, -399
// hhea Ascent/Descent/LineGap = 923, -239, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>