<?php
$name='AngsanaUPC';
$type='TTF';
$desc=array (
  'CapHeight' => 437,
  'XHeight' => 283,
  'FontBBox' => '[-490 -410 791 941]',
  'Flags' => 4,
  'Ascent' => 941,
  'Descent' => -410,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 750,
);
$unitsPerEm=4096;
$up=-50;
$ut=25;
$strp=172;
$strs=25;
$ttffile='/vagrant/websites/room-management-system/application/third_party/mpdf/ttfonts/browau.ttf';
$TTCfontID='0';
$originalsize=109784;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='thbrowau';
$panose=' 1 5 2 2 6 3 5 4 5 2 3 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 923, -239, 0
// usWinAscent/usWinDescent = 941, -410
// hhea Ascent/Descent/LineGap = 923, -239, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>