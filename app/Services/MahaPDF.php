<?php
/**
 * Created by PhpStorm.
 * User: mahadoang
 * Date: 3/24/16
 * Time: 12:35 PM
 */
namespace App\Services;
use mPDF;
use Storage;
use App\Helper\UserHelper;

class MahaPDF
{
    public static function html($html='',$name='',$top=19,$orentation='P',$special_path=FALSE)
    {
        include_once dirname(__FILE__) . '/mpdf/mpdf.php';
        date_default_timezone_set('Asia/Bangkok');
        if($orentation=='P') $pdf = new mPDF('th','A4',"","",24,15,$top,10,6,3,'P'); // margin left right top bottom
        else $pdf = new mPDF('th','A4-L',"","",10,10,10,10,6,3,'L');
        $css = 'body{font-family:thsarabun;font-size:15pt;letter-spacing:0px;}.fa{font-family:fa;font-size:15px;}span{body{font-family:thsarabun;font-size:15pt;}';
        $pdf->WriteHTML($css,1);
        $pdf->WriteHTML(trim($html));
        if(!empty($name))
        {
            $path_inner = 'pdf/'.date('Ym');
            Storage::makeDirectory($path_inner);
            $path = storage_path('app/'.$path_inner.'/'.$name.'-'.date('YmdHis').'.pdf');
            $pdf->Output($path);
            return $path;
        }
        else
        {
            $pdf->Output();
        }
    }

    public static function preview($html='',$top=19,$orentation='P')
    {
        include_once dirname(__FILE__) . '/mpdf/mpdf.php';
        date_default_timezone_set('Asia/Bangkok');
        if($orentation=='P') $pdf = new mPDF('th','A4',"","",24,15,$top,10,6,3,'P'); // margin left right top bottom
        else $pdf = new mPDF('th','A4-L');
        $css = 'body{font-family:thsarabun;font-size:15pt;letter-spacing:0px;}.fa{font-family:fa;font-size:15px;}';
        $pdf->WriteHTML($css,1);
        $pdf->WriteHTML(trim($html));
        $file_name = md5($html).'.pdf';
        Storage::makeDirectory('preview');
        $path   = storage_path('app/preview/'.$file_name);
        $pdf->Output($path);
        return $file_name;
    }

    public static function order($html='',$ref_number,$orentation='P')
    {
        include_once dirname(__FILE__) . '/mpdf/mpdf.php';
        date_default_timezone_set('Asia/Bangkok');
        if($orentation=='P') $pdf = new mPDF('th','A4',"","",24,15,19,10,6,3,'P'); // margin left right top bottom
        else $pdf = new mPDF('th','A4-L',"","",10,10,10,10,6,3,'L');
        $css = 'body{font-family:thsarabun;font-size:15pt;letter-spacing:0px;}.fa{font-family:fa;font-size:15px;}';
        $pdf->WriteHTML($css,1);
        $pdf->WriteHTML(trim($html));
        $folder = 'orders/'.date('Ym');
        Storage::makeDirectory($folder);
        $s_path = $folder.'/'.$ref_number.'.pdf';
        $path = storage_path('app/'.$s_path);
        $pdf->Output($path);
        return $s_path;
    }

    public static function evaluation($html='',$top=19,$orentation='P')
    {
        include_once dirname(__FILE__) . '/mpdf/mpdf.php';
        date_default_timezone_set('Asia/Bangkok');
        if($orentation=='P') $pdf = new mPDF('th','A4',"","",24,15,$top,10,6,3,'P'); // margin left right top bottom
        else $pdf = new mPDF('th','A4-L',"","",10,10,10,10,6,3,'L');
        $css = 'body{font-family:thsarabun;font-size:15pt;letter-spacing:0px;}.fa{font-family:fa;font-size:15px;}';
        $pdf->WriteHTML($css,1);
        $pdf->WriteHTML(trim($html));
        $name = md5(microtime());
        if(!empty($name))
        {
            $path_inner = 'evaluation/'.date('Ym');
            Storage::makeDirectory($path_inner);
            $path = storage_path('app/'.$path_inner.'/'.$name.'.pdf');
            $pdf->Output($path);
            return $path_inner.'/'.$name.'.pdf';
        }
        else
        {
            $pdf->Output();
        }
    }
}