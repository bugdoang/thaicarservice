<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31/10/2559
 * Time: 17:05
 */

namespace App\Services;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class MyExcel extends Controller
{
    public static function export($name = '', $data)
    {
        $file_name = md5(microtime());
        $path='./downloads/'.date('Ym');
        if(!is_dir($path))
        {
            mkdir($path);
            chmod($path,0777);
        }
        Excel::create($file_name, function ($excel) use ($data) {
            $excel->sheet('Sheet1', function ($sheet) use ($data) {
                $sheet->setAutoSize(true);
                $sheet->fromArray($data);
            });
        })->store('xlsx',$path);
        return [
            'export' => 'export-file',
            'path' => $path .'/'. $file_name . '.xlsx'
        ];
    }
}