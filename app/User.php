<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        = 'members';
    protected $appends      = ['id'];
    protected $primaryKey   = 'member_id';
    protected $fillable     = [
        'title_name', 'first_name', 'last_name','avatar','email','type_of_member','status_member'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute(){
        return $this->attributes['title_name'].$this->attributes['first_name'].' '.$this->attributes['last_name'];
    }
}
