<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31/10/2559
 * Time: 17:49
 */

namespace App\Helepers;


class Download
{
    public static function get($name,$path)
    {
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        $filename = str_replace(' ', '-', $name);
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        readfile($path);
        exit;
    }

    public static function pdf($name,$path)
    {
        $filename = str_replace(' ', '-', $name);
        header("Content-type: application/octet-stream");
        header("Content-Disposition:inline;filename='".basename($filename).".pdf'");
        header('Content-Length: ' . filesize($path));
        header("Cache-control: private"); //use this to open files directly
        /*
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/pdf");
        header("Content-Disposition: attachment;filename=$filename.pdf");
        header("Content-Transfer-Encoding: binary ");
        */
        readfile($path);
        exit;
    }
}