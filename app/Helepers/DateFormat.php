<?php
namespace App\Helepers;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/9/2559
 * Time: 17:08
 */
class DateFormat
{
    public static function thaiDate($my_date){//ฟังก์ชั่น แปลงวันที่จาก ค.ศ. เป็น พ.ศ.
        if(empty($my_date))  return '';
        list($Y,$m,$d) = explode('-',$my_date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y+543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
        return $d."/".$m."/".$Y;
    }
    public static function engDate($my_date){//ฟังก์ชั่น แปลงวันที่จาก พ.ศ. เป็น ค.ศ.
        if(empty($my_date))  return '';
        list($d,$m,$Y) = explode('/',$my_date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y-543; // เปลี่ยน พ.ศ. เป็น ค.ศ.
        return $Y."-".$m."-".$d;
    }

    public static function get_time($start=0,$end=24)
    {
        $times = array();
        for($i=$start;$i<$end;$i++)
        {
            $times[] =$i.'.00';
            $times[] =$i.'.15';
            $times[] =$i.'.30';
            $times[] =$i.'.45';

        }

        return $times;
    }
    public static function Date_Clock($my_date){//ฟังก์ชั่น แปลงวันที่จาก ค.ศ. เป็น พ.ศ.
        if(empty($my_date))  return '';
        list($date,$time)=explode(' ',$my_date);
        list($Y,$m,$d) = explode('-',$date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y+543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
        list($H,$i) = explode(':',$time);
        return $d."/".$m."/".$Y."  ".$H.".".$i." "."น.";
    }
    public static function Date($my_date){//ฟังก์ชั่น แปลงวันที่จาก ค.ศ. เป็น พ.ศ.
        if(empty($my_date))  return '';
        list($date,$time)=explode(' ',$my_date);
        list($Y,$m,$d) = explode('-',$date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y+543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
        list($H,$i) = explode(':',$time);
        return $d."/".$m."/".$Y;
    }
}