<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;


/**
 * Created by PhpStorm.
 * User: User
 * Date: 27/9/2559
 * Time: 10:31
 */
class UploadController extends Controller
{
    public function index(Request $request)
    {
        $userfile=$request->file('userfile');//จากที่เลือก
        $datatype=$request->get('datatype');
        $extension=strtolower($userfile->getClientOriginalExtension());//แปลงเป็นตัวเล็ก นามสกุล
        if(in_array($extension,['jpg','png','jpeg','gif']))
        {
            if($datatype=='profile')
            {
                $member_id = Auth::user()->member_id; //ตรวจสอบการล็อคอิน
                file_put_contents('./uploads/profile/' . $member_id . '.' . $extension,//เขียนค่า string ลงไฟล์
                file_get_contents($userfile->getRealPath()));
                DB::table('members')//บันทึกอัตโนมัติเฉพาะรูปภาพ
                    ->where('member_id',$member_id)
                    ->update([
                        'avatar'=>'/uploads/profile/' . $member_id . '.' . $extension
                    ]);
                return [
                    'file_name' => $userfile->getClientOriginalName(),
                    'full_path' => '/uploads/profile/' . $member_id . '.' . $extension,
                    'status' => 200
                ];
            }
            elseif(in_array($datatype,['insurances','brands','categories']))
            {
                $member_id = Auth::user()->member_id; //ตรวจสอบการล็อคอิน
                $file_name = '/uploads/logo/' . $member_id . '-'.uniqid().'.' . $extension;
                file_put_contents('.'.$file_name,//เขียนค่า string ลงไฟล์
                    file_get_contents($userfile->getRealPath()));
                return [
                    'file_name' => $userfile->getClientOriginalName(),
                    'full_path' => $file_name,
                    'status' => 200
                ];
            }
            else
            {
                $member_id = Auth::user()->member_id; //ตรวจสอบการล็อคอิน
                $file_name = '/uploads/garage/' . $member_id . '-'.uniqid().'.' . $extension;
                file_put_contents('.'.$file_name,//เขียนค่า string ลงไฟล์
                    file_get_contents($userfile->getRealPath()));
                return [
                    'file_name' => $userfile->getClientOriginalName(),
                    'full_path' => $file_name,
                    'status' => 200
                ];
            }
        }
       else
       {
           return [
               'message' => 'กรุณาเลือกไฟล์ให้ถูกต้องด้วยค่ะ (JPG,PNG,JPEG,GIF)',
               'status' => 500
           ];
       }

      /*  Storage::put(
            $saveFullPath,
            file_get_contents($requestFile->getRealPath())
        );*/
    }
}