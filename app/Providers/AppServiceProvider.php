<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $modules=[];
        $modules[]='\App\Modules\Home\ModuleServiceProvider';
        $modules[]='\App\Modules\Product\ModuleServiceProvider';
        $modules[]='\App\Modules\Register\ModuleServiceProvider';
        $modules[]='\App\Modules\Profile\ModuleServiceProvider';
        $modules[]='\App\Modules\Garage\ModuleServiceProvider';
        $modules[]='\App\Modules\Reservation\ModuleServiceProvider';
        $modules[]='\App\Modules\Status\ModuleServiceProvider';
        $modules[]='\App\Modules\Management\ModuleServiceProvider';
        $modules[]='\App\Modules\Booking\ModuleServiceProvider';
        $modules[]='\App\Modules\Contacts\ModuleServiceProvider';
        $modules[]='\App\Modules\Guide\ModuleServiceProvider';
        foreach ($modules as $module)
        {
            $this->app->register($module);
        }
        if($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
