<div class="col-lg-3 col-md-3 register-container1 pull-right" >
    <h2 class="header-text myfont color2">รายการค้นหา</h2>
    <div class="item-border">
        <form class="form no-ajax" method="get" action="/product">
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <input placeholder="  ชื่ออู่ซ่อมรถยนต์" type="text" name="garage_name" class="form-control" value="{{$_garage_name}}">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกจังหวัด" name="province_id">
                        <option value="" >จังหวัด</option>
                        @if(!empty($_province))
                            @foreach($_province as $item)
                                <option {{($_province_id==$item->province_id)?'selected':''}} value="{{$item->province_id}}">
                                    {{$item->province_name}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกบริษัทประกันภัย" name="insurance_id">
                        <option value="">บริษัทประกันภัย</option>
                        @if(!empty($_insurance))
                            @foreach($_insurance as $item)
                                @if($item->insurance_name =='ไม่ระบุ')
                                @else
                                    <option {{($_insurance_id==$item->insurance_id)?'selected':''}} value="{{$item->insurance_id}}">
                                        {{$item->insurance_name}}
                                    </option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกประเภทงานบริการ" name="service_id">
                        <option value="">ประเภทการบริการ</option>
                        @if(!empty($_service))
                            @foreach($_service as $item)
                                @if($item->service_name =='ไม่ระบุ')
                                @else
                                <option {{($_service_id==$item->service_id)?'selected':''}} value="{{$item->service_id}}">
                                    {{$item->service_name}}
                                </option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกประเภทรถ" name="category_id">
                        <option value="">ประเภทรถ</option>
                        @if(!empty($_category))
                            @foreach($_category as $item)
                                @if($item->category_name =='ไม่ระบุ')
                                @else
                                <option {{($_category_id==$item->category_id)?'selected':''}} value="{{$item->category_id}}">
                                    {{$item->category_name}}
                                </option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <select class="form-control" data-placeholder="กรุณาเลือกยี่ห้อรถ" name="brand_id">
                        <option value="">ยี่ห้อรถ</option>
                        @if(!empty($_brand))
                            @foreach($_brand as $item)
                                @if($item->brand_name =='ไม่ระบุ')
                                @else
                                <option {{($_brand_id==$item->brand_id)?'selected':''}} value="{{$item->brand_id}}">
                                    {{$item->brand_name}}
                                </option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <button class="searching myfont color2" type="submit" style="width:100%">
                <i class="fa fa-search" aria-hidden="true"></i> ค้นหา
            </button>
        </form>
    </div>
</div>