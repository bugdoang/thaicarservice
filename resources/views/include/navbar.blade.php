<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="/">
                <img src="/assets/img/logo.png">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
            <ul class="nav navbar-nav navbar-right myfont">
                <li>
                    <a href="/product">
                        อู่ซ่อมรถยนต์ </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" style="background: #b00!important;" class="dropdown-toggle " href="">ช่วยเหลือ
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @if(Auth::check())
                            @if(Auth::user()->type_of_member=='admin')

                            @else
                                <li><a href="/contact">ติดต่อเรา</a></li>
                            @endif
                        @else
                            <li><a href="/contact">ติดต่อเรา</a></li>
                        @endif
                            <li><a href="/guide">คำแนะนำ</a></li>
                    </ul>
                </li>
                    @if(Auth::check())
                        <li class="dropdown">
                            <a data-toggle="dropdown" style="background: #b00!important;" class="dropdown-toggle" href="/profile">{{\Auth::user()->fullname}}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/profile">ข้อมูลส่วนตัว/อู่ซ่อมรถของฉัน</a></li>
                                <li><a href="/status">รายการจองของฉัน</a></li>
                                @if(\App\Services\MyGarage::check())
                                    <li>
                                        <a href="/booking">
                                            รายการจองของลูกค้า
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->type_of_member=='admin')
                                    <li><a href="/contact/admin">ข้อความจากผู้ใช้ระบบ</a></li>
                                    <li><a href="/management">จัดการข้อมูลพื้นฐาน</a></li>
                                @endif
                                <li><a href="/logout">ออกจากระบบ</a></li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a href="/login">
                                เข้าสู่ระบบ</a>
                        </li>
                        <li>
                            <a href="/register">สมัครสมาชิก</a>
                        </li>
                    @endif
        </ul>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>