<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_token" id="_token" content="{{csrf_token()}}">


    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/fonts.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/assets/css/sweetalert.css" rel="stylesheet">
    <link href="/assets/css/select2.min.css" rel="stylesheet">
    <link href="/assets/css/jquery-gmaps-latlon-picker.css" rel="stylesheet">
    <link href="/assets/css/datepicker.css" rel="stylesheet">


    <!-- Custom CSS -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>

<body>

<!-- Navigation -->
<!-- Header Carousel -->
<!-- Page Content -->
<!-- jQuery -->
@include('include.navbar')
<div style="height: 50px"></div>
<div class="container">
@yield('content')
<footer class="footer navbar "  style="margin-top: 50px;">
        <div class="myfont">
            <p  style="margin-top: 12px;"> เว็บไซต์นี้สร้างขึ้นเพื่อใช้ในการศึกษาเท่านั้น ( Website do for the study only ) </p>
        </div>
</footer>
</div>
{{--<script src="/assets/js/jquery-3.1.0.min.js"></script>--}}
    <script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/jquery-gmaps-latlon-picker.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCp6BkKH5sdIoKOoDuVZqdiX3t6vTBRByM&sensor=false&libraries=places&language=th"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/sweetalert.min.js"></script>
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script src="/assets/js/select2-mahadoang.min.js"></script>
<script src="/assets/js/SimpleAjaxUploader.min.js"></script>
<script src="/assets/js/messagebox.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script src="/assets/js/bootstrap-datepicker.th.js"></script>
<script src="/assets/js/bootstrap-datepicker-thai.js"></script>
<script src="/assets/js/libs.js"></script>
<script src="/assets/js/vue.js"></script>

@if (isset($errors) && count($errors)>0)
    <script>
        $(function () {
            var message='';
            @foreach($errors->all() as $error)
                message+='{{$error}}';
            @endforeach
            swal(message);
            /*MessageBox.alert(message,null,'<span stype="color:red"><i class="fa fa-lock"></i>ล็อคอินเข้าสู่ระบบไม่สำเร็จ</span>');*/
        });
    </script>
@endif
@stack('scripts')
<script>
    $(function () {
        if($('#txtDescription').length){
            CKEDITOR.replace( 'txtDescription' );
        }
    });
</script>
<div id="modal-edit-content"></div>

</body>

</html>
