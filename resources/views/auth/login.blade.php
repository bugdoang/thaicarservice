@extends('masterlayout')
@section('title','เข้าสู่ระบบ')
@section('content')

    <!-- Page Content -->
    <div class="row">
            <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div >
                    <div class="header-text myfont ">
                        <i class="fa fa-user" aria-hidden="true"></i> เข้าสู่ระบบ
                    </div>

                    <div style="padding-top:30px" class="item-border" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                        <form class="form-horizontal no-ajax" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                        <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="login-username" type="text" class="form-control" name="email" value="" placeholder="email">
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
                            </div>

                            <div style="margin-top:10px" class="form-group">
                                <!-- Button -->

                                <div class="col-sm-12 controls">
                                    <button class="btn btn-success" type="submit">
                                        เข้าสู่ระบบ
                                    </button>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-12 control">
                                    <div style="border-top: 1px solid#888;" >
                                        <a href="/register">
                                            สมัครสมาชิก
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>



                    </div>
                </div>
            </div>


    </div>



@stop