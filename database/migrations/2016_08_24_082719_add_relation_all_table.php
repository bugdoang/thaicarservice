<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members',function ($table){
            $table->foreign('province_id')->references('province_id')->on('provinces');
        });
        Schema::table('garages',function ($table){
            $table->foreign('member_id')->references('member_id')->on('members');
            $table->foreign('province_id')->references('province_id')->on('provinces');
        });
        Schema::table('garages_insurances',function ($table){
            $table->foreign('garage_id')->references('garage_id')->on('garages');
            $table->foreign('insurance_id')->references('insurance_id')->on('insurances');
        });
        Schema::table('garages_services',function ($table){
            $table->foreign('garage_id')->references('garage_id')->on('garages');
            $table->foreign('service_id')->references('service_id')->on('services');
        });
        Schema::table('garages_brands',function ($table){
            $table->foreign('garage_id')->references('garage_id')->on('garages');
            $table->foreign('brand_id')->references('brand_id')->on('brands');
        });
        Schema::table('garages_categories',function ($table){
            $table->foreign('garage_id')->references('garage_id')->on('garages');
            $table->foreign('category_id')->references('category_id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members',function ($table){
            $table->dropForeign('garages_province_id_foreign');
        });
        Schema::table('garages',function ($table){
            $table->dropForeign('garages_member_id_foreign');
            $table->dropForeign('garages_province_id_foreign');
        });
        Schema::table('garages_insurances',function ($table){
            $table->dropForeign('garages_insurances_garage_id_foreign');
            $table->dropForeign('garages_insurances_insurance_id_foreign');
        });
        Schema::table('garages_services',function ($table){
            $table->dropForeign('garages_services_garage_id_foreign');
            $table->dropForeign('garages_services_service_id_foreign');
        });
        Schema::table('garages_brands',function ($table){
            $table->dropForeign('garages_brands_garage_id_foreign');
            $table->dropForeign('garages_brands_brand_id_foreign');
        });
        Schema::table('garages_categories',function ($table){
            $table->dropForeign('garages_categories_garage_id_foreign');
            $table->dropForeign('garages_categories_category_id_foreign');
        });
    }
}
