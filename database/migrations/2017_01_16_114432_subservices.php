<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subservices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create('sub_services', function(Blueprint $table){
                $table->increments('sub_services_id');
                $table->string('sub_service_name',50)->nullable()->comment('ประเภทรถของสมาชิก');
                $table->timestamps();
                $table->softDeletes();
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_services');
    }
}
