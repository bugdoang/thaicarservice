<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservationService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations_services', function(Blueprint $table){
            $table->increments('reservation_service_id');
            $table->integer('reservation_id')->unsigned()->nullable()->comment('รหัสการจอง');
            $table->integer('service_id')->unsigned()->nullable()->comment('รหัสการซ่อม');
            $table->string('type_service',300)->nullable()->comment('รายการซ่อม');
            $table->timestamps();
            $table->softDeletes();

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations_services');

    }
}
