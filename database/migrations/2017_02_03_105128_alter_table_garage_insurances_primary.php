<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGarageInsurancesPrimary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages_insurances',function ($table){
            $table->dropColumn('garage_insurance_id');
            $table->primary(['garage_id','insurance_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages_insurances',function ($table){
            $table->integer('garage_insurance_id');
        });
    }
}
