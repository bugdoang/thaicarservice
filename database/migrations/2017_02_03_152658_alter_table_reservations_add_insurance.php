<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReservationsAddInsurance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations',function ($table){
            $table->integer('insurance_id')->unsigned()->nullable()->comment('รหัสบริษัทประกันภัย');
            $table->foreign('insurance_id')->references('insurance_id')->on('insurances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations',function ($table){
            $table->dropColumn('insurance_id');
            $table->dropForeign('reservations_insurance_id_foreign');
        });
    }
}
