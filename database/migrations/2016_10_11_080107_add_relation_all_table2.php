<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations',function ($table){
            $table->foreign('member_id')->references('member_id')->on('members');
            $table->foreign('garage_id')->references('garage_id')->on('garages');
        });
        Schema::table('reservations_services',function ($table){
            $table->foreign('reservation_id')->references('reservation_id')->on('reservations');
            $table->foreign('service_id')->references('service_id')->on('services');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations',function ($table){
            $table->dropForeign('reservations_member_id_foreign');
            $table->dropForeign('reservations_garage_id_foreign');
        });
        Schema::table('reservations_services',function ($table){
            $table->dropForeign('reservations_services_reservation_id_foreign');
            $table->dropForeign('reservations_services_service_id_foreign');
        });
    }
}
