<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function(Blueprint $table){
            $table->increments('contact_id');
            $table->string('first_name',180)->nullable()->comment('ชื่อ');
            $table->string('last_name',180)->nullable()->comment('นามสกุล');
            $table->string('email',180)->comment('อีเมล์ของผู้ส่ง');
            $table->enum('contact',['instructions','inform','add_data'])->default('instructions')->comment('ประเภทการติดต่อ instructions=สอบถาม/แนะนำ inform=แจ้งปัญหาการใช้งาน add_data=เพิ่มข้อมูลการบริการ');
            $table->text('message')->nullable()->comment('ข้อความ');
            $table->timestamps();
            $table->softDeletes();

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
