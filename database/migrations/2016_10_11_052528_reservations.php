<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create('reservations', function(Blueprint $table){
                $table->increments('reservation_id');
                $table->integer('garage_id')->unsigned()->nullable()->comment('รหัสอู่');
                $table->integer('member_id')->unsigned()->nullable()->comment('รหัสสมาชิก');
                $table->date('date')->nullable()->comment('วัน เดือน ปีที่นัดจอง');
                $table->string('time',5)->nullable()->comment('เวลาที่นัดจอง');
                $table->string('reservation_detail',500)->nullable()->comment('รายละเอียดเพิ่มเติมเกี่ยวกับการซ่อม');
                $table->timestamps();
                $table->softDeletes();
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');

    }
}
