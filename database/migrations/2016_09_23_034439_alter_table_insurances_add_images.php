<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableInsurancesAddImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurances',function ($table){
            $table->string('image_insurance',255)->nullable()->comment('รูปภาพประกัน');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurances',function ($table){
            $table->dropColumn('image_insurance');
        });
    }
}
