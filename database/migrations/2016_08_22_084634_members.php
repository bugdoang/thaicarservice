<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Members extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function(Blueprint $table){
            $table->increments('member_id');
            $table->string('title_name',10)->nullable()->comment('คำนำหน้าชื่อ');
            $table->string('first_name',180)->nullable()->comment('ชื่อ');
            $table->string('last_name',180)->nullable()->comment('นามสกุล');
            $table->string('avatar',255)->nullable()->comment('รูปภาพโปรไฟล์');
            $table->enum('gender',['M','F','O'])->comment('เพศ m=male f=female o=other');
            $table->date('dob')->nullable()->comment('วัน เดือน ปี เกิด');
            $table->string('card_id',13)->nullable()->comment('รหัสประจำตัวประชาชน');
            $table->string('mobile',50)->nullable()->comment('เบอร์โทรศัพท์');
            $table->string('email',180)->comment('อีเมล์ ใช้ในการล็อกอินเข้าสู่ระบบ');
            $table->string('password',200)->comment('รหัสผ่าน ที่เข้ารหัสได้แล้ว');
            $table->string('address',500)->nullable()->comment('ที่อยู่');
            $table->integer('province_id')->unsigned()->nullable()->comment('รหัสจังหวัด');
            $table->enum('type_of_member',['admin','member'])->default('member')->comment('ประเภทสมาชิก');
            $table->enum('status_member',['inactive','verified','unverified'])->default('unverified')->comment('สถานะสมาชิก');
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('members');
    }
}
