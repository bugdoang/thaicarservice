<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReservationsAddFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations',function ($table){
            $table->integer('service_id')->unsigned()->nullable()->comment('รหัสปรเภทการบริการ');
            $table->integer('brand_id')->unsigned()->nullable()->comment('รหัสจยี่ห้อ');
            $table->integer('category_id')->unsigned()->nullable()->comment('รหัสประเภทรถยนต์');

        });
        Schema::table('reservations',function ($table){
            $table->foreign('service_id')->references('service_id')->on('services');
            $table->foreign('brand_id')->references('brand_id')->on('brands');
            $table->foreign('category_id')->references('category_id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('reservations',function ($table){
            $table->dropColumn('service_id');
            $table->dropColumn('brand_id');
            $table->dropColumn('category_id');

            $table->dropForeign('reservations_service_id_foreign');
            $table->dropForeign('reservations_brand_id_foreign');
            $table->dropForeign('reservations_category_id_foreign');
        });
    }
}
