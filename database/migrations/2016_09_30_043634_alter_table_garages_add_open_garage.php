<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGaragesAddOpenGarage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages',function ($table){
            $table->string('open_time',5)->nullable()->comment('เวลาเปิดอู่');
            $table->string('close_time',5)->nullable()->comment('เวลาปิดอู่');
            $table->string('open_garages',500)->nullable()->comment('วันเปิดทำการ');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages',function ($table){
            $table->dropColumn('open_time');
            $table->dropColumn('close_time');
            $table->dropColumn('open_garages');
        });
    }
}
