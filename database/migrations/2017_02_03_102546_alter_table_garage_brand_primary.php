<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGarageBrandPrimary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages_brands',function ($table){
            $table->dropColumn('garage_brand_id');
            $table->primary(['garage_id','brand_id']);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages_brands',function ($table){
            $table->integer('garage_brand_id');
        });
    }
}