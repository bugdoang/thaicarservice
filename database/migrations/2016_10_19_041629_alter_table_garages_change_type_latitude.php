<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGaragesChangeTypeLatitude extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('latitude');
        });
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('longitude');
        });
        Schema::table('garages', function (Blueprint $table) {
            $table->string('latitude',30)->comment('ละติจูต');
        });
        Schema::table('garages', function (Blueprint $table) {
            $table->string('longitude',30)->comment('ลองติจูต');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('latitude');
        });
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('longitude');
        });
        Schema::table('garages', function (Blueprint $table) {
            $table->integer('latitude',30)->comment('ละติจูต');
        });
        Schema::table('garages', function (Blueprint $table) {
            $table->integer('longitude',30)->comment('ลองติจูต');
        });
    }
}
