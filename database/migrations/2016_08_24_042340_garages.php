<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Garages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garages', function(Blueprint $table){
            $table->increments('garage_id');
            $table->integer('member_id')->unsigned();
            $table->string('garage_name',180)->nullable()->comment('ชื่ออู่');
            $table->string('address',500)->nullable()->comment('ที่ตั้งอู่ซ่อม');
            $table->integer('province_id')->nullable()->comment('รหัสจังหวัด')->unsigned();
            $table->integer('latitude')->nullable()->comment('ละติจูต');
            $table->integer('longitude')->nullable()->comment('ลองติจูต');
            $table->string('email',180)->comment('อีเมล์ ใช้ในการติดต่ออู่ซ่อมรถ');
            $table->string('mobile',50)->nullable()->comment('เบอร์โทรศัพท์อู่ซ่อม');
            $table->enum('status_garages',['inactive','verified','unverified'])->default('unverified')->comment('สถานะสมาชิก');
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('garages');
    }
}