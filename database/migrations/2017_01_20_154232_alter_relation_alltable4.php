<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRelationAlltable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_list',function ($table){
            $table->foreign('reservation_id')->references('reservation_id')->on('reservations');
            $table->foreign('service_id')->references('service_id')->on('services');
            $table->foreign('sub_services_id')->references('sub_services_id')->on('sub_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_list',function ($table){
            $table->dropForeign('repair_list_reservation_id_foreign');
            $table->dropForeign('repair_list_status_id_foreign');
            $table->dropForeign('repair_list_sub_status_id_foreign');
        });
    }
}
