<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GaragesInsurances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garages_insurances', function(Blueprint $table){
            $table->increments('garage_insurance_id');
            $table->integer('garage_id')->unsigned();
            $table->integer('insurance_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('garages_insurances');
    }
}
