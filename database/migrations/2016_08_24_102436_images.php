<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function(Blueprint $table){
            $table->increments('image_id');
            $table->string('image_path',500)->comment('ที่เก็บข้อมุลรูปภาพ');
            $table->string('original_name',180)->nullable()->comment('ชื่อรูปภาพ');
            $table->string('image_name',180)->nullable()->comment('ชื่อรูปภาพ');
            $table->integer('garage_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
    }
}
