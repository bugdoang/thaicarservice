<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReservationsStatusAddActive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations_status',function ($table){
            $table->enum('active',['Y','N'])->comment('สถานะล่าสุด Y=สถานะล่าสุด N=สถานะเก่า');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations_status',function ($table){
            $table->dropColumn('active');
        });
    }
}
