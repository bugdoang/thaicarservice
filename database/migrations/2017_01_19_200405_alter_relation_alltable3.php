<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRelationAlltable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_services',function ($table){
            $table->foreign('service_id')->references('service_id')->on('services');
        });
        Schema::table('reservations_status',function ($table){
            $table->foreign('reservation_id')->references('reservation_id')->on('reservations');
            $table->foreign('status_id')->references('status_id')->on('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations',function ($table){
            $table->dropForeign('sub_services_service_id_foreign');
        });
        Schema::table('reservations_status',function ($table){
            $table->dropForeign('reservations_status_reservation_id_foreign');
            $table->dropForeign('reservations_status_status_id_foreign');
        });
    }
}
