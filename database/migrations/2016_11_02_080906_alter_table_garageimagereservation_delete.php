<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGarageimagereservationDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('status_garages');
        });
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('image_name');
        });
        Schema::table('reservations_services', function (Blueprint $table) {
            $table->dropColumn('type_service');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('status_garages');
        });
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('image_name');
        });
        Schema::table('reservations_services', function (Blueprint $table) {
            $table->dropColumn('type_service');
        });
    }
}
