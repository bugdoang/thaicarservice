<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableContactsAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts',function ($table){
            $table->enum('status',['read','unread'])->default('unread')->comment('read=อ่านแล้ว,unread=ยังไม่อ่าน');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts',function ($table){
            $table->dropColumn('status',['read','unread'])->default('unread')->comment('read=อ่านแล้ว ,unread=ยังไม่อ่าน');
        });
    }
}
