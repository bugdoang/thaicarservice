<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGarageCategoriesPrimary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages_categories',function ($table){
            $table->dropColumn('garage_category_id');
            $table->primary(['garage_id','category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages_categories',function ($table){
            $table->integer('garage_category_id');
        });
    }
}
