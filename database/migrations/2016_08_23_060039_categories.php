<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('categories', function(Blueprint $table){
            $table->increments('category_id');
            $table->string('category_name',180)->nullable()->comment('ชื่อประเภทรถ');
            $table->timestamps();
            $table->softDeletes();

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
