<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GaragesServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garages_services', function(Blueprint $table){
            $table->increments('garage_service_id');
            $table->integer('garage_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->string('service_detail',500)->nullable()->comment('รายละเอียดเพิ่มเติมเกี่ยวกับการบริการ');
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('garages_services');
    }
}
