<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RepairList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repair_list', function(Blueprint $table){
            $table->increments('repair_list_id');
            $table->integer('reservation_id')->unsigned()->nullable()->comment('รหัสการจอง');
            $table->integer('service_id')->unsigned()->nullable()->comment('รหัสประเภทการบริการ');
            $table->integer('sub_services_id')->unsigned()->nullable()->comment('รหัสการบริการ');
            $table->enum('status_repair_list_garage',['Y','N'])->default('Y')->comment('รายการซ่อมที่อู่เลือก');
            $table->enum('status_repair_list_member',['Y','N'])->default('N')->comment('รายการซ่อมที่ลูกค้าเลือก');
            $table->date('dob_status_garage')->nullable()->comment('วัน เดือน ปี ที่อู่เปลี่ยนสถานะ');
            $table->date('dob_status_member')->nullable()->comment('วัน เดือน ปี ที่ลูกค้าเปลี่ยนสถานะ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repair_list');

    }
}
