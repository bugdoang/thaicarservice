<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contain=file_get_contents(dirname(__FILE__).'/status.sql');
        if(!empty($contain))
        {
            DB::table('status')->delete();
            DB::statement($contain);
        }
    }
}
