<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contain=file_get_contents(dirname(__FILE__).'/services.sql');
        if(!empty($contain))
        {
            DB::table('services')->delete();
            DB::statement($contain);
        }
    }
}
