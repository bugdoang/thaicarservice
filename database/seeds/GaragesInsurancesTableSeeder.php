<?php

use Illuminate\Database\Seeder;

class GaragesInsurancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $garages_insurances=[];
        $garages_insurances[]=[
            'garage_insurance_id'=>1,
            'garage_id'=>1,
            'insurance_id'=>1,
            'status_garages'=>'verified',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages_insurances[]=[
            'garage_insurances_id'=>2,
            'garage_id'=>2,
            'insurances_id'=>5,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        DB::table('garages_insurances')->delete();
        DB::table('garages_insurances')->insert($garages_insurances);
    }
}
