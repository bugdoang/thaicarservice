<?php

use Illuminate\Database\Seeder;

class GarageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $garages=[];
        $garages[]=[
            'garage_id'=>1,
            'member_id'=>2,
            'garage_name'=>'อู่ทวีชัย',
            'address'=>'155/155 หมู่ 5',
            'province_id'=>5,
            'latitude'=>'128 124',
            'longitude'=>'584 987',
            'email'=>'mayp.pun2@gmail.com',
            'mobile'=>'089-999-9999',
            'status_garages'=>'verified',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages[]=[
            'garage_id'=>2,
            'member_id'=>2,
            'garage_name'=>'อู่นำโชค',
            'address'=>'155/155 หมู่ 5',
            'province_id'=>5,
            'latitude'=>'128 124',
            'longitude'=>'584 987',
            'email'=>'mayp.pun2@gmail.com',
            'mobile'=>'089-999-9999',
            'status_garages'=>'verified',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        DB::table('garages')->delete();
        DB::table('garages')->insert($garages);
    }
}
