<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members=[];
        $members[]=[
            'member_id'=>1,
            'title_name'=>'นางสาว',
            'first_name'=>'สุชานาฎ',
            'last_name'=>'พรกุณา',
            'avatar'=>'/uploads/default/profile.jpg',
            'gender'=>'g',
            'dob'=>'1995-04-07',
            'card_id'=>'00156333241064',
            'mobile'=>'097-3291137',
            'email'=>'mayp.pun@gmail.com',
            'password'=>Hash::make('1234'),
            'province_id'=>1,
            'type_of_member'=>'admin',
            'status_member'=>'verified',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];

        $members[]=[
            'member_id'=>2,
            'title_name'=>'นางสาว',
            'first_name'=>'วรวรรณ',
            'last_name'=>'บุดดา',
            'avatar'=>'/uploads/default/profile.jpg',
            'gender'=>'g',
            'dob'=>'1994-12-30',
            'card_id'=>'00156333241047',
            'mobile'=>'089-2906972',
            'email'=>'budda.worawan@gmail.com',
            'password'=>Hash::make('1234'),
            'province_id'=>1,
            'type_of_member'=>'member',
            'status_member'=>'verified',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        DB::table('members')->delete();
        DB::table('members')->insert($members);
    }
}
