<?php

use Illuminate\Database\Seeder;

class GaragesServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $garages_services=[];
        $garages_services[]=[
            'garage_service_id'=>null,
            'garage_id'=>1,
            'service_id'=>4,
            'service_detail'=>'ล้างแบบถอดตู้เท่านั้น',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages_services[]=[
            'garage_service_id'=>null,
            'garage_id'=>1,
            'service_id'=>2,
            'service_detail'=>'ล้างแบบถอดตู้เท่านั้น',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages_services[]=[
            'garage_service_id'=>null,
            'garage_id'=>2,
            'service_id'=>5,
            'service_detail'=>'--',
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        DB::table('garages_services')->delete();
        DB::table('garages_services')->insert($garages_services);
    }
}

