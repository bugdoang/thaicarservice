<?php

use Illuminate\Database\Seeder;

class GaragesBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $garages_brands=[];
        $garages_brands[]=[
            'garage_brand_id'=>1,
            'garage_id'=>1,
            'brand_id'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages_brands[]=[
            'garage_brand_id'=>2,
            'garage_id'=>2,
            'brand_id'=>5,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        DB::table('garages_brands')->delete();
        DB::table('garages_brands')->insert($garages_brands);
    }
}
