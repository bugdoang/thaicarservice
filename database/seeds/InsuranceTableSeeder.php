<?php

use Illuminate\Database\Seeder;

class InsuranceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contain=file_get_contents(dirname(__FILE__).'/insurance.sql');
        if(!empty($contain))
        {
            DB::table('insurances')->delete();
            DB::statement($contain);
        }
    }
}
