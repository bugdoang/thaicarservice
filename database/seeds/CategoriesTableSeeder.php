<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contain=file_get_contents(dirname(__FILE__).'/categories.sql');
        if(!empty($contain))
        {
            DB::table('categories')->delete();
            DB::statement($contain);
        }
    }
}
