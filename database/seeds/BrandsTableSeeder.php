<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $contain=file_get_contents(dirname(__FILE__).'/brands.sql');
        if(!empty($contain))
        {
            DB::table('brands')->delete();
            DB::statement($contain);
        }
    }

}
