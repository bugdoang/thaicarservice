<?php

use Illuminate\Database\Seeder;

class UpdateGarageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $garages=[];
        $garages[1]=[
            'detail'=>'ในปัจจุบันอัตราการใช้รถใช้ถนนมีเพิ่มมากขึ้นและในขณะเดียวกันอุบัติเหตุที่เกิดขึ้นก็มีมากขึ้นตาม อู่ซ่อมบำรุงในปัจจุบันต่างก็มีงานซ่อมอย่างต่อเนื่องและงานหลังการซ่อมนั้นมีทั้งที่ลูกค้าประทับใจและไม่พึงพอใจ บจก. ศรีทองออโต้เซอร์วิสเห็นความสำคัญในเรื่องความพึงพอใจของลูกค้าและคุณภาพของงานหลังการเข้ารับบริการ จึงเกิดแนวคิดสร้างอู่มาตรฐานให้บริการกับลูกค้า เพื่อให้ลูกค้าได้รับความพึงพอใจอย่างสูงสุดและพร้อมกันนั้นให้ได้รับงานคุณภาพและความประทับใจหลังการเข้ารับบริการ '
            ];
        $garages[2]=[
            'detail'=>'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        ];
        foreach ($garages as $index => $item)
        {
            DB::table('garages')->where('garage_id',$index)->update($item);
        }
    }
}
