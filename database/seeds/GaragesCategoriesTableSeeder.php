<?php

use Illuminate\Database\Seeder;

class GaragesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $garages_categories=[];
        $garages_categories[]=[
            'garage_category_id'=>1,
            'garage_id'=>1,
            'category_id'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages_categories[]=[
            'garage_category_id'=>2,
            'garage_id'=>2,
            'category_id'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        $garages_categories[]=[
            'garage_category_id'=>3,
            'garage_id'=>2,
            'category_id'=>3,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        DB::table('garages_categories')->delete();
        DB::table('garages_categories')->insert($garages_categories);
    }
}
